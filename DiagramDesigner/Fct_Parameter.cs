﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DiagramDesigner
{
    public class Fct_Parameter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        protected DesignerItem parentDesignerItem;
        public DesignerItem ParentDesignerItem
        {
            get { return parentDesignerItem; }
            set { parentDesignerItem = value; }
        }

        private string type;
        public string Type //define a ENum 
        {
            get { return type; }
            set
            {
                /*if (!value.Contains(" "))*/
                type = value;
                NotifyPropertyChanged("Type");
            }
        }

        private Guid id; //this will be set from the constructor
        public Guid ID
        {
            get { return id; }
        }

        private object value;
        public object Value //define a ENum 
        {
            get { return value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    NotifyPropertyChanged("Value");
                }
            }
        }

        public Fct_Parameter(DesignerItem _parent, string _name, string _type, object _value) :
            this() //sets GUID
        {
            this.name = _name;
            this.value = _value;
            this.type = _type;
            this.parentDesignerItem = _parent;
        }

        public Fct_Parameter(DesignerItem _parent, string _name, string _type, object _value, Guid _id)
        {
            this.parentDesignerItem = _parent;
            this.name = _name;
            this.value = _value;
            this.type = _type;
            this.id = _id;
        }

        public Fct_Parameter()
        {
            this.id = Guid.NewGuid();

        }

        public Fct_Parameter DeepCopy()
        {
            Fct_Parameter copiedparam = (Fct_Parameter)this.MemberwiseClone();

            return copiedparam;
        }

        

    }
}
