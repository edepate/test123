﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DiagramDesigner
{
    public class Fct_inParameter: Fct_Parameter
    {
        public Fct_inParameter(DesignerItem _parent, string _name, string _type, object _value, Guid _id, bool _extDef) :
            base(_parent, _name, _type, _value, _id)
        {
            this.externallyDefined = (bool)_extDef;
        }

        public Fct_inParameter(DesignerItem _parent, string _name, string _type, object _value, bool _extDef) :
            base(_parent, _name, _type, _value)
        {
            this.externallyDefined = (bool)_extDef;
        }

        private bool externallyDefined;
        public bool ExternallyDefined
        {
            get { return externallyDefined; }
            set
            {
                externallyDefined = value;
                NotifyPropertyChanged("ExternallyDefined");
            }
        }

        public Fct_Parameter getAssociatedReturnParameter()
        {  // TODO: returns the reference to the Fct_Parameter objects that is connected with "this" Fct_Parameter via the connector
            if (!this.ExternallyDefined) return null;

            DesignerItem designerItem = parentDesignerItem;
            if (designerItem != null)
            {
                // RUG: get the Connector object with same ID (1:1 association within same DesignerItem)
                Connector ConnectorOut = null;
                object a = designerItem.GetTemplateChild_public("PART_ConnectorDecorator");

                object z = ClassTreeHelper.FindVisualChildren<Connector>((Control)a);
                //foreach (Connector element in LogicalTreeHelper.GetChildren(designerItem).OfType<Connector>())
                foreach (Connector element in (IEnumerable<Control>)z)
                {
                    if (element.AssociatedID == this.ID)
                    {
                        ConnectorOut = element;
                        break;
                    }
                }
                if (ConnectorOut == null) return null;

                // RUG: get associated Parameter (externallyDefined)
                if ((ConnectorOut).Connections.Count == 0) return null; // RUG: no connector existing
                object ConnectorNextIn = ((Connector)ConnectorOut).Connections[0].Sink;

                /* RUG: as the arrow has a direction, it might be that the destination is in the Source:
                   InputParam []<--[] OutputParam  then Sink is the InputParam!
                   InputParam []-->[] OutputParam  then Sink is the OutputParam --> OK!
                   The next "if" just switches Sink with Source in case of wrong direction
                */
                if ((ConnectorNextIn as Connector).AssociatedID == ConnectorOut.AssociatedID)
                    ConnectorNextIn = ((Connector)ConnectorOut).Connections[0].Source;

                if (ConnectorNextIn != null)
                {
                    Guid associatedID;
                    DesignerItem_Fct associatedDesignerItem;
                    associatedID = (ConnectorNextIn as Connector).AssociatedID;

                    associatedDesignerItem = (DesignerItem_Fct)(ConnectorNextIn as Connector).ParentDesignerItem;

                    foreach (Fct_Parameter item in associatedDesignerItem.ReturnParameters)
                    {
                        if (item.ID == associatedID)
                            return item;
                    }
                }

            }

            return null;
        }
    }
}
