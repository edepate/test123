﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FTD2XX_NET;
using DiagramDesigner.ODIS_WebServer;
using System.Windows.Controls;
using OpenQA.Selenium.IE;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Text.RegularExpressions;

namespace DiagramDesigner
{
    public partial class TC_Executor
    {
        //   private BlockArgs timerargs;

        // private System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public System.Windows.Threading.DispatcherTimer dispatcherTimer;
        public Functions logpass;

        public void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            dispatcherTimer.Stop();
            
          //  dispatcherTimer.Tick -= (sender2, e2) => dispatcherTimer_Tick(sender, e);
            // dispatcherTimer.Tick -= null;


            // Three button block needs only waiting functionality withing its function definiton, therefor is 
            // onevexecuted called from TC Executor and not here.

            //if(e.Name == "ThreeBM")
            //{
            //    //Functions Funktion = new Functions(this);

            //    e.Name = "ThreeBMrelease";

            //   // object[] inout = new object[2];
            //    //IEnumerable<Fct_Parameter> obscollection = (IEnumerable<Fct_Parameter>)e.Parameters;
            //    //var ParamtoObject = (from item in obscollection select item as object).ToArray();
            //    //inout[0] = ParamtoObject;
            //    // object blabla = (int)5;
            //    //e.Status = (string)typeof(Functions).GetMethod(e.Name).Invoke(Funktion, inout);
            //} 
            BlockArgs ee = new BlockArgs();

            ee.Name = "Wait";
            ee.Status = "OK";
             
            OnEvFunctionExecuted(ee);
        }
        public TC_Executor()
        {
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            //logpass = new Functions();
            //logpass.evLogAdded += somefunction;

            // dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);




        }

        ~TC_Executor()
        {

           // dispatcherTimer.Tick -= new EventHandler(dispatcherTimer_Tick);

        }

        //public void somefunction(object sender, LogArgs e)
        //{
        //    Debug.WriteLine("blabla");
        //}

        public delegate void LogExecutorEventHandler(object source, LogArgs args);

        public event LogExecutorEventHandler evLogExecutor;

        // protected virtual void OnLogExecutor(object source,LogArgs args)
        protected virtual void OnLogExecutor(String logdata)
        {
            if (evLogExecutor != null)
                evLogExecutor(this, new LogArgs { Content = logdata });
        }




        public class Functions
        {

            // LUK: This part is needed for connection between TC Executor and nested Functions class to be able to show LOGS from "functions" class in window
            private TC_Executor _owner;

            public List<Fct_Parameter> OutputParameters = new List<Fct_Parameter>();


            public Functions(TC_Executor Owner)
            {
                _owner = Owner;
            }

            public delegate void LogAddedEventHandler(object source, LogArgs args);

            public event LogAddedEventHandler evLogAdded;

            protected virtual void OnLogAdded(String text)
            //  public void OnLogAdded(String text)
            {
                if (evLogAdded != null)
                    evLogAdded(this, new LogArgs { Content = text });
            }






            public string Parameters(object[] parameters, object[] oparameter)
            {
                //_owner.OnLogExecutor("Just write-");

                //System.Threading.Thread.Sleep(2000);

                //_owner.OnLogExecutor(" 1. after 2 sec");

                //System.Threading.Thread.Sleep(3000);

                //_owner.OnLogExecutor("2. after 3 sec");

                //System.Threading.Thread.Sleep(1000);

                //_owner.OnLogExecutor("3 . after 1 sec");
                //  object[] input = (object[])parameters[0];



                // List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                // String log = null;
                String data = null;

                foreach (var element in Paramlist)
                {
                    data += element.Name + " " + element.Type + " " + element.Value + "\n";
                    //   MessageBox.Show(element.Name + " WE GOT IT");
                }
                // MessageBox.Show(data);


                //Fct_Parameter fkt1 = new Fct_Parameter();
                //Fct_Parameter fkt2 = new Fct_Parameter();
                //fkt1.Name = "blabla";
                //fkt1.Value = 15;
                //fkt2.Name = "blabla 2";
                //fkt2.Value = 30;

                //OutputParameters.Add(fkt1);
                //OutputParameters.Add(fkt2);

                //int bla = 15;
                //int bla2 = 30;

                //object[] array = { bla, bla2 };
                //oparameter = new object[2];
                //oparameter[0] = bla;
                //oparameter[1] = bla2;

                OutputParameters = Paramlist.ToList();



                _owner.OnLogExecutor(data);
                Debug.WriteLine(data);

                return "OK";

            }

            public string Parameters2(object[] parameters, object[] oparameter)
            {
              

                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                // String log = null;
                String data = null;

                foreach (var element in Paramlist)
                {
                    data += element.Name + " " + element.Type + " " + element.Value + "\n";
                    //   MessageBox.Show(element.Name + " WE GOT IT");
                }
               

               _owner.OnLogExecutor(data);
                Debug.WriteLine(data);

                return "OK";

            }


            private static CANoe.Application mCANoeApp;
            private static CANoe.Measurement mCANoeMeasurement;
            private static CANoe.CAPLFunction[] myfunction;
            private static CANoe.CAPL mCANoeCAPL;

            private static List<Fct_Parameter> CAPLFunctions;

            public string CANoe_Open(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();



                if (mCANoeApp == null)
                    mCANoeApp = new CANoe.Application();


                mCANoeApp.Visible = false;


                if (mCANoeApp.Measurement.Running == true)
                {
                    return "CAN simulation already running (Hint: add Tool_CANoe_stopSim before this Fkt)";
                }


                if (Paramlist.Count != 1)
                    return "Wrong number of Parameters. 1 Parameter (path to config) needed. You gave " + Paramlist.Count + " parameters";

                try
                {
                    
                    mCANoeApp.Open((string)Paramlist.First().Value, true, true);

                    _owner.OnLogExecutor("CANoe opened Successfully.");

                    CANoe.OpenConfigurationResult ocresult = mCANoeApp.Configuration.OpenConfigurationResult;

                    if (ocresult.result != 0)
                        _owner.OnLogExecutor("CONFIGURATION FILE NOT FOUND");

                    _owner.OnLogExecutor("CANoe Config Loaded Successfully.");

                    mCANoeCAPL = (CANoe.CAPL)mCANoeApp.CAPL;
                    mCANoeCAPL.Compile(null);

                    _owner.OnLogExecutor("CANoe config file compiled successfully.");


                    return "OK";
                }
                catch (Exception e)
                {
                    _owner.OnLogExecutor(e.ToString() + "\n\n");
                    return "Is your Path to config file correct?";
                }



            }



            public string CANoe_StartMSR(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                if (Paramlist.Count != 0)
                    CAPLFunctions = Paramlist;

                //      mCANoeApp = new CANoe.Application();
                if (mCANoeApp == null)
                    return "No valid project opened, please insert first CANoe_Open function with valid parameter ";

                if (mCANoeApp.Measurement.Running == true)
                {
                    return "CAN simulation already running (Hint: add Tool_CANoe_stopSim before this Fkt)";
                }



                try
                {

                    mCANoeMeasurement = (CANoe.Measurement)mCANoeApp.Measurement;

                    if (CAPLFunctions != null)
                        mCANoeMeasurement.OnInit += new CANoe._IMeasurementEvents_OnInitEventHandler(MeasurementInitiated);



                    if (!mCANoeMeasurement.Running)

                    {
                        mCANoeMeasurement.Start();
                        _owner.OnLogExecutor("Measurement Started.");
                    }
                    else
                        _owner.OnLogExecutor("Measurement already Running.");




                    return "OK";

                }
                catch (Exception e)
                {
                    _owner.OnLogExecutor(e.ToString() + "\n\n");
                    return "";
                }



            }


            public string CANoe_StopMSR(object[] parameters, object[] oparameter)
            {


                if (CAPLFunctions != null)
                {
                    mCANoeMeasurement.OnInit -= new
    CANoe._IMeasurementEvents_OnInitEventHandler(MeasurementInitiated);

                    mCANoeMeasurement.OnStop -= new
  CANoe._IMeasurementEvents_OnStopEventHandler(Functioncall);
                }

                // LUK: Ask RUggero
                // PROBLEM: At this point, it happens that it gets executed before the measurement from CANoe properly started
                // We could subscribe to onstart event of measurement, but that would need the whole function subscribe to that
                // whats the best solution? now iam waiting just 5 seconds
                bool executed = false;
                for (int i = 0; i < 100; i++)
                {
                    if ((mCANoeApp.Measurement.Running))

                    {
                        mCANoeMeasurement.StopEx();
                        _owner.OnLogExecutor("Measurement Stopped.");
                        i = 100;
                        executed = true;

                    }
                    System.Threading.Thread.Sleep(50);

                }

                if (!executed)
                    _owner.OnLogExecutor("You need to start measurement first, before ending it");




                return "OK";
            }
            private void MeasurementInitiated()
            {
                // Add your code here

                myfunction = new CANoe.CAPLFunction[CAPLFunctions.Count];

                for (int i = 0; i < myfunction.Count(); i++)
                {
                    myfunction[i] = (CANoe.CAPLFunction)mCANoeCAPL.GetFunction(CAPLFunctions[i].Name.ToString());
                }

            }



            public string CANoe_FCT(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                mCANoeMeasurement.OnStart += new
  CANoe._IMeasurementEvents_OnStartEventHandler(Functioncall);


                //for (int i=0; i < CAPLFunctions.Count; i++)
                //{
                //    if(!(CAPLFunctions[i].value.ToString().Length == 0))
                //    {
                //        // parse parameters
                //    }
                //    else
                //    {
                //        // call without parameters
                //        _owner.OnLogExecutor(myfunction[i].Call().toString);
                //    }
                //}

                return "OK";
            }


            public void Functioncall()
            {
                for (int i = 0; i < CAPLFunctions.Count; i++)
                {

                    //  object blaa = CAPLFunctions[i].value;
                    if (!(CAPLFunctions[i].Value == null))
                    {
                        // parse parameters
                    }
                    else
                    {
                        // call without parameters
                        myfunction[i].Call();
                        //    _owner.OnLogExecutor(myfunction[i].Call().toString);
                    }
                }
                return;
            }


            public string CANoe_SETENV(object[] parameters, object[] oparameter)
            {
                if (mCANoeApp == null)
                    mCANoeApp = new CANoe.Application();

                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                CANoe.EnvironmentVariable[] mSwitch0EnVar;
                mSwitch0EnVar = new CANoe.EnvironmentVariable[Paramlist.Count];

                CANoe.Environment mEnvironment = (CANoe.Environment)mCANoeApp.Environment;


                for (int i = 0; i < Paramlist.Count; i++)
                {
                    mSwitch0EnVar[i] = (CANoe.EnvironmentVariable)mEnvironment.GetVariable(Paramlist[i].Name);
                    mSwitch0EnVar[i].Value = Paramlist[i].Value;
                    _owner.OnLogExecutor(Paramlist[i].Name + " set to value: " + Paramlist[i].Value);

                }


                return "OK";
            }




            public string CANoe_GETSIG(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();



                try
                {
                    if (mCANoeApp == null)
                        mCANoeApp = new CANoe.Application();


                    string[] values = new string[Paramlist.Count];


                    for (int i = 0; i < Paramlist.Count(); i++)
                    {
                        string[] data = Paramlist[i].Name.Split('|');
                        if (data.Count() != 4)
                        {
                            _owner.OnLogExecutor("Error: Wrong number of Parameters. 4 Parameters divided by | needed: Bus, Channel, Message Name, Signal Name. (CAN|1|EngineData|EngineSpeed ");
                            return "not working";
                        }

                        CANoe.Bus CANoeBus = (CANoe.Bus)mCANoeApp.get_Bus(data[0]);
                        CANoe.Signal Signal = (CANoe.Signal)CANoeBus.GetSignal(Int32.Parse(data[1]), data[2], data[3]);

                        values[i] = Signal.Value.ToString();

                        _owner.OnLogExecutor("Signal " + data[3] + " has value" + values[i] + Environment.NewLine);

                    }


                }
                catch (Exception e)
                {
                    _owner.OnLogExecutor(e.ToString() + "\n\n");
                    //  return "Is your Path to config file correct?";
                }

                return "OK";
            }

            public string CANoe_SETSIG(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();



                try
                {
                    if (mCANoeApp == null)
                        mCANoeApp = new CANoe.Application();


                    string[] values = new string[Paramlist.Count];


                    for (int i = 0; i < Paramlist.Count(); i++)
                    {
                        if (Paramlist[i].Value == null)
                        {

                            _owner.OnLogExecutor("One (or more) of Parameter Values is empty");
                            return "empty";

                        }



                        string[] data = Paramlist[i].Name.Split('|');
                        if (data.Count() != 4 || Paramlist[i].Value.ToString().Equals(""))
                        {
                            _owner.OnLogExecutor("Error: Wrong number of Parameters. 4 Parameters divided by | needed: Bus, Channel, Message Name, Signal Name. (CAN|1|EngineData|EngineSpeed ");
                            return "not working";
                        }

                        CANoe.Bus CANoeBus = (CANoe.Bus)mCANoeApp.get_Bus(data[0]);
                        CANoe.Signal Signal = (CANoe.Signal)CANoeBus.GetSignal(Int32.Parse(data[1]), data[2], data[3]);

                        Signal.Value = Int32.Parse(Paramlist[i].Value.ToString());

                        System.Threading.Thread.Sleep(500);
                        values[i] = Signal.Value.ToString();

                        _owner.OnLogExecutor("Signal " + data[3] + " has value" + values[i] + Environment.NewLine);

                    }


                }
                catch (Exception e)
                {
                    _owner.OnLogExecutor(e.ToString() + "\n\n");
                    //  return "Is your Path to config file correct?";
                }

                return "OK";
            }


            public string ODIS_getDTCList(object[] parameters, object[] oparameter)
            {


                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                if (Paramlist.Count != 2)
                    return "You need exactly two parameters: (vehicle) project and ECU Adress, you provided " + Paramlist.Count + " parameters";

                ODIS_WebServer.DiagnosticInterfaceImplClient ODIS_diag = new ODIS_WebServer.DiagnosticInterfaceImplClient("DiagnosticInterfaceImplPort", "http://127.0.0.1:8081/OdisAutomationService?wsdl");
                ODIS_WebServer.diagResultConnectEcuImpl ODIS_respConnectToEcu = new diagResultConnectEcuImpl();
                ODIS_WebServer.diagResultEventMemoryImpl ODIS_respDTC = new diagResultEventMemoryImpl();
                String name_tmp, name_tmp_2, value_tmp;


                var project = Paramlist.First(item => item.Name.ToLower() == "project").Value;
                var ECU = Paramlist.First(item => item.Name.ToLower() == "ecu").Value;

                try
                {
                    ODIS_diag.setVehicleProject((string)project);
                    ODIS_diag.configureSetting("Multilink.MaxNumberOfLogicalLinks", "1");

                    int ECUADRESS = Int32.Parse((string)ECU, System.Globalization.NumberStyles.HexNumber);
                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    ODIS_respDTC = ODIS_diag.readEventMemory(ODIS_respConnectToEcu.connectionHandle);

                    ODIS_WebServer.diagResultEventMemoryEntryImpl[] list_ODIS_entriesDTC = new ODIS_WebServer.diagResultEventMemoryEntryImpl[1024];



                    list_ODIS_entriesDTC = ODIS_respDTC.eventMemoryEntries;


                    for (int i = 0; i < ODIS_respDTC.eventMemoryEntries.Length; i++)
                    {
                        name_tmp = int.Parse(list_ODIS_entriesDTC[i].eventInfos[0].value).ToString("x");
                        name_tmp_2 = list_ODIS_entriesDTC[i].eventInfos[3].value;
                        value_tmp = list_ODIS_entriesDTC[i].dtcStates[0].bitValueText;

                        _owner.OnLogExecutor("{" + name_tmp + "}<" + value_tmp + ">" + " And this is eventInfo[3]: " + name_tmp_2);



                    }

                    ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);
                    _owner.OnLogExecutor("Connection Closed");
                }
                catch (Exception ex)
                {
                    _owner.OnLogExecutor("Error: Exception raised while communication with ODIS WebServer: " + ex.Message);

                }


                return "OK";
            }



            public string ODIS_getDTC(object[] parameters, object[] oparameter)
            {


                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();


                var project = Paramlist.First(item => item.Name.ToLower() == "project").Value;
                var ECU = Paramlist.First(item => item.Name.ToLower() == "ecu").Value;
                var inputDTC = Paramlist[2].Name;
                // string inputDTC = "1a2e";
                ODIS_WebServer.DiagnosticInterfaceImplClient ODIS_diag = new DiagnosticInterfaceImplClient("DiagnosticInterfaceImplPort", "http://127.0.0.1:8081/OdisAutomationService?wsdl");
                ODIS_WebServer.diagResultConnectEcuImpl ODIS_respConnectToEcu = new diagResultConnectEcuImpl();
                ODIS_WebServer.diagResultEventMemoryImpl ODIS_respDTC = new diagResultEventMemoryImpl();

                String name_tmp, name_tmp_2, value_tmp;


                try
                {
                    ODIS_diag.setVehicleProject((string)project);
                    ODIS_diag.configureSetting("Multilink.MaxNumberOfLogicalLinks", "1");

                    int ECUADRESS = Int32.Parse((string)ECU, System.Globalization.NumberStyles.HexNumber);
                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    ODIS_respDTC = ODIS_diag.readEventMemory(ODIS_respConnectToEcu.connectionHandle);

                    ODIS_WebServer.diagResultEventMemoryEntryImpl[] list_ODIS_entriesDTC = new ODIS_WebServer.diagResultEventMemoryEntryImpl[1024];



                    list_ODIS_entriesDTC = ODIS_respDTC.eventMemoryEntries;


                    for (int i = 0; i < ODIS_respDTC.eventMemoryEntries.Length; i++)
                    {
                        name_tmp = int.Parse(list_ODIS_entriesDTC[i].eventInfos[0].value).ToString("x");
                        name_tmp_2 = list_ODIS_entriesDTC[i].eventInfos[3].value;
                        value_tmp = list_ODIS_entriesDTC[i].dtcStates[0].bitValueText;

                        // Debug.WriteLine("{" + name_tmp + "}<" + value_tmp + ">");

                        if (name_tmp.Equals(inputDTC))

                        {
                            _owner.OnLogExecutor("Selected DTC: " + inputDTC +
                                " {" + name_tmp + "}<" + value_tmp + ">" + " And this is eventInfo[3]: " + name_tmp_2);

                        }

                    }

                    ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);
                    _owner.OnLogExecutor("Connection Closed");
                }
                catch (Exception ex)
                {
                    _owner.OnLogExecutor("Error: Exception raised while communication with ODIS WebServer: " + ex.Message);

                }


                //  return Paramlist;
                return "OK";

            }





            public string ODIS_getMSRM(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                if (Paramlist.Count < 3)
                    return "Not enough parameters! You need at least 3 (Project, ECU, Measurements)";

                string[] measurements = new string[Paramlist.Count - 2];

                var project = Paramlist.First(item => item.Name.ToLower() == "project").Value;
                var ECU = Paramlist.First(item => item.Name.ToLower() == "ecu").Value;

                //var inputDTC = Paramlist.First(item => item.Name.ToLower() == "dtc").Value;


                for (int i = 2; i < Paramlist.Count; i++)
                {
                    measurements[i - 2] = (string)Paramlist[i].Name;
                }




                ODIS_WebServer.DiagnosticInterfaceImplClient ODIS_diag = new DiagnosticInterfaceImplClient("DiagnosticInterfaceImplPort", "http://127.0.0.1:8081/OdisAutomationService?wsdl");
                ODIS_WebServer.diagResultConnectEcuImpl ODIS_respConnectToEcu = new diagResultConnectEcuImpl();
                ODIS_WebServer.diagResultEventMemoryImpl ODIS_respDTC = new diagResultEventMemoryImpl();

                String name_tmp, name_tmp_2, value_tmp, return_str;

                // int measurementscount = 1;
                // string param = "battery voltage";

                ODIS_WebServer.diagMeasurementDescriptorImpl[] ODIS_MeasDescriptor = new ODIS_WebServer.diagMeasurementDescriptorImpl[measurements.Count()];
                ODIS_WebServer.diagResultMeasurementImpl[] ODIS_MeasResult = new ODIS_WebServer.diagResultMeasurementImpl[1024];
                ODIS_WebServer.diagResultValueImpl[] ODIS_MeasResultItem = new ODIS_WebServer.diagResultValueImpl[1024];



                name_tmp = "";
                value_tmp = "";

                try
                {
                    ODIS_diag.setVehicleProject((string)project);
                    ODIS_diag.configureSetting("Multilink.MaxNumberOfLogicalLinks", "1");


                    // HIER IST ECU "19"
                    int ECUADRESS = Int32.Parse((string)ECU, System.Globalization.NumberStyles.HexNumber);
                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    for (int i = 0; i < measurements.Count(); i++)
                    {
                        ODIS_MeasDescriptor[i] = new ODIS_WebServer.diagMeasurementDescriptorImpl();
                        ODIS_MeasDescriptor[i].ecuAddress = ECUADRESS;
                        ODIS_MeasDescriptor[i].group = measurements[i];
                        ODIS_MeasDescriptor[i].parameter = "";

                    }

                    ODIS_MeasResult = ODIS_diag.readMeasurements(ODIS_MeasDescriptor);

                    for (int i = 0; i < (ODIS_MeasResult.Count()); i++)
                    {
                        ODIS_MeasResultItem = ODIS_MeasResult[i].measurements;

                        _owner.OnLogExecutor("Measurement " + measurements[i] + "has following values:");


                        for (int j = 0; j < ODIS_MeasResultItem.Length; j++)
                        {
                            name_tmp = ODIS_MeasResultItem[j].name;
                            value_tmp = ODIS_MeasResultItem[j].value;
                            _owner.OnLogExecutor("Oject: " + name_tmp + " has value: " + value_tmp);
                        }

                        //MRKNI DO VB ....je to nejakej error handling
                        //if(i > 



                    }

                    ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);

                }
                catch (Exception ex)
                {
                    _owner.OnLogExecutor("Error: Exception raised while communication with ODIS WebServer: " + ex.Message);

                }

                return "OK";
            }


            public string ODIS_getID(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                if (Paramlist.Count < 3)
                    return "Not enough parameters! You need at least 3 (Project, ECU, IDs to get)";

                string[] IDs = new string[Paramlist.Count - 2];

                var project = Paramlist.First(item => item.Name.ToLower() == "project").Value;
                var ECU = Paramlist.First(item => item.Name.ToLower() == "ecu").Value;

                //var inputDTC = Paramlist.First(item => item.Name.ToLower() == "dtc").Value;


                for (int i = 2; i < Paramlist.Count; i++)
                {
                    IDs[i - 2] = (string)Paramlist[i].Name;
                }


                ODIS_WebServer.DiagnosticInterfaceImplClient ODIS_diag = new DiagnosticInterfaceImplClient("DiagnosticInterfaceImplPort", "http://127.0.0.1:8081/OdisAutomationService?wsdl");
                ODIS_WebServer.diagResultConnectEcuImpl ODIS_respConnectToEcu = new diagResultConnectEcuImpl();

                ODIS_WebServer.diagResultIdentificationImpl[] ODIS_respIdentif = new diagResultIdentificationImpl[1024];
                ODIS_WebServer.diagResultValueImpl[] ODIS_entryIdentif = new diagResultValueImpl[1024];

                string name_tmp, name_tmp_2, value_tmp;



                //ECU 005f


                try
                {
                    ODIS_diag.setVehicleProject((string)project);
                    ODIS_diag.configureSetting("Multilink.MaxNumberOfLogicalLinks", "1");

                    int ECUADRESS = Int32.Parse((string)ECU, System.Globalization.NumberStyles.HexNumber);
                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    ODIS_respIdentif = ODIS_diag.readIdentification(ODIS_respConnectToEcu.connectionHandle);

                    ODIS_WebServer.diagResultEventMemoryEntryImpl[] list_ODIS_entriesDTC = new diagResultEventMemoryEntryImpl[1024];

                    for (int i = 0; i < ODIS_respIdentif.Length; i++)
                    {
                        ODIS_entryIdentif = ODIS_respIdentif[i].standardData;

                        if (ODIS_entryIdentif[i].name.Contains("Slave"))
                        { continue; }

                        for (int j = 0; j < ODIS_entryIdentif.Length; j++)
                        {
                            name_tmp = ODIS_entryIdentif[j].name;
                            value_tmp = ODIS_entryIdentif[j].value;
                            // a vyprintuj to nebo co ...mrkni do orig funkce...
                            //v postate se printne jen ten zadanej parameter z value_tmp
                            _owner.OnLogExecutor(name_tmp + ": " + value_tmp);

                            for (int k = 0; k < IDs.Count(); k++)
                            {
                                if (name_tmp.Contains(IDs[k]))
                                {
                                    // DO SOMETHIN

                                    // v podstate se az tady bude neco vypisovat respektive vracet
                                    _owner.OnLogExecutor(name_tmp + ": " + value_tmp);
                                }
                            }

                        }




                    }

                    ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);

                }
                catch (Exception ex)
                {
                    _owner.OnLogExecutor("Error: Exception raised while communication with ODIS WebServer: " + ex.Message);

                }


                return "OK";
            }







            public string ODIS_SetCoding(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                if (Paramlist.Count < 3)
                    return "Not enough parameters! You need at least 3 (Project, ECU,  Coding parameters)";

                string[] Codings = new string[Paramlist.Count - 2];
                string[] Values = new string[Paramlist.Count - 2];
                bool[] Codingfound = new bool[Paramlist.Count - 2];

                var project = Paramlist.First(item => item.Name.ToLower() == "project").Value;
                var ECU = Paramlist.First(item => item.Name.ToLower() == "ecu").Value;

                //var inputDTC = Paramlist.First(item => item.Name.ToLower() == "dtc").Value;


                for (int i = 2; i < Paramlist.Count; i++)
                {
                    Codings[i - 2] = (string)Paramlist[i].Name;
                    Values[i - 2] = (string)Paramlist[i].Value;
                    Codingfound[i - 2] = true;

                }


                ODIS_WebServer.DiagnosticInterfaceImplClient ODIS_diag = new DiagnosticInterfaceImplClient("DiagnosticInterfaceImplPort", "http://127.0.0.1:8081/OdisAutomationService?wsdl");
                ODIS_WebServer.diagResultConnectEcuImpl ODIS_respConnectToEcu = new diagResultConnectEcuImpl();
                ODIS_WebServer.diagResultCodingImpl[] ODIS_CodingResult = new diagResultCodingImpl[1024];
                ODIS_WebServer.mapAdapter ODIS_CodingResultItem = new mapAdapter();
                ODIS_WebServer.mapAdapter ODIS_mapToCodeBack = new mapAdapter();

                // string name_tmp, value_tmp;
                string ODIS_userID = string.Empty;
                string ODIS_pwd = string.Empty;
                string name_tmp = string.Empty;
                string value_tmp = string.Empty;


                ODIS_userID = "Login";
                ODIS_pwd = "20103";

                int n_measurement = 1; // number of parameters myslim
                try
                {
                    ODIS_diag.setVehicleProject((string)project);
                    ODIS_diag.configureSetting("Multilink.MaxNumberOfLogicalLinks", "1");

                    int ECUADRESS = Int32.Parse((string)ECU, System.Globalization.NumberStyles.HexNumber);
                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    ODIS_diag.switchSession(ODIS_respConnectToEcu.connectionHandle, "DiagnServi_DiagnSessiContrDevelSessi");

                    if (!(ODIS_userID.Length == 0 || ODIS_pwd.Length == 0))
                        ODIS_diag.securityAccess(ODIS_respConnectToEcu.connectionHandle, ODIS_pwd, ODIS_userID);


                    ODIS_CodingResult = ODIS_diag.readCoding(ODIS_respConnectToEcu.connectionHandle);

                    string SystemName;

                    //      For i = 0 To 0 'ODIS_CodingResult.Count - 1 WTF, Mrkni do originalu

                    int l = 0;
                    ODIS_CodingResultItem = ODIS_CodingResult[0].textCoding;
                    _owner.OnLogExecutor("set name: " + ODIS_CodingResult[0].logicalLinkname);

                    SystemName = ODIS_CodingResult[0].systemName;
                    ODIS_WebServer.mapEntryType[] ODIS_EntryMap = new mapEntryType[Codings.Count()];
                    ODIS_mapToCodeBack.entries = ODIS_EntryMap;

                    for (int j = 0; j < ODIS_CodingResultItem.entries.Length; j++)
                    {
                        for (int k = 0; k < Codings.Count(); k++)
                        {
                            if (ODIS_CodingResultItem.entries[j].key.Contains(Codings[k]))
                            {
                                ODIS_mapToCodeBack.entries[l] = new ODIS_WebServer.mapEntryType();
                                ODIS_mapToCodeBack.entries[l].key = ODIS_CodingResultItem.entries[j].key;
                                ODIS_mapToCodeBack.entries[l].value = Values[k];
                                l++;
                            }


                        }
                    }

                    ODIS_WebServer.diagResultImpl CodingResult = new ODIS_WebServer.diagResultImpl();
                    CodingResult = ODIS_diag.writeTextCoding(ODIS_respConnectToEcu.connectionHandle, SystemName, ODIS_mapToCodeBack, true);

                    // tady nejaka checking show coded parametres smycka

                    for (int c = 0; c < Codingfound.Count(); c++)
                    {
                        if (!Codingfound[c])
                            _owner.OnLogExecutor("WARNING - parameter: " + " [" + Codings[c] + "] " +
                         "not present int the ECU Coding register");
                        else
                            _owner.OnLogExecutor("Coded: " + "[" + Codings[c] + "] = " + Values[c]);
                    }


                    ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);


                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    ODIS_diag.switchSession(ODIS_respConnectToEcu.connectionHandle, "DiagnServi_DiagnSessiContrDevelSessi");
                    if (!(ODIS_userID.Length == 0 || ODIS_pwd.Length == 0))
                        ODIS_diag.securityAccess(ODIS_respConnectToEcu.connectionHandle, ODIS_pwd, ODIS_userID);

                    // CHECK ODIS CODING
                    ODIS_CodingResult = ODIS_diag.readCoding(ODIS_respConnectToEcu.connectionHandle);
                    // a nejakej spicenej checking algo




                    ODIS_CodingResultItem = ODIS_CodingResult[0].textCoding;
                    //_owner.OnLogExecutor("set name: " + ODIS_CodingResult[0].logicalLinkname);

                    //SystemName = ODIS_CodingResult[0].systemName;
                    //ODIS_WebServer.mapEntryType[] ODIS_EntryMap = new mapEntryType[Codings.Count()];
                    //ODIS_mapToCodeBack.entries = ODIS_EntryMap;
                    _owner.OnLogExecutor("Check coded values:");
                    for (int j = 0; j < ODIS_CodingResultItem.entries.Length; j++)
                    {
                        for (int k = 0; k < Codings.Count(); k++)
                        {
                            if (ODIS_CodingResultItem.entries[j].key.Contains(Codings[k]))
                            {


                                _owner.OnLogExecutor("Coded: " + "[" + Codings[k] + "] = " + ODIS_CodingResultItem.entries[j].value);

                                //ODIS_mapToCodeBack.entries[l] = new ODIS_WebServer.mapEntryType();
                                //ODIS_mapToCodeBack.entries[l].key = ODIS_CodingResultItem.entries[j].key;
                                //ODIS_mapToCodeBack.entries[l].value = Values[k];
                                //l++;
                            }


                        }
                    }






                    ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);


                }
                catch (Exception ex)
                {
                    _owner.OnLogExecutor("Error: Exception raised while communication with ODIS WebServer: " + ex.Message);

                }








                return "OK";
            }




            public string ODIS_SetAdapt(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                if (Paramlist.Count < 3)
                    return "Not enough parameters! You need at least 3 (Project, ECU,  Adaptation parameters)";

                string[] Adaptations = new string[Paramlist.Count - 2];
                string[] Values = new string[Paramlist.Count - 2];
                bool[] Codingfound = new bool[Paramlist.Count - 2];

                var project = Paramlist.First(item => item.Name.ToLower() == "project").Value;
                var ECU = Paramlist.First(item => item.Name.ToLower() == "ecu").Value;

                //var inputDTC = Paramlist.First(item => item.Name.ToLower() == "dtc").Value;


                for (int i = 2; i < Paramlist.Count; i++)
                {
                    Adaptations[i - 2] = (string)Paramlist[i].Name;
                    Values[i - 2] = (string)Paramlist[i].Value;
                    Codingfound[i - 2] = true;

                }
                //htmlelement phuck;
                ODIS_WebServer.DiagnosticInterfaceImplClient ODIS_diag = new DiagnosticInterfaceImplClient("DiagnosticInterfaceImplPort", "http://127.0.0.1:8081/OdisAutomationService?wsdl");
                ODIS_WebServer.diagResultConnectEcuImpl ODIS_respConnectToEcu = new diagResultConnectEcuImpl();
                ODIS_WebServer.diagResultAdaptationImpl ODIS_AdaptionResult = new diagResultAdaptationImpl();
                ODIS_WebServer.mapAdapter ODIS_mapToCodeBack = new ODIS_WebServer.mapAdapter();
                ODIS_WebServer.mapAdapter ODIS_mapToAdapt = new ODIS_WebServer.mapAdapter();


                string nameFamily_tmp = string.Empty;

                string value_tmp = string.Empty;
                string[] _nameFamily = new string[2];

                string ODIS_userID = string.Empty;
                string ODIS_pwd = string.Empty;

                ODIS_userID = "Login";
                ODIS_pwd = "20103";

                // int n_measurement = 1; // number of parameters myslim

                try
                {
                    ODIS_diag.setVehicleProject((string)project);
                    ODIS_diag.configureSetting("Multilink.MaxNumberOfLogicalLinks", "1");


                    // 005f
                    int ECUADRESS = Int32.Parse((string)ECU, System.Globalization.NumberStyles.HexNumber);
                    ODIS_respConnectToEcu = ODIS_diag.connectToEcuAndOpenConnection(ECUADRESS);
                    _owner.OnLogExecutor("Connection with ECU: " + ODIS_respConnectToEcu.ecuName + " established.");

                    ODIS_diag.switchSession(ODIS_respConnectToEcu.connectionHandle, "DiagnServi_DiagnSessiContrDevelSessi");

                    if (!(ODIS_userID.Length == 0 || ODIS_pwd.Length == 0))
                        ODIS_diag.securityAccess(ODIS_respConnectToEcu.connectionHandle, ODIS_pwd, ODIS_userID);

                    ODIS_WebServer.mapEntryType[] ODIS_EntryMap = new mapEntryType[Adaptations.Count()];
                    ODIS_mapToAdapt.entries = ODIS_EntryMap;

                    for (int i = 2; i < Paramlist.Count; i++)
                    {
                        if (!Paramlist[i].Name.Contains('@'))
                            return "Error, parameter name isn't in correct format (Family@name)";
                    }

                    for (int i = 2; i < Paramlist.Count; i++)
                    {
                        _nameFamily = Paramlist[i].Name.Split('@');

                        ODIS_AdaptionResult = ODIS_diag.readAdaptation(ODIS_respConnectToEcu.connectionHandle, _nameFamily[0]);

                        ODIS_mapToAdapt.entries[i - 2] = new ODIS_WebServer.mapEntryType();
                        ODIS_mapToAdapt.entries[i - 2].key = _nameFamily[1];
                        ODIS_mapToAdapt.entries[i - 2].value = Values[i - 2];

                        ODIS_diag.writeAdaptation(ODIS_respConnectToEcu.connectionHandle, _nameFamily[0], ODIS_mapToAdapt);
                        ODIS_diag.closeConnection(ODIS_respConnectToEcu.connectionHandle);

                    }


                }
                catch (Exception ex)
                {
                    return ("Error: Exception raised while communication with ODIS WebServer: " + ex.Message);


                }

                return "OK";
            }

            private WebBrowser mybrowser;
            static IWebDriver driver;


            public string oecon_login(object[] parameters, object[] oparameter)
            {


                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();


                try
                {

                

               
                string browserType = Paramlist.Find(a => a.Name.ToLower() == "browser").Value.ToString().ToLower();

                FirefoxDriverService service = FirefoxDriverService.CreateDefaultService();
               InternetExplorerDriverService ieservice = InternetExplorerDriverService.CreateDefaultService();

               
                
               
                service.HideCommandPromptWindow = true;
                ieservice.HideCommandPromptWindow = true;

                //var options = new InternetExplorerOptions();
                //options.AddAdditionalCapability("window-position", "-333,-333");

                //    AddArgument("--window-position=-32000,-32000");

                //   var driver = new ChromeDriver(service, options);
                // driver.Navigate().GoToUrl("https://www.google.co.uk");


                if (browserType.Equals("ff"))
                {
                    driver = new FirefoxDriver(service);
                }
                else
                {
                    driver = new InternetExplorerDriver(ieservice);

                }


                    

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Int32.Parse(Paramlist.Find(a => a.Name.ToLower() == "timeout").Value.ToString()));
                driver.Navigate().GoToUrl(Paramlist.Find(a => a.Name.ToLower() == "url").Value.ToString());
                IWebElement element = driver.FindElement(By.Id("user"));
                element.Clear();
                element.SendKeys(Paramlist.Find(a => a.Name.ToLower() == "login").Value.ToString());
                element = driver.FindElement(By.Name("j_password"));
                element.Clear();
                element.SendKeys(Paramlist.Find(a => a.Name.ToLower() == "password").Value.ToString());
                element = driver.FindElement(By.ClassName("btn-primary"));
                element.Submit();
                
                return "OK";
                }
                catch (Exception e)
                {
                    return (e.ToString());
                }
            }

            

                 public string OECON_Hangup(object[] parameters, object[] oparameter)
            {


                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();


                try
                {

                    driver.Navigate().GoToUrl(Paramlist.Find(a => a.Name.ToLower() == "url").Value.ToString());
                    IWebElement dispatchtable = driver.FindElement(By.Id("callListTable"));

                    IList<IWebElement> rows = dispatchtable.FindElements(By.TagName("tr"));
                    bool found = false;
                    foreach (IWebElement element in rows)
                    {
                        if (element.Text.Contains(Paramlist.Find(a => a.Name.ToLower() == "number").Value.ToString()))
                        {

                            element.Click();
                            found = true;
                            break;

                        }
                    }

                    if (!found)
                        return "Selected Dispatch Number has not been found";
                    IWebElement button = driver.FindElement(By.XPath("//*[contains(text(), 'Hang up')]"));

                    button.Submit();


                    return "OK";
                }
                catch (Exception e)
                {
                    return (e.ToString());
                }
            }



            // LUK: Reads out the first page of the records table
            // https://ecalldev02.comloc.net/devserver/call/index
            public string oecon_loghistory(object[] parameters, object[] oparameter)
            {

                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                driver.Navigate().GoToUrl(Paramlist.Find(a => a.Name.ToLower() == "url").Value.ToString());
                string info = String.Empty;
                IList<IWebElement> rows = driver.FindElements(By.XPath("//tr[@class='odd' or @class='even']"));
                foreach (IWebElement child in rows)
                {
                    //  info += child.Text + " \n";
                    IList<IWebElement> content = child.FindElements(By.TagName("td"));
                    String text = child.GetAttribute("innerHTML");
                    //   IList<IWebElement> content = child.FindElements(By.XPath("//td"));



                    text = ("Begin: " + content[1].Text + "\n" );
                    text += ("Duration: " + content[2].Text + "\n" );
                    text += ("I/O: " + content[3].GetAttribute("innerHTML") + "\n" );
                    text += ("External Subscriber: " + content[4].Text + "\n" );
                    text += ("Internal Subscriber: " + content[5].Text + "\n" );
                    text += ("# of MSDs: " + content[6].Text + "\n" );
                    _owner.OnLogExecutor(text);



                    //foreach(IWebElement innerdata in cells)
                    //{
                    // //   _owner.OnLogExecutor(innerdata.Text +  " ");
                    //}
                    //string[] cell;

                    //cell = child.Text.Split(null);

                    //_owner.OnLogExecutor(child.Text + Environment.NewLine);



                }

                driver.Close();
                return "OK";
            }


            public string OECON_dispatchLOG(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                driver.Navigate().GoToUrl(Paramlist.Find(a => a.Name.ToLower() == "url").Value.ToString());

                IWebElement dispatchtable = driver.FindElement(By.Id("callListTable"));

                IList<IWebElement> rows = dispatchtable.FindElements(By.TagName("tr"));
                bool found = false;
                foreach(IWebElement element in rows)
                {
                    if(element.Text.Contains(Paramlist.Find(a => a.Name.ToLower() == "number").Value.ToString()))
                    {
                       
                            element.Click();
                        found = true;
                        break;
                      
                    }
                }

                if (!found)
                    return "Selected Dispatch Number has not been found";




              //  rows[1].Click();

                IWebElement logtable = driver.FindElement(By.Id("logs-table"));

                //LUK: I implemented tbody, because it use to hapen that only header (first tab - row) has been loaded and no content cells
                IWebElement tbody = logtable.FindElement(By.TagName("tbody"));
                IList<IWebElement> logs = tbody.FindElements(By.TagName("tr"));

                for(int i=1; i<logs.Count;i++)
                {
                    _owner.OnLogExecutor(logs[i].Text + Environment.NewLine);
                }

                return "OK";
            }




            public string OECON_GETParam(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                //driver.Navigate().GoToUrl("https://ecalldev02.comloc.net/devserver/dispatch/index");


                driver.Navigate().GoToUrl(Paramlist.Find(a => a.Name.ToLower() == "url").Value.ToString());
                IWebElement dispatchtable = driver.FindElement(By.Id("callListTable"));

                IList<IWebElement> rows = dispatchtable.FindElements(By.TagName("tr"));
                bool found = false;
                foreach (IWebElement element in rows)
                {
                    if (element.Text.Contains(Paramlist.Find(a => a.Name.ToLower() == "number").Value.ToString()))
                    {

                        element.Click();
                        found = true;
                        break;

                    }
                }

                if (!found)
                    return "Selected Dispatch Number has not been found";




                IList<IWebElement> data = driver.FindElements(By.TagName("input"));


                /// sofar param names are case sensitive!!!
                foreach (IWebElement element in data)
                {
                    if (element.GetAttribute("name").Contains(Paramlist.Find(a => a.Name.ToLower() == "parameter").Value.ToString()))
                    {




                        // nejdriv naser podminku ktera votestuje diesel gasoline ntural liquid electric hydrogen other 
                        // a pak jen zkontroluj if element.selected ...potom napis ja a kdyz ne tak nein 
                        if (element.GetAttribute("name").Contains("Tank") || element.GetAttribute("name").Contains("Gas") || element.GetAttribute("name").Contains("Storage"))
                        {
                            String content = element.GetAttribute("name") + ": ";

                            if (element.Selected)
                                content += "selected";
                            else
                                content += "not selected";
                            _owner.OnLogExecutor(content);

                        }
                        else
                        {

                            // musi se nejak predelat abych pouzival ty variable primo...mrknout pristi tyden
                            string name = element.GetAttribute("name");
                            string value = element.GetAttribute("value");
                            _owner.OnLogExecutor(name + ": " + value);
                            Debug.WriteLine(name + ": " + value);

                            //IJavaScriptExecutor js = (IJavaScriptExecutor)element;

                            //// String styleAttribute = (String)js.ExecuteScript("return element.value", element);

                            //string styleattribute = (string)js.ExecuteScript("return value");
                        }

                        

                        Fct_Parameter output = new Fct_Parameter();
                        output.Name = element.GetAttribute("name");
                        output.Value = element.GetAttribute("value");
                        OutputParameters.Add(output);
                      //  return "OK";

                    }

                }

                if (OutputParameters.Count == 0)
                    return "Parameter not found";
                else
                    return "OK";
            }

           


                 public string OECON_GETStatus(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                //driver.Navigate().GoToUrl("https://ecalldev02.comloc.net/devserver/dispatch/index");


                driver.Navigate().GoToUrl(Paramlist.Find(a => a.Name.ToLower() == "url").Value.ToString());
                IWebElement dispatchtable = driver.FindElement(By.Id("callListTable"));

                IList<IWebElement> rows = dispatchtable.FindElements(By.TagName("tr"));
                bool found = false;
                string status = string.Empty;

                foreach (IWebElement element in rows)
                {
                    if (element.Text.Contains(Paramlist.Find(a => a.Name.ToLower() == "number").Value.ToString()))
                    {
                        if (element.Text.Contains("ENDED"))
                            status = "ended";
                        else
                            status = "active";
                       
                        found = true;
                        break;

                    }
                }

                if (!found)
                    return "Selected Dispatch Number has not been found";






                Fct_Parameter output = new Fct_Parameter();
                output.Name = "status";
                output.Value = status;
                OutputParameters.Add(output);
                return "OK";

            }



            public string Comparator(object[] parameters, object[] oparameter)
            {
                Fct_Parameter output = new Fct_Parameter();
                output.Name = "Comparison";
                output.Type = "String";
                output.Value = "undefined";

                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                List<Fct_Parameter> outParamlist = oparameter.Cast<Fct_Parameter>().ToList();

                
                string pattern = "<=|>=|>|<|=";
                foreach(Fct_Parameter input in Paramlist)
                {

                    Fct_Parameter paramfound = outParamlist.First(stringtocheck => stringtocheck.Name.ToLower().Contains(input.Name.ToLower()));

                    if (paramfound != null)
                    {
                        foreach (Match match in Regex.Matches(input.Value.ToString(), pattern))
                        {

                          //  if (Regex.Matches)


                            switch (match.Value.ToString())
                            {
                                case ">=":
                                    break;
                                case "<=":
                                    // string content = data.Trim('<', '=');


                                    break;

                                case "<":
                                    break;

                                case ">":
                                    break;

                                case "=":

                                    string content = input.Value.ToString().Trim('=');
                                    int number;

                                    if (int.TryParse(content, out number))
                                    {
                                        if (number == Int32.Parse(paramfound.Value.ToString()))
                                        {
                                            output.Value = "True";
                                            break;
                                        }
                                        else
                                        {
                                            output.Value = "False";
                                            break;
                                        }

                                    }
                                    else
                                    {
                                        // content is probably string 
                                        if(content.ToLower().Equals(paramfound.Value.ToString().ToLower()))
                                        {
                                            output.Value = "True";
                                            _owner.OnLogExecutor(input.Name.ToString().ToLower() + "= "+ content.ToLower());
                                        }
                                        else
                                        {
                                            output.Value = "False";
                                            _owner.OnLogExecutor(input.Name.ToString().ToLower() + "= " + content.ToLower());
                                        }




                                      //  do some textual parsing
                                    }

                                    break;

                                default:
                                    return "No compatible comparison operators found, have you tried one of these? [<,>,<=,>=]";
                                   
                            }




                        }
                    }
                    else
                        return "Parameter " + input.Name + " has not been found";
                    
                }




                
               // output.Value = "False";




                OutputParameters.Add(output);

                return "OK";
            }

            public string Success(object[] parameters, object[] oparameter)
            {


                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                if (driver != null)
                    driver.Quit();

                _owner.OnLogExecutor("SUCCESS");



                return "OK";
            }

            public string Fail(object[] parameters, object[] oparameter)
            {


                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                if (driver != null)
                    driver.Quit();

                _owner.OnLogExecutor("FAIL");
                return "OK";
            }

            public string Wait(object[] parameters, object[] oparameter)
            {
                //_owner.dispatcherTimer.Interval = new TimeSpan(0, 
                //    0, 
                //    10);
                //dispatcherTimer.Tick += (sender, e) => dispatcherTimer_Tick(sender, timerargs);
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();
                
                _owner.dispatcherTimer.Interval = new TimeSpan(
                    Int32.Parse(Paramlist.Find(a => a.Name.ToLower() == "hours").Value.ToString()),
                    Int32.Parse(Paramlist.Find(a => a.Name.ToLower() == "minutes").Value.ToString()),
                    Int32.Parse(Paramlist.Find(a => a.Name.ToLower() == "seconds").Value.ToString()));

                _owner.dispatcherTimer.Start();
              //  _owner.OnLogExecutor("FAIL");
                return "OK";
            }


            #region ThreeBM

            public string ThreeBM(object[] parameters, object[] oparameter)
            {
                List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();

                try
                { 

                string status;
                UInt32 ftdiDeviceCount = 0;
                FTDI.FT_STATUS ftStatus = FTDI.FT_STATUS.FT_OK;

                // Create new instance of the FTDI device class
                FTDI myFtdiDevice = new FTDI();


                ftStatus = myFtdiDevice.GetNumberOfDevices(ref ftdiDeviceCount);

                if (ftStatus != FTDI.FT_STATUS.FT_OK)
                {
                    // Wait for a key press
                    status = ("Failed to get number of devices (error " + ftStatus.ToString() + ")");

                    return status;
                }

                if (ftdiDeviceCount == 0)
                {
                    // Wait for a key press
                    // Console.WriteLine("Failed to get number of devices (error " + ftStatus.ToString() + ")");
                    status = ("Failed to get number of devices (error " + ftStatus.ToString() + ")");
                    // Console.ReadKey();
                    return status;
                }

                ftStatus = myFtdiDevice.OpenBySerialNumber("A505FR1F");

                if (ftStatus != FTDI.FT_STATUS.FT_OK)
                {
                    // Wait for a key press
                    status = ("Failed to open device (error " + ftStatus.ToString() + ")");

                    return status;
                }

                ftStatus = myFtdiDevice.SetBitMode(255, 0x01);

                if (ftStatus != FTDI.FT_STATUS.FT_OK)
                {
                    // Wait for a key press
                    status = (" Setting Bitmode didn't work (error " + ftStatus.ToString() + ")");

                    return status;
                }


                // LUK:  in Value is the previous state stored
                byte[] Value = new byte[1];
                uint Bytesread = 0;
                ftStatus = myFtdiDevice.Read(Value, 1, ref Bytesread);
                Console.WriteLine("READING (error " + ftStatus.ToString() + ")"
                    + "ARRAY" + Value[0] + "bytes read " + Bytesread);



               
                string oldNumber = Convert.ToString(Value[0], 2).PadLeft(8, '0');
                char[] oldNumberchars = oldNumber.ToCharArray();

                string newNumber = Paramlist.First<Fct_Parameter>().Value.ToString();
                char[] newNumberchars = newNumber.ToCharArray();

                //for (int i = 0; i < newNumberchars.Length; i++)
                //{
                //    if (newNumberchars[i].Equals('-'))
                //    {
                //        newNumberchars[i] = oldNumberchars[i];
                //    }
                //}

                    for (int i = 1; i <= newNumberchars.Length; i++)
                    {
                        if (newNumberchars[newNumberchars.Length - i].Equals('-'))
                        {
                            newNumberchars[newNumberchars.Length - i] = oldNumberchars[oldNumberchars.Length - i];
                        }
                    }

                    string FinalNumber = new string(newNumberchars);
                byte FinalNumberAsByte = Convert.ToByte(FinalNumber, 2); // 104
                Value[0] = FinalNumberAsByte;
                uint BytesWritten = 0;

                ftStatus = myFtdiDevice.Write(Value, 1, ref BytesWritten);
                if (ftStatus != FTDI.FT_STATUS.FT_OK)
                {
                    // Wait for a key press
                    status = ("Writing didnt pass (error " + ftStatus.ToString() + ")");
                    return status;
                }

                ftStatus = myFtdiDevice.Close();

                    //   MessageBox.Show("OHAI");



                    //if (Paramlist.Find(a => a.Name.ToLower() == "minutes").Value.ToString() != "0"
                    // ||
                    // Paramlist.Find(a => a.Name.ToLower() == "seconds").Value.ToString() != "0")
                    //{

                    
                    //    _owner.dispatcherTimer.Interval = new TimeSpan(
                    //Int32.Parse("0"),
                    //Int32.Parse(Paramlist.Find(a => a.Name.ToLower() == "minutes").Value.ToString()),
                    //Int32.Parse(Paramlist.Find(a => a.Name.ToLower() == "seconds").Value.ToString()));

                    //_owner.dispatcherTimer.Start();
                    //}





                    return "OK";
            }
                catch(Exception e)
                {
                    return e.ToString();
                }

            }

            #endregion



            //public string ThreeBMrelease(object[] parameters, object[] oparameter)
            //{
            //    List<Fct_Parameter> Paramlist = parameters.Cast<Fct_Parameter>().ToList();


            //    try
            //    {

            //        string status;
            //        UInt32 ftdiDeviceCount = 0;
            //        FTDI.FT_STATUS ftStatus = FTDI.FT_STATUS.FT_OK;

            //        // Create new instance of the FTDI device class
            //        FTDI myFtdiDevice = new FTDI();


            //        ftStatus = myFtdiDevice.GetNumberOfDevices(ref ftdiDeviceCount);

            //        if (ftStatus != FTDI.FT_STATUS.FT_OK)
            //        {
            //            // Wait for a key press
            //            status = ("Failed to get number of devices (error " + ftStatus.ToString() + ")");

            //            return status;
            //        }

            //        if (ftdiDeviceCount == 0)
            //        {
            //            // Wait for a key press
            //            // Console.WriteLine("Failed to get number of devices (error " + ftStatus.ToString() + ")");
            //            status = ("Failed to get number of devices (error " + ftStatus.ToString() + ")");
            //            // Console.ReadKey();
            //            return status;
            //        }

            //        ftStatus = myFtdiDevice.OpenBySerialNumber("A505FR1F");

            //        if (ftStatus != FTDI.FT_STATUS.FT_OK)
            //        {
            //            // Wait for a key press
            //            status = ("Failed to open device (error " + ftStatus.ToString() + ")");

            //            return status;
            //        }

            //        ftStatus = myFtdiDevice.SetBitMode(255, 0x01);

            //        if (ftStatus != FTDI.FT_STATUS.FT_OK)
            //        {
            //            // Wait for a key press
            //            status = (" Setting Bitmode didn't work (error " + ftStatus.ToString() + ")");

            //            return status;
            //        }


            //        // LUK:  in Value is the previous state stored
            //        byte[] Value = new byte[1];
            //        uint Bytesread = 0;
            //        ftStatus = myFtdiDevice.Read(Value, 1, ref Bytesread);
            //        Console.WriteLine("READING (error " + ftStatus.ToString() + ")"
            //            + "ARRAY" + Value[0] + "bytes read " + Bytesread);

            //        string oldNumber = Convert.ToString(Value[0], 2).PadLeft(8, '0');
            //        char[] oldNumberchars = oldNumber.ToCharArray();

            //        string newNumber = Paramlist.First<Fct_Parameter>().Value.ToString();

            //        char[] newNumberchars = newNumber.ToCharArray();

            //        //for (int i = 0; i < newNumberchars.Length; i++)
            //        //{
            //        //    if (newNumberchars[i].Equals('1'))
            //        //    {
            //        //        newNumberchars[i] = '0';
            //        //    }
            //        //    if (newNumberchars[i].Equals('-'))
            //        //    {
            //        //        newNumberchars[i] = oldNumberchars[i];
            //        //    }
            //        //}
            //        //for (int i = newNumberchars.Length - 1; i >= 0; i--)
            //        //{

            //        //}

            //        for (int i = 1; i <= newNumberchars.Length; i++)
            //        {
            //            if (newNumberchars[newNumberchars.Length-i].Equals('1'))
            //            {
            //                newNumberchars[newNumberchars.Length - i] = '0';
            //            }
            //            else if (newNumberchars[newNumberchars.Length - i].Equals('-'))
            //            {
            //                newNumberchars[newNumberchars.Length - i] = oldNumberchars[oldNumberchars.Length-i];
            //            }
            //        }


            //        string FinalNumber = new string(newNumberchars);
            //        byte FinalNumberAsByte = Convert.ToByte(FinalNumber, 2); // 104
            //        Value[0] = FinalNumberAsByte;
            //        uint BytesWritten = 0;

            //        ftStatus = myFtdiDevice.Write(Value, 1, ref BytesWritten);
            //        if (ftStatus != FTDI.FT_STATUS.FT_OK)
            //        {
            //            // Wait for a key press
            //            status = ("Writing didnt pass (error " + ftStatus.ToString() + ")");
            //            return status;
            //        }

            //        ftStatus = myFtdiDevice.Close();

            //        //   MessageBox.Show("OHAI");




                  






            //        return "OK";
            //    }
            //    catch (Exception e)
            //    {
            //        return e.ToString();
            //    }

            //}

        }




    }
}
