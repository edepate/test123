﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;

namespace DiagramDesigner
{
    public partial class Window1 : Window
    {
        TC_Executor obj_TC_Executor;

        WebBrowser windowbrowser;



        public Window1()
        {
            InitializeComponent();
            // Lukas_tests test = new Lukas_tests();
            //   LoggingWindow.AppendText("blabla");
         //   windowbrowser = browser;

        }
        //public WebBrowser mainbrowser
        //{
        //    get { return browser; }
        //}
        public void OnEvFunctionsFinalized(object sender, EventArgs e)
        {
            MyDebuggerExecutor.evExecuteFunction -= obj_TC_Executor.OnEvExecuteFunction;
            obj_TC_Executor.evFunctionExecuted -= MyDebuggerExecutor.OnEvFunctionExecuted;
            MyDebuggerExecutor.evFinalizeFunctions -= OnEvFunctionsFinalized;

            obj_TC_Executor.dispatcherTimer.Tick -= obj_TC_Executor.dispatcherTimer_Tick;
            obj_TC_Executor = null;
            GC.Collect();
        }


        private void TextBlock_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MyDebuggerExecutor.loggingWindow.Document.Blocks.Clear();
            MyDebuggerExecutor.AddLogLine("Starting execution, please wait...");
            //Instantiate the object that contains the Fkt definitions
            obj_TC_Executor = new TC_Executor(); 

            // LUK: Two Events: Classname.eventhandlerName


            MyDebuggerExecutor.evExecuteFunction += obj_TC_Executor.OnEvExecuteFunction;
            obj_TC_Executor.evFunctionExecuted += MyDebuggerExecutor.OnEvFunctionExecuted;
            MyDebuggerExecutor.evFinalizeFunctions += OnEvFunctionsFinalized;

            obj_TC_Executor.dispatcherTimer.Tick += obj_TC_Executor.dispatcherTimer_Tick;


            obj_TC_Executor.evLogExecutor += MyDebuggerExecutor.OnLogExecutor;
            // obj_TC_Executor.evLogExecutor += MyDebuggerExecutor.
            //MyDebuggerExecutor.log

            // obj_TC_Executor.evLogAdded += MyDebuggerExecutor.OnLogAdded;
            MyDebuggerExecutor.InitializeTCExecution();

            

         //  obj_TC_Executor.LogAdded +=





        }

        private void LoggingWindow_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {

            // MessageBox.Show("PHUCK");
            //this.InvalidateVisual();
            //if(MyDebuggerExecutor != null)
            //MyDebuggerExecutor.InvalidateVisual();
            this.Refreshh();
            
         //   this.refre
        }

        private void loadFctDefinition_Click(object sender, RoutedEventArgs e)
        {
            

            XElement root = null;
            try
            {
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\FunctionCatalog.xml";
                root = XElement.Load(path);
            }
            catch (Exception exc)
            {
                    MessageBox.Show(exc.StackTrace, exc.Message, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (root == null)
                return;

            //loop over all functions
            IEnumerable<XElement> itemsXML = root.Elements("functionList").Elements("Fkt_functionDefinition");
            bool category_found; //points to the category of the Fct, whatever if a new has been created, or already existing
            foreach (XElement itemXML in itemsXML)
            {
                string xml_category = itemXML.Element("category").Value;

                //check if category is already present.If not create a new header
                #region 
                category_found = false;
                Expander current_category = null;
                foreach (Control item in this.toolPalette.Children)
                    if (item is Expander)
                        if ((string)(item as Expander).Header == xml_category)
                        {
                            category_found = true;
                            current_category = (Expander)item;
                        }
                
                //create category's header if not existing
                if (!category_found)
                {
                    Expander newCat = new Expander();
                    newCat.Header = xml_category;
                    newCat.IsExpanded = true;
                    object content = this.FindResource("FctDef_category_style1");
                    
                    newCat.Content = content;
                    current_category = newCat;
                    this.toolPalette.Children.Add(newCat);
                }
                #endregion

                //add the icon
                string icon = itemXML.Element("icon").Value;
                System.Windows.Shapes.Path newPath = new System.Windows.Shapes.Path();
                var converter = TypeDescriptor.GetConverter(typeof(Geometry));
                newPath.Data = (Geometry)converter.ConvertFrom(icon);
                
                //add to .Tag the xml function definition
                newPath.Tag = itemXML.ToString();
                newPath.Style = (this.FindResource("DefaultStyle") as Style);
                
                //add item to the Expander, finally
                newPath.Name = itemXML.Element("name").Value;
                (current_category.Content as Toolbox).Items.Add(newPath);

            }

        }

        private object Uri(object iconPath)
        {
            throw new NotImplementedException();
        }
    }
}
