﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DiagramDesigner
{
    public class DesignerItem_Fct : DesignerItem
    {
        public DesignerItem_Fct() : base()
        {
            InputParameters = new ObservableCollection<Fct_inParameter>();
            ReturnParameters = new ObservableCollection<Fct_Parameter>();
            AdditionalOutputs = new ObservableCollection<Parent_name_ID>();

            //RUG: each time an element is added/deleted, the event is raised
            InputParameters.CollectionChanged += InputParameters_CollectionChanged;
        }
        public DesignerItem_Fct(Guid id) : base(id)
        {
            InputParameters = new ObservableCollection<Fct_inParameter>();
            ReturnParameters = new ObservableCollection<Fct_Parameter>();
            AdditionalOutputs = new ObservableCollection<Parent_name_ID>();

            //RUG: each time an element is added/deleted, the event is raised
            InputParameters.CollectionChanged += InputParameters_CollectionChanged;
        }

        #region InputParameters
        public ObservableCollection<Fct_inParameter> InputParameters
        {
            get { return (ObservableCollection<Fct_inParameter>)GetValue(inputParametersProperty); }
            set { SetValue(inputParametersProperty, value); }
        }
        public static readonly DependencyProperty inputParametersProperty =
            DependencyProperty.Register("InputParameters", typeof(ObservableCollection<Fct_inParameter>), typeof(DesignerItem_Fct));
        #endregion

        #region ReturnParameters
        public ObservableCollection<Fct_Parameter> ReturnParameters
        {
            get { return (ObservableCollection<Fct_Parameter>)GetValue(ReturnParametersProperty); }
            set { SetValue(ReturnParametersProperty, value); }
        }
        public static readonly DependencyProperty ReturnParametersProperty =
            DependencyProperty.Register("ReturnParameters", typeof(ObservableCollection<Fct_Parameter>), typeof(DesignerItem_Fct));
        #endregion

        #region DefaultOutputName
        public string DefaultOutputName
        {
            get { return (string)GetValue(DefaultOutputNameProperty); }
            set { SetValue(DefaultOutputNameProperty, value); }
        }
        public static readonly DependencyProperty DefaultOutputNameProperty =
            DependencyProperty.Register("DefaultOutputName", typeof(string), typeof(DesignerItem_Fct));
        #endregion

        #region AdditionalOutputs
        // if more than one out statement flows out from the DesignerItem (IF, DeMultiplexer, ...)
        public class Parent_name_ID : DependencyObject
        {
            public Parent_name_ID(DesignerItem_Fct di, string _name, Guid ID_)
            {
                Parent = di;
                Name = _name;
                ID = ID_;
            }
            public DesignerItem_Fct Parent;
            public string Name { get; set; }
            public Guid ID { get; set; }

        }
        public ObservableCollection<Parent_name_ID> AdditionalOutputs
        {
            get { return (ObservableCollection<Parent_name_ID>)GetValue(AdditionalOutputsProperty); }
            set { SetValue(AdditionalOutputsProperty, value); }
        }
        public static readonly DependencyProperty AdditionalOutputsProperty =
            DependencyProperty.Register("AdditionalOutputs", typeof(ObservableCollection<Parent_name_ID>), typeof(DesignerItem_Fct));
        #endregion

        private void InputParameters_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            /*RUG: each time an element is added to the collection, we handle its PropertyChanged
                   here. In this way the whole collection raises an "OnPropertyChanged" not only
                   if a new element is added/removed, but also if anything in the item itself
                   changed. It's important to catch the ReturnsValue modification, so we can
                   accordingly update the ReturnParameters.
             */
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Fct_Parameter item in e.OldItems)
                {
                    //Removed items
                    item.PropertyChanged -= Handler_inputParam_OnPropChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Fct_Parameter item in e.NewItems)
                {
                    //Added items
                    item.PropertyChanged += Handler_inputParam_OnPropChanged;
                }
            }
        }

        public void Handler_inputParam_OnPropChanged(object sender, PropertyChangedEventArgs e)
        {
            //handles the disabilitation of ExternallyDefined flag: will the associated connection be deleted?
            if (e.PropertyName == "ExternallyDefined")
            {
                Fct_inParameter senderParam = (Fct_inParameter)sender;

                if (senderParam.ExternallyDefined == false)
                {
                    // serach for connector associated to the senderParam
                    Connector senderConnector = null;
                    object connectorDecorator = this.GetTemplateChild_public("PART_ConnectorDecorator");
                    object connectors = ClassTreeHelper.FindVisualChildren<Connector>((Control)connectorDecorator);
                    foreach (Connector element in (IEnumerable<Control>)connectors)
                    {
                        if (element.AssociatedID == senderParam.ID)
                        {
                            senderConnector = element;
                            break;
                        }
                    }

                    // if user does not confirm, ExternallyDefined is set back to true
                    if (senderConnector.Connections.Count != 0)
                        if (MessageBox.Show("Do you really want to proceed?",
                                        "The parameter has an association",
                                        MessageBoxButton.YesNo,
                                        MessageBoxImage.Warning) == MessageBoxResult.No)
                        {
                            this.InputParameters.FirstOrDefault(a => a.ID == senderParam.ID).ExternallyDefined = true;
                            return;
                        }

                    // if confirms, all the associated connections are removed from the input parameter
                    DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;
                    foreach (Connection connection in senderConnector.Connections)
                    {
                        designer.Children.Remove(connection);
                    }
                }
            }
        }

        public object[] OutputParameters { get; set; }

    }
}
