﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DiagramDesigner
{
    /**************************************************
     *  Group Function: is the DesignerItem that
     **************************************************/
    public class DesignerItem_FctGroup : DesignerItem_Fct
    {
        #region SubCanvas
        // contains the blocks in case this DesignerItem groups a set of DesignerItem elements
        private DesignerCanvas_Group subCanvas;
        public DesignerCanvas_Group SubCanvas
        {
            get { return subCanvas; }
            set
            {
                subCanvas = value;

                if (subWindow == null)
                {
                    subWindow = new SubWindowClass();
                    subWindow.SizeToContent = SizeToContent.WidthAndHeight;
                }

                SubWindow.Content = subCanvas;
            }
        }
        #endregion

        #region SubWindow
        private SubWindowClass subWindow;
        public SubWindowClass SubWindow
        {
            get { return subWindow; }
            set
            {
                if (subWindow == null)
                {

                }

                subWindow = value;
            }
        }
        #endregion

        public DesignerItem_FctGroup() : base() { }
        public DesignerItem_FctGroup(Guid id) : base(id) { }

        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (subCanvas == null) return;

            if (subWindow.IsVisible)
                subWindow.Hide();
            else
                subWindow.Show();
        }
    }
}
