﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using DiagramDesigner.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using System.ComponentModel;
using System.Collections.Specialized;

namespace DiagramDesigner
{
    //These attributes identify the types of the named parts that are used for templating
    [TemplatePart(Name = "PART_DragThumb", Type = typeof(DragThumb))]
    [TemplatePart(Name = "PART_ResizeDecorator", Type = typeof(Control))]
    [TemplatePart(Name = "PART_ConnectorDecorator", Type = typeof(Control))]
    [TemplatePart(Name = "PART_ContentPresenter", Type = typeof(ContentPresenter))]

    public class DesignerItem : ContentControl, ISelectable, IGroupable
    {

        #region BlockName
        //RUG: this property stores the name, taken from the Tag attribute from the Stencil Path object. The name is
        //     assigned in the moment the object is dropped in the canvas (see DesignerCanvas.OnDrop)
        public string BlockName
        {
            get { return (string)GetValue(BlockNameProperty); }
            set { SetValue(BlockNameProperty, value); }
        }
        public static readonly DependencyProperty BlockNameProperty =
            DependencyProperty.Register("BlockName", typeof(string), typeof(DesignerItem));
        #endregion

         /*RUG: used to get the nextBlock (execution order).
        */
        public DesignerItem NextDesignerItem
        {
            get
            {
                // LUK: Gets Template child ..gets the child with the name in "" ..I think...or probably gets child of control template with this name
                object ConnectorDecoratorOut = this.GetTemplateChild("PART_ConnectorDecorator");
                if (ConnectorDecoratorOut != null)
                {

                    // LUK: OK SO IT BASICALLY SEARCHES FOR S:CONNECTOR INSTANCE ?
                    Connector ConnectorOut = ((Control)ConnectorDecoratorOut).Template.FindName("Out", (Control)ConnectorDecoratorOut) as Connector;
                    if (ConnectorOut != null)
                    {
                        // RUGMOD: remove Connection list and allow only one connection for each connector
                        if (((Connector)ConnectorOut).Connections.Count == 0) return null;
                        // LUK: This gets reference to Connector of the connection (sink connector)
                        object ConnectorNextIn = ((Connector)ConnectorOut).Connections[0].Sink;
                        if (ConnectorNextIn != null)
                        {
                            // LUK: And this probably gets the block
                            return ((Connector)ConnectorNextIn).ParentDesignerItem;
                        }
                    }
                }

                return null;
            }
        }

        public DependencyObject GetTemplateChild_public(String element)
        {  /*RUG: used as the FrameworkElement.GetTemplateChild is private, and we want to access the
                  decorator from the Fct_parameter class
            */
            var aa = this.GetTemplateChild(element);
            return this.GetTemplateChild(element);
        }

        public DesignerItem getNextDesignerItemCompared(string status)
        {
            
            {
                // LUK: Gets Template child ..gets the child with the name in "" ..I think...or probably gets child of control template with this name
                object ConnectorDecoratorOut = this.GetTemplateChild("PART_ConnectorDecorator");
                if (ConnectorDecoratorOut != null)
                {

                    // LUK: OK SO IT BASICALLY SEARCHES FOR S:CONNECTOR INSTANCE ?
                    Connector ConnectorOut = ((Control)ConnectorDecoratorOut).Template.FindName(status, (Control)ConnectorDecoratorOut) as Connector;
                    if (ConnectorOut != null)
                    {
                        // RUGMOD: remove Connection list and allow only one connection for each connector
                        if (((Connector)ConnectorOut).Connections.Count == 0) return null;
                        // LUK: This gets reference to Connector of the connection (sink connector)
                        object ConnectorNextIn = ((Connector)ConnectorOut).Connections[0].Sink;
                        if (ConnectorNextIn != null)
                        {
                            // LUK: And this probably gets the block
                            return ((Connector)ConnectorNextIn).ParentDesignerItem;
                        }
                    }
                }

                return null;
            }
        }

        // LUK: This peace of code is never used?
        private void GetConnectors(DependencyObject parent, List<Connector> connectors)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child is Connector)
                {
                    connectors.Add(child as Connector);
                }
                else
                    GetConnectors(child, connectors);
            }
        }

        /*
        RUG: define the delegate and the event to throw when we want to execute the Function (described here). 
        public delegate void ExecuteFunctionEventHandler(object source, EventArgs args);
        public event ExecuteFunctionEventHandler ExecuteFunction;
        RUG: from .NET 2.0 we can use the following more compact way, where <> contains all other additional parameters
             other then the source object: */
        public event EventHandler<EventArgs> ExecuteFunction;

        #region ID
        private Guid id;
        public Guid ID
        {
            get { return id; }
        }
        #endregion

        #region ParentID
        public Guid ParentID
        {
            get { return (Guid)GetValue(ParentIDProperty); }
            set { SetValue(ParentIDProperty, value); }
        }
        public static readonly DependencyProperty ParentIDProperty = DependencyProperty.Register("ParentID", typeof(Guid), typeof(DesignerItem));
        #endregion

        #region IsGroup
        public bool IsGroup
        {
            get { return (bool)GetValue(IsGroupProperty); }
            set { SetValue(IsGroupProperty, value); }
        }
        public static readonly DependencyProperty IsGroupProperty =
            DependencyProperty.Register("IsGroup", typeof(bool), typeof(DesignerItem));
        #endregion

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(DesignerItem),
                                       new FrameworkPropertyMetadata(false));
        #endregion
        
        #region DragThumbTemplate Property


        // can be used to replace the default template for the DragThumb
        // RUG: see comment in DesignerItem_Loaded
        public static readonly DependencyProperty DragThumbTemplateProperty =
            DependencyProperty.RegisterAttached("DragThumbTemplate", typeof(ControlTemplate), typeof(DesignerItem));

        // LUK: MAYBE IT GETS BACK TEMPLATE OF THIS OBJECT? BUT WHY?
        // Probalby to get correct drag thumb template for FlowChartStencils.xaml
        public static ControlTemplate GetDragThumbTemplate(UIElement element)
        {
            return (ControlTemplate)element.GetValue(DragThumbTemplateProperty);
        }

        public static void SetDragThumbTemplate(UIElement element, ControlTemplate value)
        {
            element.SetValue(DragThumbTemplateProperty, value);
        }

        #endregion

        #region ConnectorDecoratorTemplate Property

        // can be used to replace the default template for the ConnectorDecorator
        // RUG: in facts, some elements require a rearrangement of the Connectors positions. This is done in the .xaml definition of
        //      the Toolbox elements: FlowChartStencils.xaml, under <s:Toolbox <ItemsControl.Items <Path <s:DesignerItem.ConnectorDecoratorTemplate
        public static readonly DependencyProperty ConnectorDecoratorTemplateProperty =
            DependencyProperty.RegisterAttached("ConnectorDecoratorTemplate", typeof(ControlTemplate), typeof(DesignerItem));

        public static ControlTemplate GetConnectorDecoratorTemplate(UIElement element)
        {
            return (ControlTemplate)element.GetValue(ConnectorDecoratorTemplateProperty);
        }

        public static void SetConnectorDecoratorTemplate(UIElement element, ControlTemplate value)
        {
            element.SetValue(ConnectorDecoratorTemplateProperty, value);
        }

        #endregion

        #region IsDragConnectionOver

        // while drag connection procedure is ongoing and the mouse moves over 
        // this item this value is true; if true the ConnectorDecorator is triggered
        // to be visible, see template
        public bool IsDragConnectionOver
        {
            get { return (bool)GetValue(IsDragConnectionOverProperty); }
            set { SetValue(IsDragConnectionOverProperty, value); }
        }
        public static readonly DependencyProperty IsDragConnectionOverProperty =
            DependencyProperty.Register("IsDragConnectionOver",
                                         typeof(bool),
                                         typeof(DesignerItem),
                                         new FrameworkPropertyMetadata(false));

        #endregion

        static DesignerItem()
        {
            

            // RUG: a static constructor is called once, automatically, when the class is used for the first time.
            // set the key to reference the style for this control
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(DesignerItem), new FrameworkPropertyMetadata(typeof(DesignerItem)));
        }

        public DesignerItem() : this(Guid.NewGuid()) { }

        public DesignerItem(Guid id)
        {
            this.id = id;

            //RUG: each time an element is added/deleted, the event is raised
            //InputParameters.CollectionChanged += InputParameters_CollectionChanged;

            //this.Loaded += new RoutedEventHandler(DesignerItem_Loaded);
            //this.subWindow.ev_OnClosing += SubWindow_ev_OnClosing;
        }
        
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
            DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;

            //var a = this.NextDesignerItem;
            // var foundButton = this.Children.OfType<Button>().Where(x => x.Tag.ToString() == "2").FirstOrDefault();

            // update selection
            if (designer != null)
            {
                if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
                    if (this.IsSelected)
                    {
                        designer.SelectionService.RemoveFromSelection(this);
                    }
                    else
                    {
                        designer.SelectionService.AddToSelection(this);
                    }
                else if (!this.IsSelected)
                {
                    designer.SelectionService.SelectItem(this);
                }
                Focus();
                designer.OnPropertyChanged("FirstSelectedElement");
            }

            e.Handled = false;
        }

        public void DesignerItem_Loaded(object sender, RoutedEventArgs e) //RUG: also if i comment the code works
        {
           
        }
    }
}
