﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;

namespace DiagramDesigner
{


    public static class ExtensionMethods
    {
        private static Action EmptyDelegate = delegate () { };

        public static void Refreshh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }

    public class LogArgs : EventArgs
    {
        public string Content { get; set; }

    }


    // LUK: CLASS ARGS FOR PARAMS 
    public class BlockArgs : EventArgs
    {
        public string Name { get; set; }

        

        public List<Fct_Parameter> Parameters { get; set; }

      //  public List<Fct_Parameter> OutParameters { get; set; }

        public string Status { get; set; }

        public object[] OutputParam;

        public string Log { get; set; }

        //  public BlockArgs()
        // {
        //      Status = "";
        //  }

    }

    public class DebuggerExecutor : Control
    {
       private DesignerItem_Fct blockIterator;

       private BlockArgs blockData;

        public DesignerCanvas designerCanvas
        {
            get { return (DesignerCanvas)GetValue(designerCanvasProperty); }
            set { SetValue(designerCanvasProperty, value); }
        }

        public static readonly DependencyProperty designerCanvasProperty =
            DependencyProperty.Register("designerCanvas", typeof(DesignerCanvas), typeof(DebuggerExecutor));


        public RichTextBox loggingWindow
        {
            get { return (RichTextBox)GetValue(loggingWindowProperty); }
            set { SetValue(loggingWindowProperty, value); }
        }

        public static readonly DependencyProperty loggingWindowProperty =
            DependencyProperty.Register("loggingWindow", typeof(RichTextBox), typeof(DebuggerExecutor));

        //public static RoutedCommand ExecuteDebug = new RoutedCommand();

        public DebuggerExecutor()
        {
            //this.CommandBindings.Add(new CommandBinding(DebuggerExecutor.ExecuteDebug, ExecuteDebug_Executed));
            this.AllowDrop = false;
            Clipboard.Clear();
        }

      

        //public void ExecuteDebug_Executed(object sender, ExecutedRoutedEventArgs e)
        public void InitializeTCExecution()
        {
            /* RUGGUR
            // RUG: if (this.designerCanvas.NumberOfChildren == 0) ... we don't execute because
            //      this condition is already checked as trigger for the button "Execute"
            
            if (this.designerCanvas.Children.OfType<DesignerItem>().Where(x => x.BlockName.ToString() == "Start").Count() == 0)
            {
                AddLogLine("There must be at least 1 \"Start\" blocks");
                return;
            }
            if (this.designerCanvas.Children.OfType<DesignerItem>().Where(x => x.BlockName.ToString() == "Start").Count() > 1)
            {
                AddLogLine("Too many \"Start\" blocks. Only one allowed");
                return;
            }
            DesignerItem Start = this.designerCanvas.Children.OfType<DesignerItem>()
                                     .Where(x => x.BlockName.ToString() == "Start").FirstOrDefault();

            if (Start.NextDesignerItem == null)
            {
                AddLogLine("Start connecion missing. Please connect one block to Start");

                return;
            }

            blockData = new BlockArgs();



            //blockIterator = Start.NextDesignerItem;
            blockIterator = Start.NextDesignerItem;
            // LUK: here we set statically size of output parameters which should be fct parameters.

            //  blockIterator.OutputParameters = new object[10];

            blockData.Name = blockIterator.BlockName;





            List<Fct_Parameter> paramsAsList = blockIterator.InputParameters.ToList();



            //  blockData.Parameters = blockIterator.InputParameters;

            blockData.Parameters = paramsAsList;

          //  blockIterator.OutputParameters = new object[10];
            // blockIterator.OutputParameters[0] = (int)5;
            //   blockData.OutputParam = blockIterator.OutputParameters;




            //blockData.OutputParam[0] = (int)10;
            //  variable[0] = (int)10;

            //  int number = (int)blockIterator.OutputParameters[0];


            //Thread t = new Thread(() => OnEvExecuteFunction(blockData));

            //t.Start();

            OnEvExecuteFunction(blockData);

            //  OnEvExecuteFunction();

            /*    
            (NOTE) the evExecute's handler (in Fkt_impleme) and the evExecuteFinished's handler (here) must be subscribed
                   in Window1.xaml.cs, after InitializeComponent()

            this.ExecuteFkt(DesignerItem Start) <- this method collects all the param. from the Fkt, and then 
                                                   throws the event evExecute. The evargs are the (string)"functionName",
                                                   the inputParam list and the returnParam list. After evExecute has been
                                                   handled (i.e. the Fkt terminated), it fils up the returnParam list and
                                                   throws another event: evExecuteFinished.

            (evExecuteFinished) that is not a part of this function: the DebuggerExecutor handles evExecuteFinished. In
                                the handler it just write the Log data and then launch evExecute (in case the block is not
                                the EndBlock. Morehover it checks which block to execute in case of an IF.

            (InputParam connected with returnParam) the connected inputParam can be read just by following backwards the
                                connectionAdorner, just like for the .NextDesignerItem

            (IDEA) to avoid too many connections in the diagram, we can show the parameterConnections just when the involved
                                DesignerItem is selected
            */
            /*RUGGUR*/
        }

        public void OnEvFunctionExecuted(object sender, BlockArgs e)
        {
            /* RUGGUR
            if(e.Status != null)
            if (!e.Status.Equals("OK"))
            { 
                AddLogLine( "Function " + e.Name + " Returned following Error: " + e.Status);
                OnFinalizeFunctions();
                return;
            }
            List<Fct_Parameter> OutParamlist = new List<Fct_Parameter>();



            //if(e.Name == "ThreeBMrelease")
            //{


            //    blockData.Name = "ThreeBMrelease";
            //    OnEvExecuteFunction(blockData);
            //    return;
            //}
           



            // LUK: OUTP
            // nevim nikde to nepouzivam ale treba se to hodi
            if (e.OutputParam != null)
            {
                OutParamlist = e.OutputParam.Cast<Fct_Parameter>().ToList();
            }
            // TAK to zkousim pouzivat s THREEBM

            //if (e.Name == "ThreeBM")
            //{
            //    if(e.Parameters.Find(a => a.Name.ToLower() == "minutes").Value.ToString() != "0"
            //        ||
            //        e.Parameters.Find(a => a.Name.ToLower() == "seconds").Value.ToString() != "0")

            //        // IF The timer is set to 0 we want to start 3BM in the calssic wait -> without timer
            //    { 
            //    //blockData.Name = "ThreeBMrelease";
                
            //    return;

            //    }
            //}

            string comparator = "False";

            if(e.Name.Equals("Comparator"))
            {
                Fct_Parameter parameter = (Fct_Parameter)e.OutputParam[0];

                comparator = parameter.Value.ToString();
            }
            //blockIterator = blockIterator.NextDesignerItemCompared;


            if (blockIterator.NextDesignerItem == null && blockIterator.getNextDesignerItemCompared(comparator)== null)
            {
             //   AddLogLine(e.Status);
                OnFinalizeFunctions();
                return;
            }



            // Here we copy by deepcopy the contents from functiondefinition outputparam back to our designer item
            if (e.OutputParam != null)
            {
                blockIterator.OutputParameters = new object[e.OutputParam.Count()];
                

               for(int i=0; i<e.OutputParam.Count();i++)
                {

                    Fct_Parameter tobecopied = (Fct_Parameter)e.OutputParam[i];

                    // deepcopy should work, googled it somewhere on stack overflow
                    blockIterator.OutputParameters[i] = tobecopied.DeepCopy();
                }


            }


            //HERERE YOU SHOULD ALSO NULLIFY YOUR E.OUTPUTPARAMETERS SO THAT IT IS EMPTY FOR NEXT ROUND
            // BUT THIS WILL BE DONE NEXT TIME; WHEN WE HAVE METHOD TO ACCESS PREVIOUS DESIGNER ITEM
            // SO NOW I JUST PASS IT BACK TO EXECUTE FUNCTION A NULIFY IT THERE AFTER ASSIGNIG IT TO INOUT


            // hele lukadlo ja si myslim ze ten passing tech parametru nefunguje protoze je passujes a pak ten celej designer item nahrazujes next designer itemem


            if (e.Name.Equals("Comparator"))
            {
                blockIterator = blockIterator.getNextDesignerItemCompared(comparator);
            }
            else
            {
                blockIterator = blockIterator.NextDesignerItem;
            }


              //  blockIterator = blockIterator.NextDesignerItemCompared;

            List<Fct_Parameter> paramsAsList = blockIterator.InputParameters.ToList();


            

                blockData.Name = blockIterator.BlockName;
              //  blockData.OutputParam = null;


                blockData.Parameters = paramsAsList;
                OnEvExecuteFunction(blockData);
            //}
            RUGGUR*/
        }


        public delegate void evFinalizeFunctionsEventHandler(object source, EventArgs data);
        public event evFinalizeFunctionsEventHandler evFinalizeFunctions;


        protected virtual void OnFinalizeFunctions()
        {
            if (evFinalizeFunctions != null)
                evFinalizeFunctions(this, EventArgs.Empty);
        }


        // LUK: ORIGINAL
        // Definition of event/handler that will be handled from TC_Executor
        //   public delegate void evExecuteFunctionEventHandler(object source, EventArgs args);
        //  public event evExecuteFunctionEventHandler evExecuteFunction;


        public delegate void evExecuteFunctionEventHandler(object source, BlockArgs data);
        public event evExecuteFunctionEventHandler evExecuteFunction;


        // LUK: ORIGINAL ONE
        //protected virtual void OnEvExecuteFunction()
        //{
        //    if (evExecuteFunction != null)
        //        evExecuteFunction(this, EventArgs.Empty);
        //}


        protected virtual void OnEvExecuteFunction(BlockArgs args)
        {
            if (evExecuteFunction != null)
                evExecuteFunction(this, args);
        }



        private void GetConnectors(DependencyObject parent, List<Connector> connectors)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child is Connector)
                {
                    connectors.Add(child as Connector);
                }
                else
                    GetConnectors(child, connectors);
            }
        }
       
        public void AddLogLine(string _text)
        {
            loggingWindow.AppendText(_text);
            loggingWindow.AppendText("\u2028"); // Linebreak, not paragraph break
            loggingWindow.ScrollToEnd();
            //loggingWindow.InvalidateProperty();
            //loggingWindow.UpdateDefaultStyle();
            //loggingWindow.UpdateLayout();
            //loggingWindow.InvalidateVisual();
            loggingWindow.Refreshh();
          //f  loggingWindow.InvalidateProperty()
            //  loggingWindowProperty.ValidateValueCallback();
            //.DataContextChanged();
           // Application.DoEvents();
         
           //- loggingWindow.DataContextChanged();
        }

        public void OnLogExecutor(object sender, LogArgs e)
        {
            AddLogLine(e.Content);
        }

        //public void OnLogAdded(object sender, LogArgs e)
        //{
        // //  e.
        //        AddLogLine(e.Content);
        //}


        }
}
