﻿using System.Windows;
using System.Windows.Shapes;

namespace DiagramDesigner
{
    public class DesignerItemStencil : DependencyObject
    //RUG: inheriting from the base class DependencyObject in order to be able to use attached properties
    {
        public Shape designerItemShape
        {
            get { return (Shape)GetValue(designerItemShapeProperty); }
            set { SetValue(designerItemShapeProperty, value); }
        }
        public static readonly DependencyProperty designerItemShapeProperty =
            DependencyProperty.Register("designerItemShape", typeof(Shape), typeof(DesignerItemConfigurator));


    }


}
