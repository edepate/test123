﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Xml;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace DiagramDesigner
{
    public partial class DesignerCanvas : Canvas, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public DesignerCanvas(StackPanel _toolPalette) : this()
        {
            this.ToolPalette = _toolPalette;
        }

        #region ToolPalette
        // needed to access the tool palette. In DeserializeDesignerItem the name of the DesignerItem object
        // is read, and used as key to retrieve the corresponding ToolboxItem
        public StackPanel ToolPalette
        {
            get { return (StackPanel)GetValue(ToolPaletteProperty); }
            set { SetValue(ToolPaletteProperty, value); }

        }
        public static readonly DependencyProperty ToolPaletteProperty =
            DependencyProperty.Register("ToolPalette", typeof(StackPanel), typeof(DesignerCanvas));
        #endregion ToolPalette

        //RUG: added addChild to add the PropertyNotification. This is needed for the TC_Debugger in order to disable
        //     the button "ImportFromDeisgner"
        public void addChild(UIElement child)
        {
            this.Children.Add(child);
            OnPropertyChanged("NumberOfChildren");
        }

        private Point? rubberbandSelectionStartPoint = null;

        private SelectionService selectionService;

        internal SelectionService SelectionService
        {
            get
            {
                if (selectionService == null)
                    selectionService = new SelectionService(this);

                return selectionService;
            }
        }

        // LUK: Triggers only when we select one block
        public DesignerItem FirstSelectedElement
        {
            get
            {
                object a = SelectionService.CurrentSelection.OfType<DesignerItem>();
                if (SelectionService.CurrentSelection.OfType<DesignerItem>().Count() == 1)
                    return SelectionService.CurrentSelection.OfType<DesignerItem>().FirstOrDefault();
                return null;
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {

            base.OnMouseDown(e); // rug: maybe it routes the event also to the base class. If it had not been here, 

            // then the event just dies as soon as this handler is invoked.
            if (e.Source == this)
            {
                // in case that this click is the start of a 
                // drag operation we cache the start point
                this.rubberbandSelectionStartPoint = new Point?(e.GetPosition(this));




                // if you click directly on the canvas all 
                // selected items are 'de-selected'
                SelectionService.ClearSelection();

                // This call cleares func table
                this.OnPropertyChanged("FirstSelectedElement");

                Focus();
                e.Handled = true;
            }
        }

        // LUK: whenever you run your mouse over design canvas

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            // if mouse button is not pressed we have no drag operation, ...
            if (e.LeftButton != MouseButtonState.Pressed)
                this.rubberbandSelectionStartPoint = null;

            // ... but if mouse button is pressed and start
            // point value is set we do have one
            if (this.rubberbandSelectionStartPoint.HasValue)
            {
                // create rubberband adorner
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    RubberbandAdorner adorner = new RubberbandAdorner(this, rubberbandSelectionStartPoint);
                    if (adorner != null)
                    {
                        adornerLayer.Add(adorner);
                    }
                }
            }
            e.Handled = true;
        }

        // LUK: This function is invoked when you drop something onto canvas

        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            DragObject dragObject = e.Data.GetData(typeof(DragObject)) as DragObject;
            if (dragObject != null && !String.IsNullOrEmpty(dragObject.Xaml))
            {
                DesignerItem newItem = null;
                Object content = XamlReader.Load(XmlReader.Create(new StringReader(dragObject.Xaml)));
                
                if (content != null)
                {
                    // RUG: the xaml data taken from the ToolBox will be written to the ContentControl.content (named
                    //      PART_ContentPresenter) of DesignerItem.
                    //      All the other controls of DesignerItem (PART_DragThumb, PART_ResizeDecoratorm, PART_ConnectorDecorator)
                    //      remains untouched and yet associated.

                    newItem = new DesignerItem();
                    XElement newItem_xml = null;

                    if (((FrameworkElement)content).Tag is String)
                    {
                        // gets the xml Fct_definition and assigns name and execution flow connectors (default true, to be adpated)
                        newItem_xml = XElement.Parse((string)((FrameworkElement)content).Tag);

                        // instantiates the proper object
                        string type = newItem_xml.Element("blockType").Value;
                        if (type == null) return;
                        switch (type)
                        {
                            case "function": newItem = new DesignerItem_Fct(); break;
                            case "startEnd": newItem = new DesignerItem_StartEnd(); break;
                            case "groupFunction": newItem = new DesignerItem_FctGroup(); break;
                            case "groupFctConnector": newItem = new DesignerItem_FctGroupConnector(); break;
                            default:
                                MessageBox.Show(string.Format("blockType ({0}) not recognized", type),
                                    "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                        }

                        newItem.BlockName = newItem_xml.Element("name").Value;

                        // we check only if newItem is DesignerItem_Fct or DesignerItem_StartEnd. The other elements
                        // DesignerItem_FctGroup and DesignerItem_FctGroupConnector are never dropped, but only
                        // created in the grouping routine
                        if (newItem is DesignerItem_Fct)
                        {
                            //inputParameters (externallyDefined by default set to false)
                            IEnumerable<XElement> inputParamList = newItem_xml.Elements("parameterList").Elements("parameter");
                            foreach (XElement item in inputParamList)
                            {
                                (newItem as DesignerItem_Fct).InputParameters.Add(
                                    new Fct_inParameter(newItem,
                                                      item.Element("name").Element("value").Value,
                                                      item.Element("type").Element("value").Value,
                                                      item.Element("value").Value,
                                                      false));
                            }

                            //returnParameters
                            IEnumerable<XElement> outputParamList = newItem_xml.Elements("returnParameterList").Elements("parameter");
                            foreach (XElement item in outputParamList)
                            {
                                (newItem as DesignerItem_Fct).ReturnParameters.Add(
                                    new Fct_Parameter(newItem,
                                                      item.Element("name").Value,
                                                      item.Element("type").Value,
                                                      null));
                            }

                            //default output
                            if (newItem is DesignerItem_Fct)
                                (newItem as DesignerItem_Fct).DefaultOutputName = newItem_xml.Element("defaultOutput").Value;

                            //additional outputs
                            IEnumerable<XElement> additionalOutputs = newItem_xml.Elements("additionalOutputs").Elements("out");
                            foreach (XElement item in additionalOutputs)
                            {
                                (newItem as DesignerItem_Fct).AdditionalOutputs.Add(
                                    new DesignerItem_Fct.Parent_name_ID(newItem as DesignerItem_Fct, item.Value, Guid.NewGuid()));
                            }
                        }

                        if (newItem is DesignerItem_StartEnd)
                        {
                            (newItem as DesignerItem_StartEnd).IsStart = Convert.ToBoolean(newItem_xml.Element("isStart").Value);
                        }

                    } else
                    {
                        MessageBox.Show("ERROR", "problem to load the functionDefinition.xml", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    newItem.Content = content;
                    Point position = e.GetPosition(this);

                    if (dragObject.DesiredSize.HasValue)
                    {
                        Size desiredSize = dragObject.DesiredSize.Value;
                        newItem.Width = desiredSize.Width;
                        newItem.Height = desiredSize.Height;

                        DesignerCanvas.SetLeft(newItem, Math.Max(0, position.X - newItem.Width / 2));
                        DesignerCanvas.SetTop(newItem, Math.Max(0, position.Y - newItem.Height / 2));
                    }
                    else
                    {
                        DesignerCanvas.SetLeft(newItem, Math.Max(0, position.X));
                        DesignerCanvas.SetTop(newItem, Math.Max(0, position.Y));

                    }

                    Canvas.SetZIndex(newItem, this.Children.Count);
                    this.addChild(newItem);

                    //sets the style of the connectorDecorator based on what specified in the function catalog
                    SetConnectorDecoratorTemplate(newItem, newItem_xml.Element("connectionsStyle").Value);

                    // LUK: This function selects automatically the item once sent to canvas
                    this.SelectionService.SelectItem(newItem);
                    newItem.Focus();

                    this.OnPropertyChanged("FirstSelectedElement");
                }

                e.Handled = true;
            }
        }

        // return the size of the canvas, by computing it from its children position/size
        // https://stackoverflow.com/questions/3401636/measuring-controls-created-at-runtime-in-wpf
        protected override Size MeasureOverride(Size constraint)
        {
            Size size = new Size();

            foreach (UIElement element in this.InternalChildren)
            {
                double left = Canvas.GetLeft(element);
                double top = Canvas.GetTop(element);
                left = double.IsNaN(left) ? 0 : left;
                top = double.IsNaN(top) ? 0 : top;

                element.Measure(constraint);

                Size desiredSize = element.DesiredSize;
                if (!double.IsNaN(desiredSize.Width) && !double.IsNaN(desiredSize.Height))
                {
                    size.Width = Math.Max(size.Width, left + desiredSize.Width);
                    size.Height = Math.Max(size.Height, top + desiredSize.Height);
                }
            }
            // add margin 
            size.Width += 10;
            size.Height += 10;
            return size;
        }

        // LUK: This sets connectors
        protected void SetConnectorDecoratorTemplate(DesignerItem item, string type)
        {
            if (item.ApplyTemplate() && item.Content is UIElement)
            {
                ControlTemplate template = null;

                if (item is DesignerItem_Fct)
                {
                    if (type == "default")
                        template = (ControlTemplate)item.FindResource("ConnectorDecoratorTemplate");
                    if (type == "IF_like")
                        template = (ControlTemplate)item.FindResource("ConnectorDecoratorTemplate_IF");
                }
                else
                {
                    if (item is DesignerItem_StartEnd)
                    {
                        if (type == "default")
                            template = (ControlTemplate)item.FindResource("ConnectorDecoratorTemplate_StartEnd");
                    }
                    else
                    {
                        if (item is DesignerItem_FctGroupConnector)
                            if (type == "default")
                                template = (ControlTemplate)item.FindResource("ConnectorDecoratorTemplate_GroupConnector");
                    }
                }

                if (template == null)
                    template = (ControlTemplate)item.FindResource("ConnectorDecoratorTemplate");

                Control decorator = item.Template.FindName("PART_ConnectorDecorator", item) as Control;
                if (decorator != null && template != null)
                    decorator.Template = template;
            }
        }
    }
}
