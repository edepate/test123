﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows.Threading;

namespace DiagramDesigner
{
    public partial class DesignerCanvas
    {
        public static RoutedCommand Group = new RoutedCommand();
        public static RoutedCommand Ungroup = new RoutedCommand();
        public static RoutedCommand BringForward = new RoutedCommand();
        public static RoutedCommand BringToFront = new RoutedCommand();
        public static RoutedCommand SendBackward = new RoutedCommand();
        public static RoutedCommand SendToBack = new RoutedCommand();
        public static RoutedCommand AlignTop = new RoutedCommand();
        public static RoutedCommand AlignVerticalCenters = new RoutedCommand();
        public static RoutedCommand AlignBottom = new RoutedCommand();
        public static RoutedCommand AlignLeft = new RoutedCommand();
        public static RoutedCommand AlignHorizontalCenters = new RoutedCommand();
        public static RoutedCommand AlignRight = new RoutedCommand();
        public static RoutedCommand DistributeHorizontal = new RoutedCommand();
        public static RoutedCommand DistributeVertical = new RoutedCommand();
        public static RoutedCommand SelectAll = new RoutedCommand();

        public static RoutedCommand Show = new RoutedCommand();

        // SourceSink used to store source and sink connectors for connections
        private struct SourceSink
        {
            public SourceSink(Connector _source, Connector _sink)
            {
                Source = _source;
                Sink = _sink;
            }

            public Connector Source;
            public Connector Sink;

        }

        /* SourceSink used to store source (*), sink (**) connectors for connections
         * and also the old (ID,Name) (***) of the connector that will be in the group:
         * 
         *    designerItem[returnP]-------[[->[inputP]designerItem...group]]
         *    
         *                        *------->      (1)
         *                                *--->  (2)
         *                        *      **  ***
         * (1) is the connection with DesignerItem_FctGroup. source(*) and sink(**) is used here
         * (2) is the connection between DesignerItem_FctGroup.subCanvas GroupConnector and the
         *     DesignerItem inside the group. old (***) is used here
         */
        private struct Source_Sink_Old_AssIDName
        {
            public Source_Sink_Old_AssIDName(Guid sourceID, string sourceName,
                                                 Guid sinkID, string sinkName,
                                                 Guid oldID, string oldName)
            {
                SourceID = sourceID;
                SinkID = sinkID;
                SourceName = sourceName;
                SinkName = sinkName;
                OldID = oldID;
                OldName = oldName;
            }
            public Guid SourceID;
            public Guid SinkID;
            public Guid OldID;
            public string SourceName;
            public string SinkName;
            public string OldName;
        }

        public DesignerCanvas()
        {
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.New, New_Executed));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Open, Open_Executed));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Save, Save_Executed));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Print, Print_Executed));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, Cut_Executed, Cut_Enabled));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, Copy_Executed, Copy_Enabled));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, Paste_Executed, Paste_Enabled));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, Delete_Executed, Delete_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.Group, Group_Executed, Group_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.Ungroup, Ungroup_Executed, Ungroup_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.BringForward, BringForward_Executed, Order_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.BringToFront, BringToFront_Executed, Order_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.SendBackward, SendBackward_Executed, Order_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.SendToBack, SendToBack_Executed, Order_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.AlignTop, AlignTop_Executed, Align_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.AlignVerticalCenters, AlignVerticalCenters_Executed, Align_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.AlignBottom, AlignBottom_Executed, Align_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.AlignLeft, AlignLeft_Executed, Align_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.AlignHorizontalCenters, AlignHorizontalCenters_Executed, Align_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.AlignRight, AlignRight_Executed, Align_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.DistributeHorizontal, DistributeHorizontal_Executed, Distribute_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.DistributeVertical, DistributeVertical_Executed, Distribute_Enabled));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.SelectAll, SelectAll_Executed));
            this.CommandBindings.Add(new CommandBinding(DesignerCanvas.Show, ShowSomething));
            SelectAll.InputGestures.Add(new KeyGesture(Key.A, ModifierKeys.Control));

            this.AllowDrop = true;
            Clipboard.Clear();
        }
        
        private void ShowSomething(object sender, ExecutedRoutedEventArgs e)
        {
            //MyDesigner.Mytext = "test";
           
            MessageBox.Show("nice");
        }

        #region New Command

        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Children.Clear();
            this.SelectionService.ClearSelection();
        }

        #endregion

        #region Open Command

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            XElement root = LoadSerializedDataFromFile();

            if (root == null)
                return;

            this.Children.Clear();
            this.SelectionService.ClearSelection();

            IEnumerable<XElement> itemsXML = root.Elements("functionList").Elements("function");
            foreach (XElement itemXML in itemsXML)
            {
                Guid id = new Guid(itemXML.Element("ID").Value);
                DesignerItem item = DeserializeDesignerItem(itemXML, id, 0, 0);  
                if (item == null) return;
                this.Children.Add(item);

                // apply the connectorDecoratorTemplate
                XElement newItem_xml = XElement.Parse((string)((FrameworkElement)item.Content).Tag);
                string connectionStyle = newItem_xml.Element("connectionsStyle").Value;
                if (connectionStyle == String.Empty)
                {
                    MessageBox.Show(string.Format("TAG connectionsStyle of function ID ({0}) not set in opened file", id.ToString()),
                               "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                this.SetConnectorDecoratorTemplate(item, connectionStyle);
            }

            this.InvalidateVisual();
            IEnumerable<XElement> connectionsXML = root.Elements("connections").Elements("connection");

            if (Application.Current == null)
                return;
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => LoadConnections(connectionsXML)));
        }

        #endregion

        #region Save Command

        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IEnumerable<DesignerItem> designerItems = this.Children.OfType<DesignerItem>();
            IEnumerable<Connection> connections = this.Children.OfType<Connection>();

            XElement designerItemsXML = SerializeDesignerItems(designerItems);  //RUG: here I can modify the serialization to avoid saving XAML data
            XElement connectionsXML = SerializeConnections(connections);

            XElement root = new XElement("functionCollector");
            root.Add(designerItemsXML);
            root.Add(connectionsXML);

            SaveFile(root);
        }

        #endregion

        #region Print Command

        private void Print_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SelectionService.ClearSelection();

            PrintDialog printDialog = new PrintDialog();

            if (true == printDialog.ShowDialog())
            {
                printDialog.PrintVisual(this, "WPF Diagram");
            }
        }

        #endregion

        #region Copy Command

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CopyCurrentSelection();
        }

        private void Copy_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectionService.CurrentSelection.Count() > 0;
        }

        #endregion

        #region Paste Command

        private void Paste_Executed(object sender, ExecutedRoutedEventArgs e)
        {   /*
            XElement root = LoadSerializedDataFromClipBoard();

            if (root == null)
                return;

            // create DesignerItems
            Dictionary<Guid, Guid> mappingOldToNewIDs = new Dictionary<Guid, Guid>();
            List<ISelectable> newItems = new List<ISelectable>();
            IEnumerable<XElement> itemsXML = root.Elements("DesignerItems").Elements("DesignerItem");

            double offsetX = Double.Parse(root.Attribute("OffsetX").Value, CultureInfo.InvariantCulture);
            double offsetY = Double.Parse(root.Attribute("OffsetY").Value, CultureInfo.InvariantCulture);

            foreach (XElement itemXML in itemsXML)
            {
                Guid oldID = new Guid(itemXML.Element("ID").Value);
                Guid newID = Guid.NewGuid();
                mappingOldToNewIDs.Add(oldID, newID);
                DesignerItem item = DeserializeDesignerItem(itemXML, newID, offsetX, offsetY);
                this.Children.Add(item);
                SetConnectorDecoratorTemplate(item);
                newItems.Add(item);
            }

            // update group hierarchy
            SelectionService.ClearSelection();
            foreach (DesignerItem el in newItems)
            {
                if (el.ParentID != Guid.Empty)
                    el.ParentID = mappingOldToNewIDs[el.ParentID];
            }


            foreach (DesignerItem item in newItems)
            {
                if (item.ParentID == Guid.Empty)
                {
                    SelectionService.AddToSelection(item);
                }
            }

            // create Connections
            IEnumerable<XElement> connectionsXML = root.Elements("Connections").Elements("Connection");
            foreach (XElement connectionXML in connectionsXML)
            {
                Guid oldSourceID = new Guid(connectionXML.Element("SourceID").Value);
                Guid oldSinkID = new Guid(connectionXML.Element("SinkID").Value);

                if (mappingOldToNewIDs.ContainsKey(oldSourceID) && mappingOldToNewIDs.ContainsKey(oldSinkID))
                {
                    Guid newSourceID = mappingOldToNewIDs[oldSourceID];
                    Guid newSinkID = mappingOldToNewIDs[oldSinkID];

                    String sourceConnectorName = connectionXML.Element("SourceConnectorName").Value;
                    String sinkConnectorName = connectionXML.Element("SinkConnectorName").Value;

                    Connector sourceConnector = GetConnector(newSourceID, sourceConnectorName);
                    Connector sinkConnector = GetConnector(newSinkID, sinkConnectorName);

                    Connection connection = new Connection(sourceConnector, sinkConnector);
                    Canvas.SetZIndex(connection, Int32.Parse(connectionXML.Element("zIndex").Value));
                    this.Children.Add(connection);

                    SelectionService.AddToSelection(connection);
                }
            }

            DesignerCanvas.BringToFront.Execute(null, this);

            // update paste offset
            root.Attribute("OffsetX").Value = (offsetX + 10).ToString();
            root.Attribute("OffsetY").Value = (offsetY + 10).ToString();
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Xaml, root);
            */
        }

        private void Paste_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
e.CanExecute = Clipboard.ContainsData(DataFormats.Xaml);
        }

        #endregion

        #region Delete Command

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DeleteCurrentSelection();
        }

        private void Delete_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.SelectionService.CurrentSelection.Count() > 0;
        }

        #endregion

        #region Cut Command

        private void Cut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CopyCurrentSelection();
            DeleteCurrentSelection();
        }

        private void Cut_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = this.SelectionService.CurrentSelection.Count() > 0;
        }

        #endregion

        #region Group Command

        private void Group_Executed(object sender, ExecutedRoutedEventArgs e)
        { 
            IEnumerable<DesignerItem> selectedItems = from item in this.SelectionService.CurrentSelection.OfType<DesignerItem>()
                                                      select item;

            DesignerItem_FctGroup groupItem = new DesignerItem_FctGroup();

            //assign to the DesignerItem representing the group the visual properties of "Group"
            string xamlString = GetXamlTag(this.ToolPalette, "Group");
            if (xamlString == "") return;
            groupItem.BlockName = "Group";
            groupItem.Content = XamlReader.Load(XmlReader.Create(new StringReader(xamlString)));

            //and apply the properties that are defined in FunctionCatalog.xml
            XElement item_xml = XElement.Parse((string)((FrameworkElement)groupItem.Content).Tag);
            groupItem.Width = 100;
            groupItem.Height = 100;

            SetConnectorDecoratorTemplate(groupItem, "default");

            //creates a canvas that will contain the grouped elements. ToolPalette must also be given
            //so the subCanvas can access to Palette to create elements (constants for external connectors)
            Rect rect = GetBoundingRectangle(selectedItems);
            DesignerCanvas_Group groupCanvas = new DesignerCanvas_Group(ToolPalette);
            groupCanvas.Height = rect.Height + 20;
            groupCanvas.Width = rect.Width + 20;

            /* looks for the connectors that start and end to DesignerItems in the group (exclude
             * the connectors that comes/leads form/to outside the group), and store theirs Source/Sink
             * (it will be used in the future to re-create the connections in the new DesignerCanvas) */

            bool foundIn = false;
            bool foundOut = false;
            List<SourceSink> connectionsInternal = new List<SourceSink>();
            List<SourceSink> connectionsWithOutside = new List<SourceSink>();
            //copy the selection list in a new list (the selection will be incrementally deselected
            //in the next cycle, therefore we need to store it in a premanent list)
            List<DesignerItem> selectedDesignerItems = new List<DesignerItem>();
            foreach (DesignerItem item in this.SelectionService.CurrentSelection.OfType<DesignerItem>())
                selectedDesignerItems.Add(item);
            foreach (DesignerItem item in selectedDesignerItems)
            {
                foreach (Connector conn in ClassTreeHelper.FindVisualChildren<Connector>(item))
                {
                    // if connector has an associated connection...
                    if (conn.Connections.Count > 0)
                        // if connection starts and ends in the selected group
                        if (this.SelectionService.CurrentSelection.Contains(conn.Connections[0].Source.ParentDesignerItem) &&
                            this.SelectionService.CurrentSelection.Contains(conn.Connections[0].Sink.ParentDesignerItem))
                        {
                            //store source/sink
                            SourceSink sourceSink = new SourceSink(
                                conn.Connections[0].Source,
                                conn.Connections[0].Sink);
                            if (!connectionsInternal.Contains(sourceSink))
                                connectionsInternal.Add(sourceSink);
                            //and add the connection into the currentSelection (for late handling) if not already present
                            if (!this.SelectionService.CurrentSelection.Contains(conn.Connections[0]))
                                this.SelectionService.CurrentSelection.Add(conn.Connections[0]);
                        }
                        else
                        // if the connection connects the group with outside
                        {
                            if (conn.Connections[0].Sink.IsInput &&
                                this.SelectionService.CurrentSelection.Contains(conn.Connections[0].Sink.ParentDesignerItem))
                            {
                                if (foundIn == true)
                                {
                                    MessageBox.Show("Error: the selected group has more than one input connection with outside",
                                       "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return;
                                }
                                foundIn = true;
                            }
                            if (conn.Connections[0].Source.IsDefaultOutput &&
                                this.SelectionService.CurrentSelection.Contains(conn.Connections[0].Source.ParentDesignerItem))
                            {
                                if (foundOut == true)
                                {
                                    MessageBox.Show("Error: the selected group has more than one default output connection with outside",
                                       "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return;
                                }
                                foundOut = true;
                            }

                            connectionsWithOutside.Add(new SourceSink(
                                conn.Connections[0].Source,
                                conn.Connections[0].Sink));
                        }
                }
            }
            
            /* detach elements from the main DesignerCanvas and attach them to the new DesignerCanvas */
            //  copy the selection list in a new list (the selection will be incrementally deselected
            //  in the next cycle, therefore we need to store it in a premanent list)
            List<Control> groupedItems = new List<Control>();
            foreach (Control item in this.SelectionService.CurrentSelection.OfType<Control>())
                groupedItems.Add(item);
            foreach (Control item in groupedItems)
            {
                //it is important to remove the connections from the selection, otherwise the ConnectorAdorner
                //remains locked in the old DesignerCanvas (see Connection.Connection_Unloaded)
                this.SelectionService.RemoveFromSelection(item as ISelectable);
                this.Children.Remove(item);
                //only add DesignerItems because the connection will be re-created later
                if (item is DesignerItem)
                {
                    groupCanvas.Children.Add(item);
                    item.InvalidateMeasure();
                    //and move them to the point (0+40;0+40)
                    Canvas.SetLeft(item, Canvas.GetLeft(item) - rect.Left + 80);
                    Canvas.SetTop(item, Canvas.GetTop(item) - rect.Top + 80);
                }
            }

            //add groupItem to the DeisgnerCanvas (show) and select it
            groupCanvas.Width += 160;  //add some margin
            groupCanvas.Height += 160; //add some margin
            Canvas.SetLeft(groupItem, (rect.Left + rect.Width / 2 - groupItem.Width/2));
            Canvas.SetTop(groupItem, rect.Top + rect.Height / 2 - groupItem.Height/2);
            Canvas.SetZIndex(groupItem, this.Children.Count);
            this.Children.Add(groupItem);
            //this.SelectionService.SelectItem(groupItem);

            groupItem.SubCanvas = groupCanvas;

            /* define the list of connections that connect the DesignerItem_FctGroup with elements outside. Remember
             * that the DesignerItem_FctGroup should as first be filled up with all inputParameters and returnParameters,
             * and only then we must call the dispatcher. Only after the dispatcher has executed all the
             * render threads we will have all the connectors available to create new connections.
             
             IMORTANT: a connection can be started if it is being dragged starting from:
                - .IsOutput connector
                - .IsAdditionalOutput connector
                - .IsReturnParameter connector
                therefore the connectoin.source can be only one of the previous three types
             */

            List<Source_Sink_Old_AssIDName> connectionsWithOutside2 = new List<Source_Sink_Old_AssIDName>();
            foreach (SourceSink item in connectionsWithOutside)
            {
                //handles input statement
                //     designerItem[out]-------[[->[input]designerItem...group]]
                if (item.Sink.IsInput)
                    if (groupedItems.Contains(item.Sink.ParentDesignerItem))
                    {
                        //store rference to the associated connection
                        Connection connection = item.Sink.Connections[0];

                        //push the connection to redraw. Note that for the Sink we give the groupItem.ID, because the
                        //IN connector should be found there
                        connectionsWithOutside2.Add(new Source_Sink_Old_AssIDName(
                                connection.Source.AssociatedID, connection.Source.Name,
                                groupItem.ID, connection.Sink.Name,
                                connection.Sink.AssociatedID, connection.Sink.Name));
                        this.Children.Remove(connection);
                    }

                //handles defaultOutput statement
                //     [[group...designerItem[Out]-]]------->[input]designerItem
                if (item.Source.IsDefaultOutput)
                    if (groupedItems.Contains(item.Source.ParentDesignerItem))
                    {
                        //store rference to the associated connection
                        Connection connection = item.Source.Connections[0];

                        //push the connection to redraw. Note that for the Sink we give the groupItem.ID, because the
                        //IN connector should be found there
                        connectionsWithOutside2.Add(new Source_Sink_Old_AssIDName(
                                groupItem.ID, connection.Source.Name,
                                connection.Sink.AssociatedID, connection.Sink.Name,
                                connection.Source.AssociatedID,connection.Source.Name));
                        this.Children.Remove(connection);
                    }

                //handles additionalOutput statement, only for DesignerItem_Fct and inherited
                //     [[group...designerItem[AdditionalOut]-]]------->[input]designerItem
                if (item.Source.IsAdditionalOutput)
                    if (groupedItems.Contains(item.Source.ParentDesignerItem))
                    {
                        //store rference to the associated connection
                        Connection connection = item.Source.Connections[0];

                        //get the reference to the AdditionalOutputs element associated to the item.Source
                        DesignerItem_Fct.Parent_name_ID output = (from elem in (item.Source.ParentDesignerItem as DesignerItem_Fct).AdditionalOutputs
                                                              where elem.ID == item.Source.AssociatedID
                                                              select elem).FirstOrDefault<DesignerItem_Fct.Parent_name_ID>();

                        //creates a new AdditionalOutput connector for the groupItem
                        DesignerItem_Fct.Parent_name_ID newConnector = new DesignerItem_Fct.Parent_name_ID(groupItem, output.Name, Guid.NewGuid());
                        groupItem.AdditionalOutputs.Add(newConnector);

                        connectionsWithOutside2.Add(new Source_Sink_Old_AssIDName(
                                newConnector.ID, connection.Source.Name,
                                connection.Sink.AssociatedID, connection.Sink.Name,
                                connection.Source.AssociatedID,connection.Source.Name));
                        this.Children.Remove(connection);
                    }

                //handles return parameter
                //     [[group...designerItem[returnP]-]]------->[inputP]designerItem
                if (item.Source.IsReturnParameter)
                    if (groupedItems.Contains(item.Source.ParentDesignerItem))
                    {
                        //store rference to the associated connection
                        Connection connection = item.Source.Connections[0];

                        //looks for the Fct_Parameter object associated to the connnector
                        Fct_Parameter param = (from elem in (item.Source.ParentDesignerItem as DesignerItem_Fct).ReturnParameters
                                               where elem.ID == item.Source.AssociatedID
                                               select elem).FirstOrDefault<Fct_Parameter>();

                        //create a new parameter for the DesignerItem_FctGroup.
                        Fct_Parameter newParam = new Fct_Parameter(groupItem, param.Name, param.Type, null, Guid.NewGuid());
                        //newParam.ExternallyDefined = true;

                        //if the connector is the source of the connection
                        if (connection.Source.AssociatedID == param.ID)
                        {
                            groupItem.ReturnParameters.Add(newParam);
                            connectionsWithOutside2.Add(new Source_Sink_Old_AssIDName(
                                newParam.ID, connection.Source.Name,
                                connection.Sink.AssociatedID, connection.Sink.Name,
                                connection.Source.AssociatedID,connection.Source.Name));
                            this.Children.Remove(connection);
                        }
                    }
                //handles input parameter
                //     designerItem[returnP]-------[[->[inputP]designerItem...group]]
                if (item.Sink.IsInputParameter)
                    if (groupedItems.Contains(item.Sink.ParentDesignerItem))
                    {
                        //store rference to the associated connection
                        Connection connection = item.Sink.Connections[0];

                        //looks for the Fct_Parameter object associated to the connnector
                        Fct_Parameter param = (from elem in (item.Sink.ParentDesignerItem as DesignerItem_Fct).InputParameters
                                               where elem.ID == item.Sink.AssociatedID
                                               select elem).FirstOrDefault<Fct_Parameter>();

                        //create a new parameter for the DesignerItem_FctGroup.
                        Fct_inParameter newParam = new Fct_inParameter(groupItem, param.Name, param.Type, null, Guid.NewGuid(),true);

                        //if the connector is the sink of the connection
                        if (connection.Sink.AssociatedID == param.ID)
                        {
                            groupItem.InputParameters.Add(newParam);
                            connectionsWithOutside2.Add(new Source_Sink_Old_AssIDName(
                                connection.Source.AssociatedID, connection.Source.Name,
                                newParam.ID, connection.Sink.Name,
                                connection.Sink.AssociatedID, connection.Sink.Name));
                            this.Children.Remove(connection);
                        }
                    }
            }

            if (Application.Current == null)
                return;
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                new Action(() => Group_Executed_2(groupedItems, groupItem, connectionsWithOutside2, connectionsInternal)));

        }

        //Group_Executed_2 will be executed after the Dispatcher completes all the render threads (connectors will be
        //in the VisualTree therefore accessible with the "GetConnector" method)
        private void Group_Executed_2(List<Control> groupedItems, DesignerItem_FctGroup groupItem, List<Source_Sink_Old_AssIDName> connectionsWithOutside2, List<SourceSink> connectionsInternal)
        { 
            foreach (Source_Sink_Old_AssIDName item in connectionsWithOutside2)
            {
                Connector source = GetConnector(item.SourceID, item.SourceName);
                Connector sink = GetConnector(item.SinkID, item.SinkName);
                Connection connectionUpdated = new Connection(source, sink);
                Canvas.SetZIndex(connectionUpdated, 1);
                this.Children.Add(connectionUpdated);

                //handles input parameter
                //     designerItem[returnP]-------[[->[inputP]designerItem...group]] 
                if (sink.IsInputParameter && (sink.ParentDesignerItem == groupItem))
                {
                    //looks for the inputParameter of the DesignerItem_FctGroup
                    Fct_Parameter parameter = (from param in (sink.ParentDesignerItem as DesignerItem_Fct).InputParameters
                                              where sink.AssociatedID == param.ID
                                              select param).FirstOrDefault<Fct_Parameter>();

                    groupItem.SubCanvas.ext2intConn_ToRedrawQueue.Enqueue(
                        new DesignerCanvas_Group.FctGroup_BoundaryRef2InternRefID(parameter, item.OldID));
                }

                //handles return parameter
                //     [[group...designerItem[returnP]-]]------->[inputP]designerItem
                if (source.IsReturnParameter && (source.ParentDesignerItem == groupItem))
                {
                    Fct_Parameter parameter = (from param in (source.ParentDesignerItem as DesignerItem_Fct).ReturnParameters
                                               where source.AssociatedID == param.ID
                                               select param).FirstOrDefault<Fct_Parameter>();

                    groupItem.SubCanvas.ext2intConn_ToRedrawQueue.Enqueue(
                        new DesignerCanvas_Group.FctGroup_BoundaryRef2InternRefID(parameter, item.OldID));
                }

                //handles input statement
                //     designerItem[out]-------[[->[input]designerItem...group]]
                if (sink.IsInput && (sink.ParentDesignerItem == groupItem))
                {
                    // item.OldName is always "In" for .IsInput; groupItem and OldID actually are not used in ToRedrawQueue
                    // DesignerItem_Fct.Parent_name_ID ref_ = new DesignerItem_Fct.Parent_name_ID(groupItem, item.OldName, item.OldID);
                    groupItem.SubCanvas.ext2intConn_ToRedrawQueue.Enqueue(
                        new DesignerCanvas_Group.FctGroup_BoundaryRef2InternRefID((string)"In", item.OldID));
                }

                //handles output statement
                //     [[group...designerItem[Out]-]]------->[input]designerItem
                if (source.IsDefaultOutput && (source.ParentDesignerItem == groupItem))
                {
                    // item.OldName is always "Out" for .IsDefaultOutput
                    // DesignerItem_Fct.Parent_name_ID ref_ = new DesignerItem_Fct.Parent_name_ID(groupItem, item.OldName, item.OldID);
                    groupItem.SubCanvas.ext2intConn_ToRedrawQueue.Enqueue(
                        new DesignerCanvas_Group.FctGroup_BoundaryRef2InternRefID((string)"Out", item.OldID));
                }

                //handles additionalOut statement
                //     [[group...designerItem[additionalOut] -]]------->[input]designerItem
                if (source.IsAdditionalOutput && (source.ParentDesignerItem == groupItem))
                {
                    // if the null parameter is passed, the Ext2intConn_ToRedrawQueue_CollectionChanged knows that
                    // is not about a parameter, but about Start/End

                    DesignerItem_Fct.Parent_name_ID ref_ = (from conn_ in (source.ParentDesignerItem as DesignerItem_Fct).AdditionalOutputs
                                               where source.AssociatedID == conn_.ID
                                               select conn_).FirstOrDefault<DesignerItem_Fct.Parent_name_ID>();
                    //ref_.ID = item.SinkID;

                    //DesignerItem_Fct.Parent_name_ID ref_ = new DesignerItem_Fct.Parent_name_ID(groupItem, item.OldName, item.OldID);
                    groupItem.SubCanvas.ext2intConn_ToRedrawQueue.Enqueue(
                        new DesignerCanvas_Group.FctGroup_BoundaryRef2InternRefID(ref_, item.OldID));

                    //groupItem.SubCanvas.ext2intConn_ToRedrawQueue.Enqueue(
                    //    new DesignerCanvas_Group.FctGroupParamRef_internFctGroupRefID(item.OldID, item.OldName));
                }

            }

            /* re-create connections from stored source/sink.
             * INFO: this could have been done also before waiting for the dispatcher, as the DesignerItem elements were already
             * available and we should not have waited for the connectors to be created by the Render thread, as for DesignerItem_FctGroup */
             
            foreach (SourceSink item in connectionsInternal)
            {
                Connector source = GetConnector(item.Source.ParentDesignerItem, item.Source.AssociatedID, item.Source.Name);
                Connector sink = GetConnector(item.Sink.ParentDesignerItem, item.Sink.AssociatedID, item.Sink.Name);

                Connection connection = new Connection(source, sink);
                Canvas.SetZIndex(connection, 1);
                groupItem.SubCanvas.Children.Add(connection);
            }

            // show the window otherwise the elements inside (connections) are not generated. If this is not
            // executed, the connections inside will be not instantiated, with negative consequences on UnGroup and 
            // Serialization
            groupItem.SubWindow.Show();
            
        } 

        private void Group_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            int count = (from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                         where item.ParentID == Guid.Empty
                         select item).Count();

            e.CanExecute = count > 1;
        }

        #endregion

        #region Ungroup Command

        private void Ungroup_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var groupItem = (from item in SelectionService.CurrentSelection.OfType<DesignerItem_FctGroup>()
                          select item).FirstOrDefault();

            // creates a copy of all items in the subCanvas
            List<Control> items = new List<Control>();
            foreach (Control item in groupItem.SubCanvas.Children.OfType<Control>())
               items.Add(item);
                
            // computes how much space is needed
            Rect rect = GetBoundingRectangle(from item in groupItem.SubCanvas.Children.OfType<DesignerItem>()
                                             where !(item is DesignerItem_StartEnd) && !(item is DesignerItem_StartEnd) select item);

            // stores the connections with outside
            List<SourceSink> connectionsWithOutside = new List<SourceSink>();
            Connector groupItemConnection, source, ext_source, ext_sink, sink;
            foreach (Connection item in items.OfType<Connection>())
            {
                if (item.Source.ParentDesignerItem is DesignerItem_FctGroupConnector)
                {   // case of inputParameter of groupItem
                    //     designerItem[returnP]------->[[  Connector--->[inputP]designerItem...group]]

                    groupItemConnection = GetConnector(groupItem, (item.Source.ParentDesignerItem as DesignerItem_FctGroupConnector).AssociatedParamID, item.Source.Name);
                    
                    // skip if connection not present (user deleted after groupCreation the external connection)
                    if (groupItemConnection.Connections.Count == 0) continue;
                    ext_source = groupItemConnection.Connections[0].Source;
                    sink = item.Sink;

                    connectionsWithOutside.Add(new SourceSink(ext_source, sink));
                }

                if (item.Sink.ParentDesignerItem is DesignerItem_FctGroupConnector)
                {   // case of returnParameter of groupItem
                    //     [[group...designerItem[returnP]--->Connector  ]]------->[inputP]designerItem

                    groupItemConnection = GetConnector(groupItem, (item.Sink.ParentDesignerItem as DesignerItem_FctGroupConnector).AssociatedParamID, item.Sink.Name);

                    if (groupItemConnection.Connections.Count == 0) continue;
                    ext_sink = groupItemConnection.Connections[0].Sink;
                    source = item.Source;

                    connectionsWithOutside.Add(new SourceSink(source, ext_sink));
                }

                if (item.Source.ParentDesignerItem is DesignerItem_StartEnd)
                {   // case of input of groupItem
                    //     designerItem[Out/AdditionalOut]------->[[  Connector--->[In]designerItem...group]]

                    groupItemConnection = GetConnector(groupItem.ID, "In");

                    // skip if connection not present (user deleted after groupCreation the external connection)
                    if (groupItemConnection.Connections.Count == 0) continue;
                    ext_source = groupItemConnection.Connections[0].Source;
                    sink = item.Sink;

                    connectionsWithOutside.Add(new SourceSink(ext_source, sink));
                }

                if (item.Sink.ParentDesignerItem is DesignerItem_StartEnd)
                {   // case of Out of groupItem
                    //     [[group...designerItem[Out/AdditionalOut]--->Connector  ]]------->[In]designerItem

                    if ((item.Sink.ParentDesignerItem as DesignerItem_StartEnd).IsEnd)
                    {   // handle Out 
                        groupItemConnection = GetConnector(groupItem.ID, "Out");

                        if (groupItemConnection.Connections.Count == 0) continue;
                        ext_sink = groupItemConnection.Connections[0].Sink;
                        source = item.Source;

                        connectionsWithOutside.Add(new SourceSink(source, ext_sink));
                    }

                    if ((item.Sink.ParentDesignerItem as DesignerItem_StartEnd).IsAdditionalEnd)
                    {   // handle additionalOut

                        groupItemConnection = GetConnector(groupItem, (item.Sink.ParentDesignerItem as DesignerItem_StartEnd).AssociatedInOutNameID.ID, "additionalOutConn_i");
                        
                        if (groupItemConnection.Connections.Count == 0) continue;
                        ext_sink = groupItemConnection.Connections[0].Sink;
                        source = item.Source;

                        connectionsWithOutside.Add(new SourceSink(source, ext_sink));
                    }

                }

                if (item.Source.ParentDesignerItem is DesignerItem_Fct && item.Sink.ParentDesignerItem is DesignerItem_Fct)
                   connectionsWithOutside.Add(new SourceSink(item.Source, item.Sink));                
            }

            foreach (Control item in items)
            {
                groupItem.SubCanvas.Children.Remove(item);
                if (item is DesignerItem_Fct)
                {
                    this.Children.Add(item);
                    SetLeft(item, GetLeft(item) - rect.Left + GetLeft(groupItem) - 120);
                    SetTop(item, GetTop(item) - rect.Top + GetTop(groupItem) - 95);
                }
            }

            this.selectionService.ClearSelection();
            this.selectionService.SelectItem(groupItem);
            DeleteCurrentSelection();

            foreach (SourceSink iter in connectionsWithOutside)
            {
                Connection conn = new Connection(iter.Source, iter.Sink);
                this.Children.Add(conn);
            }
        }
        
        private void Ungroup_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            var groupedItem = from item in SelectionService.CurrentSelection.OfType<DesignerItem_FctGroup>()
                              select item;


            e.CanExecute = groupedItem.Count() == 1;
        }

        #endregion

        #region BringForward Command

        private void BringForward_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            List<UIElement> ordered = (from item in SelectionService.CurrentSelection
                                       orderby Canvas.GetZIndex(item as UIElement) descending
                                       select item as UIElement).ToList();

            int count = this.Children.Count;

            for (int i = 0; i < ordered.Count; i++)
            {
                int currentIndex = Canvas.GetZIndex(ordered[i]);
                int newIndex = Math.Min(count - 1 - i, currentIndex + 1);
                if (currentIndex != newIndex)
                {
                    Canvas.SetZIndex(ordered[i], newIndex);
                    IEnumerable<UIElement> it = this.Children.OfType<UIElement>().Where(item => Canvas.GetZIndex(item) == newIndex);

                    foreach (UIElement elm in it)
                    {
                        if (elm != ordered[i])
                        {
                            Canvas.SetZIndex(elm, currentIndex);
                            break;
                        }
                    }
                }
            }
        }

        private void Order_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            //e.CanExecute = SelectionService.CurrentSelection.Count() > 0;
            e.CanExecute = true;
        }

        #endregion

        #region BringToFront Command

        private void BringToFront_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            List<UIElement> selectionSorted = (from item in SelectionService.CurrentSelection
                                               orderby Canvas.GetZIndex(item as UIElement) ascending
                                               select item as UIElement).ToList();

            List<UIElement> childrenSorted = (from UIElement item in this.Children
                                              orderby Canvas.GetZIndex(item as UIElement) ascending
                                              select item as UIElement).ToList();

            int i = 0;
            int j = 0;
            foreach (UIElement item in childrenSorted)
            {
                if (selectionSorted.Contains(item))
                {
                    int idx = Canvas.GetZIndex(item);
                    Canvas.SetZIndex(item, childrenSorted.Count - selectionSorted.Count + j++);
                }
                else
                {
                    Canvas.SetZIndex(item, i++);
                }
            }
        }

        #endregion

        #region SendBackward Command

        private void SendBackward_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            List<UIElement> ordered = (from item in SelectionService.CurrentSelection
                                       orderby Canvas.GetZIndex(item as UIElement) ascending
                                       select item as UIElement).ToList();

            int count = this.Children.Count;

            for (int i = 0; i < ordered.Count; i++)
            {
                int currentIndex = Canvas.GetZIndex(ordered[i]);
                int newIndex = Math.Max(i, currentIndex - 1);
                if (currentIndex != newIndex)
                {
                    Canvas.SetZIndex(ordered[i], newIndex);
                    IEnumerable<UIElement> it = this.Children.OfType<UIElement>().Where(item => Canvas.GetZIndex(item) == newIndex);

                    foreach (UIElement elm in it)
                    {
                        if (elm != ordered[i])
                        {
                            Canvas.SetZIndex(elm, currentIndex);
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        #region SendToBack Command

        private void SendToBack_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            List<UIElement> selectionSorted = (from item in SelectionService.CurrentSelection
                                               orderby Canvas.GetZIndex(item as UIElement) ascending
                                               select item as UIElement).ToList();

            List<UIElement> childrenSorted = (from UIElement item in this.Children
                                              orderby Canvas.GetZIndex(item as UIElement) ascending
                                              select item as UIElement).ToList();
            int i = 0;
            int j = 0;
            foreach (UIElement item in childrenSorted)
            {
                if (selectionSorted.Contains(item))
                {
                    int idx = Canvas.GetZIndex(item);
                    Canvas.SetZIndex(item, j++);

                }
                else
                {
                    Canvas.SetZIndex(item, selectionSorted.Count + i++);
                }
            }
        }        

        #endregion

        #region AlignTop Command

        private void AlignTop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                select item;

            if (selectedItems.Count() > 1)
            {
                double top = Canvas.GetTop(selectedItems.First());

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = top - Canvas.GetTop(item);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetTop(di, Canvas.GetTop(di) + delta);
                    }
                }
            }
        }

        private void Align_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            //var groupedItem = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
            //                  where item.ParentID == Guid.Empty
            //                  select item;


            //e.CanExecute = groupedItem.Count() > 1;
            e.CanExecute = true;
        }

        #endregion

        #region AlignVerticalCenters Command

        private void AlignVerticalCenters_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                select item;

            if (selectedItems.Count() > 1)
            {
                double bottom = Canvas.GetTop(selectedItems.First()) + selectedItems.First().Height / 2;

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = bottom - (Canvas.GetTop(item) + item.Height / 2);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetTop(di, Canvas.GetTop(di) + delta);
                    }
                }
            }
        }

        #endregion

        #region AlignBottom Command

        private void AlignBottom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                select item;

            if (selectedItems.Count() > 1)
            {
                double bottom = Canvas.GetTop(selectedItems.First()) + selectedItems.First().Height;

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = bottom - (Canvas.GetTop(item) + item.Height);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetTop(di, Canvas.GetTop(di) + delta);
                    }
                }
            }
        }

        #endregion

        #region AlignLeft Command

        private void AlignLeft_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                select item;

            if (selectedItems.Count() > 1)
            {
                double left = Canvas.GetLeft(selectedItems.First());

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = left - Canvas.GetLeft(item);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetLeft(di, Canvas.GetLeft(di) + delta);
                    }
                }
            }
        }

        #endregion

        #region AlignHorizontalCenters Command

        private void AlignHorizontalCenters_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                select item;

            if (selectedItems.Count() > 1)
            {
                double center = Canvas.GetLeft(selectedItems.First()) + selectedItems.First().Width / 2;

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = center - (Canvas.GetLeft(item) + item.Width / 2);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetLeft(di, Canvas.GetLeft(di) + delta);
                    }
                }
            }
        }

        #endregion

        #region AlignRight Command

        private void AlignRight_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                select item;

            if (selectedItems.Count() > 1)
            {
                double right = Canvas.GetLeft(selectedItems.First()) + selectedItems.First().Width;

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = right - (Canvas.GetLeft(item) + item.Width);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetLeft(di, Canvas.GetLeft(di) + delta);
                    }
                }
            }
        }

        #endregion

        #region DistributeHorizontal Command

        private void DistributeHorizontal_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                let itemLeft = Canvas.GetLeft(item)
                                orderby itemLeft
                                select item;

            if (selectedItems.Count() > 1)
            {
                double left = Double.MaxValue;
                double right = Double.MinValue;
                double sumWidth = 0;
                foreach (DesignerItem item in selectedItems)
                {
                    left = Math.Min(left, Canvas.GetLeft(item));
                    right = Math.Max(right, Canvas.GetLeft(item) + item.Width);
                    sumWidth += item.Width;
                }

                double distance = Math.Max(0, (right - left - sumWidth) / (selectedItems.Count() - 1));
                double offset = Canvas.GetLeft(selectedItems.First());

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = offset - Canvas.GetLeft(item);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetLeft(di, Canvas.GetLeft(di) + delta);
                    }
                    offset = offset + item.Width + distance;
                }
            }
        }

        private void Distribute_Enabled(object sender, CanExecuteRoutedEventArgs e)
        {
            //var groupedItem = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
            //                  where item.ParentID == Guid.Empty
            //                  select item;


            //e.CanExecute = groupedItem.Count() > 1;
            e.CanExecute = true;
        }

        #endregion

        #region DistributeVertical Command

        private void DistributeVertical_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedItems = from item in SelectionService.CurrentSelection.OfType<DesignerItem>()
                                where item.ParentID == Guid.Empty
                                let itemTop = Canvas.GetTop(item)
                                orderby itemTop
                                select item;

            if (selectedItems.Count() > 1)
            {
                double top = Double.MaxValue;
                double bottom = Double.MinValue;
                double sumHeight = 0;
                foreach (DesignerItem item in selectedItems)
                {
                    top = Math.Min(top, Canvas.GetTop(item));
                    bottom = Math.Max(bottom, Canvas.GetTop(item) + item.Height);
                    sumHeight += item.Height;
                }

                double distance = Math.Max(0, (bottom - top - sumHeight) / (selectedItems.Count() - 1));
                double offset = Canvas.GetTop(selectedItems.First());

                foreach (DesignerItem item in selectedItems)
                {
                    double delta = offset - Canvas.GetTop(item);
                    foreach (DesignerItem di in SelectionService.GetGroupMembers(item))
                    {
                        Canvas.SetTop(di, Canvas.GetTop(di) + delta);
                    }
                    offset = offset + item.Height + distance;
                }
            }
        }

        #endregion

        #region SelectAll Command

        private void SelectAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SelectionService.SelectAll();
        }

        #endregion

        #region Helper Methods

        private XElement LoadSerializedDataFromFile()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Designer Files (*.xml)|*.xml|All Files (*.*)|*.*";

            if (openFile.ShowDialog() == true)
            {
                try
                {
                    return XElement.Load(openFile.FileName);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace, e.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return null;
        }

        void SaveFile(XElement xElement)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Files (*.xml)|*.xml|All Files (*.*)|*.*";
            if (saveFile.ShowDialog() == true)
            {
                try
                {
                    xElement.Save(saveFile.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private XElement LoadSerializedDataFromClipBoard()
        {
            if (Clipboard.ContainsData(DataFormats.Xaml))
            {
                String clipboardData = Clipboard.GetData(DataFormats.Xaml) as String;

                if (String.IsNullOrEmpty(clipboardData))
                    return null;
                try
                {
                    return XElement.Load(new StringReader(clipboardData));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace, e.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return null;
        }

        //RUG: it was private, but now has been set to public static, as it is also used from DebuggerCanvas
        public static XElement SerializeDesignerItems(IEnumerable<DesignerItem> designerItems)
        {
            // IEnumerable<ObservableCollection<Fct_Parameter>> paramas = designerItems

            XElement serializedItems = new XElement("functionList");
            foreach (DesignerItem item in designerItems)
            {
                // add basic shared properties
                XElement functionToAdd = new XElement("function");
                functionToAdd.Add(new XElement("name", item.BlockName));
                functionToAdd.Add(new XElement("Left", Canvas.GetLeft(item)));
                functionToAdd.Add(new XElement("Top", Canvas.GetTop(item)));
                functionToAdd.Add(new XElement("Width", item.Width));
                functionToAdd.Add(new XElement("Height", item.Height));
                functionToAdd.Add(new XElement("ID", item.ID));
                functionToAdd.Add(new XElement("zIndex", Canvas.GetZIndex(item)));
                //functionToAdd.Add(new XElement("IsGroup", item.IsGroup));
                //functionToAdd.Add(new XElement("ParentID", item.ParentID));

                // additional parameters for DesignerItem_Fct and DesignerItem_FctGroup (inherits from DesignerItem_Fct)
                if (item is DesignerItem_Fct)
                {
                    functionToAdd.Add(new XElement("parameterList",
                        from param in (item as DesignerItem_Fct).InputParameters
                        select new XElement("parameter",
                                    new XElement("ID", param.ID),
                                    new XElement("name",
                                        new XElement("value", param.Name)),
                                    new XElement("type",
                                        new XElement("value", param.Type)),
                                    new XElement("value", param.Value),
                                    new XElement("externallyDefined", (param as Fct_inParameter).ExternallyDefined))));

                    functionToAdd.Add(new XElement("returnParameterList",
                        from param in (item as DesignerItem_Fct).ReturnParameters
                        select new XElement("parameter",
                                    new XElement("ID", param.ID),
                                    new XElement("name", param.Name),
                                    new XElement("type", param.Type))));

                    functionToAdd.Add(new XElement("additionalOutputs",
                        from param in (item as DesignerItem_Fct).AdditionalOutputs
                        select new XElement("out",
                                    new XElement("ID", param.ID),
                                    new XElement("name", param.Name))));

                    if (item is DesignerItem_FctGroup)
                    {
                        //handle the serialization of the subCanvas
                        functionToAdd.Add(new XElement("isGroup", "true"));
                        functionToAdd.Add(
                            SerializeDesignerItems((item as DesignerItem_FctGroup).SubCanvas.Children.OfType<DesignerItem>()));

                        IEnumerable<Connection> connections = (item as DesignerItem_FctGroup).SubCanvas.Children.OfType<Connection>();
                        XElement connectionsXML = SerializeConnections(connections);
                        
                        functionToAdd.Add(connectionsXML);
                    }
                }

                if (item is DesignerItem_FctGroupConnector)
                {
                    functionToAdd.Add(new XElement("associatedParameterID", (item as DesignerItem_FctGroupConnector).AssociatedParamID));

                    functionToAdd.Add(new XElement("parameterList",
                        from param in (item as DesignerItem_FctGroupConnector).InputParameters
                        select new XElement("parameter",
                                    new XElement("ID", param.ID),
                                    new XElement("name",
                                        new XElement("value", param.Name)),
                                    new XElement("type",
                                        new XElement("value", param.Type)),
                                    new XElement("value", param.Value),
                                    new XElement("externallyDefined", (param as Fct_inParameter).ExternallyDefined))));

                    functionToAdd.Add(new XElement("returnParameterList",
                        from param in (item as DesignerItem_FctGroupConnector).ReturnParameters
                        select new XElement("parameter",
                                    new XElement("ID", param.ID),
                                    new XElement("name", param.Name),
                                    new XElement("type", param.Type))));
                }

                if (item is DesignerItem_StartEnd)
                {
                    functionToAdd.Add(new XElement("isStart", (item as DesignerItem_StartEnd).IsStart.ToString()));
                    functionToAdd.Add(new XElement("isEnd", (item as DesignerItem_StartEnd).IsEnd.ToString()));
                    if ((item as DesignerItem_StartEnd).IsAdditionalEnd)
                        functionToAdd.Add(new XElement("associatedInOutNameID",
                        new XElement("ID", (item as DesignerItem_StartEnd).AssociatedInOutNameID.ID.ToString()),
                        new XElement("name", (item as DesignerItem_StartEnd).AssociatedInOutNameID.Name)));
                }

                serializedItems.Add(functionToAdd);
            }
            return serializedItems;
        }

        //RUG: it was private, but now has been set to public static, as it is also used from DebuggerCanvas
        public static XElement SerializeConnections(IEnumerable<Connection> connections)
        {
            var serializedConnections = new XElement("connections");
            XElement tempConnection = null;

            foreach (Connection itemConnection in connections)
            {
                tempConnection = new XElement("connection",
                                          new XElement("SourceID", itemConnection.Source.AssociatedID),
                                          new XElement("SinkID", itemConnection.Sink.AssociatedID),
                                          new XElement("SourceConnectorName", itemConnection.Source.Name),
                                          new XElement("SinkConnectorName", itemConnection.Sink.Name),
                                          new XElement("zIndex", Canvas.GetZIndex(itemConnection)));
                
                serializedConnections.Add(tempConnection);
            }
            return serializedConnections;
        }

        private DesignerItem DeserializeDesignerItem(XElement itemXML, Guid id, double OffsetX, double OffsetY)
        {
            DesignerItem item;
            string blockName = (string)itemXML.Element("name");
            Guid blockID = (Guid)itemXML.Element("ID");

            // loads the Xaml definition (.Content): look for the element in the toolPalette whose name matches with item
            // and assigns the properties that are not present in the source .xml file
            bool found = false;
            XElement item_xml = null;
            string xamlString = "";
            object children = ClassTreeHelper.FindVisualChildren<ToolboxItem>(this.ToolPalette);
            foreach (ToolboxItem element in (IEnumerable<Control>)children)
            {
                string _name = (element.Content as FrameworkElement).Name;
                if (_name == blockName)
                {
                    found = true;
                    
                    // gets the xaml definition (path)
                    xamlString = XamlWriter.Save(element.Content);
                    
                    // gets the Tag, that contains the related xml portion from FunctionCatalog.xml
                    item_xml = XElement.Parse((string)((FrameworkElement)element.Content).Tag);
                }
            }
            if (!found)
            {
                MessageBox.Show(string.Format("Trying to load a function ({0}) that is not present in the Catalog", blockName),
                                "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            // instantiate the right DesignerItem based on the blockType property
            string type_ = item_xml.Element("blockType").Value;
            if (type_ == null) return null;
            switch (type_)
            {
                case "function": item = new DesignerItem_Fct(blockID); break;
                case "startEnd": item = new DesignerItem_StartEnd(blockID); break;
                case "groupFunction": item = new DesignerItem_FctGroup(blockID); break;
                case "groupFctConnector": item = new DesignerItem_FctGroupConnector(blockID); break;
                default:
                    MessageBox.Show(string.Format("blockType ({0}) not recognized", type_),
                        "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
            }

            item.BlockName = blockName;
            item.Content = XamlReader.Load(XmlReader.Create(new StringReader(xamlString)));
            SetConnectorDecoratorTemplate(item, item_xml.Element("connectionsStyle").Value);

            // loads graphical properties
            item.Width = Double.Parse(itemXML.Element("Width").Value, CultureInfo.InvariantCulture);
            item.Height = Double.Parse(itemXML.Element("Height").Value, CultureInfo.InvariantCulture);
            Canvas.SetLeft(item, Double.Parse(itemXML.Element("Left").Value, CultureInfo.InvariantCulture) + OffsetX);
            Canvas.SetTop(item, Double.Parse(itemXML.Element("Top").Value, CultureInfo.InvariantCulture) + OffsetY);
            Canvas.SetZIndex(item, Int32.Parse(itemXML.Element("zIndex").Value));

            // additional parameters for DesignerItem_StartEnd
            if (item is DesignerItem_StartEnd)
            {
                (item as DesignerItem_StartEnd).IsStart = Convert.ToBoolean(item_xml.Element("isStart").Value);
                XElement AssociatedInOutNameID = itemXML.Element("associatedInOutNameID");
                if (AssociatedInOutNameID != null)
                {
                    (item as DesignerItem_StartEnd).AssociatedInOutNameID =
                        new DesignerItem_Fct.Parent_name_ID(
                            null, AssociatedInOutNameID.Element("name").Value,
                            new Guid(AssociatedInOutNameID.Element("ID").Value));
                }
            }

            // additional parameters for DesignerItem_FctGroupConnector
            // NOTE: the inputParameter and returnParameter are not inherited from DesignerItem nor from DesignerItem_Fct.
            if (item is DesignerItem_FctGroupConnector)
            {
                (item as DesignerItem_FctGroupConnector).AssociatedParamID = new Guid(item_xml.Element("associatedParameterID").Value);

                // loads inputParameters
                IEnumerable<XElement> ParametersXML = itemXML.Elements("parameterList").Elements("parameter");
                foreach (XElement itemParameter in ParametersXML)
                {
                    string name = itemParameter.Element("name").Element("value").Value;
                    string type = itemParameter.Element("type").Element("value").Value;
                    string value = itemParameter.Element("value").Value;
                    string externallyDefined = itemParameter.Element("externallyDefined").Value;
                    Guid ID = new Guid(itemParameter.Element("ID").Value);

                    object paramval = null;
                    if (type.Equals("integer"))
                    {
                        if (Convert.ToBoolean(externallyDefined) == true) break;
                        if (value == "") paramval = 0;
                        else paramval = int.Parse(value);
                    }
                    else if (!value.Equals(""))
                    {
                        paramval = value;
                    }

                    Fct_inParameter newParam = new Fct_inParameter(item as DesignerItem_FctGroupConnector, name, type, paramval, ID, Convert.ToBoolean(externallyDefined));
                    //newParam.ExternallyDefined = Convert.ToBoolean(externallyDefined);
                    (item as DesignerItem_FctGroupConnector).InputParameters.Add(newParam);

                }

                // loads returnParameters
                ParametersXML = itemXML.Elements("returnParameterList").Elements("parameter");
                foreach (XElement itemParameter in ParametersXML)
                {
                    string name = itemParameter.Element("name").Value;
                    string type = itemParameter.Element("type").Value;
                    Guid ID = new Guid(itemParameter.Element("ID").Value);

                    Fct_Parameter newParam = new Fct_Parameter(item as DesignerItem_FctGroupConnector, name, type, null, ID);
                    (item as DesignerItem_FctGroupConnector).ReturnParameters.Add(newParam);

                }
            }

            // additional parameters for DesignerItem_Fct and DesignerItem_FctGroup (inherits from DesignerItem_Fct)
            if (item is DesignerItem_Fct)
            {
                // loads inputParameters
                IEnumerable<XElement> ParametersXML = itemXML.Elements("parameterList").Elements("parameter");
                foreach (XElement itemParameter in ParametersXML)
                {
                    string name = itemParameter.Element("name").Element("value").Value;
                    string type = itemParameter.Element("type").Element("value").Value;
                    string value = itemParameter.Element("value").Value;
                    string externallyDefined = itemParameter.Element("externallyDefined").Value;
                    Guid ID = new Guid(itemParameter.Element("ID").Value);

                    object paramval = null;
                    if (type.Equals("integer"))
                    {
                        if (Convert.ToBoolean(externallyDefined) == true) break;
                        if (value == "") paramval = 0;
                        else paramval = int.Parse(value);
                    }
                    else if (!value.Equals(""))
                    {
                        paramval = value;
                    }

                    Fct_inParameter newParam = new Fct_inParameter(item as DesignerItem_Fct, name, type, paramval, ID, Convert.ToBoolean(externallyDefined));
                    //newParam.ExternallyDefined = Convert.ToBoolean(externallyDefined);
                    (item as DesignerItem_Fct).InputParameters.Add(newParam);

                }

                // loads returnParameters
                ParametersXML = itemXML.Elements("returnParameterList").Elements("parameter");
                foreach (XElement itemParameter in ParametersXML)
                {
                    string name = itemParameter.Element("name").Value;
                    string type = itemParameter.Element("type").Value;
                    Guid ID = new Guid(itemParameter.Element("ID").Value);

                    Fct_Parameter newParam = new Fct_Parameter(item as DesignerItem_Fct, name, type, null, ID);
                    (item as DesignerItem_Fct).ReturnParameters.Add(newParam);

                }

                // set default output connector name
                (item as DesignerItem_Fct).DefaultOutputName = item_xml.Element("defaultOutput").Value;

                // loads additionaOutputs
                ParametersXML = itemXML.Elements("additionalOutputs").Elements("out");
                foreach (XElement itemParameter in ParametersXML)
                {
                    string name = itemParameter.Element("name").Value;
                    Guid ID = new Guid(itemParameter.Element("ID").Value);

                    (item as DesignerItem_Fct).AdditionalOutputs.Add(
                        new DesignerItem_Fct.Parent_name_ID((item as DesignerItem_Fct), name, ID));
                }

                // case of DesignerItem_FctGroup: use the ricorsion to load all sub-elements
                if (item is DesignerItem_FctGroup)
                {
                    IEnumerable<XElement> subItemsXML = itemXML.Elements("functionList").Elements("function");
                    foreach (XElement subItemXML in subItemsXML)
                    {
                        Guid id_ = new Guid(itemXML.Element("ID").Value);
                        DesignerItem item_ = DeserializeDesignerItem(subItemXML, id_, 0, 0);

                        if (item_ == null) return null;

                        if ((item as DesignerItem_FctGroup).SubCanvas == null)
                            (item as DesignerItem_FctGroup).SubCanvas = new DesignerCanvas_Group(ToolPalette);
                        (item as DesignerItem_FctGroup).SubCanvas.Children.Add(item_);

                        // apply the connectorDecoratorTemplate
                        XElement newItem_xml = XElement.Parse((string)((FrameworkElement)item_.Content).Tag);
                        string connectionStyle = newItem_xml.Element("connectionsStyle").Value;
                        if (connectionStyle == String.Empty)
                        {
                            MessageBox.Show(string.Format("TAG connectionsStyle of function ID ({0}) not set in opened file", id_.ToString()),
                                       "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                            return null;
                        }
                        this.SetConnectorDecoratorTemplate(item, connectionStyle);
                    }
                

                    this.InvalidateVisual();
                    IEnumerable<XElement> connectionsXML = itemXML.Elements("connections").Elements("connection");

                    if (Application.Current == null)
                        return null;
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() =>
                        (item as DesignerItem_FctGroup).SubCanvas.LoadConnections(connectionsXML) ));
                }

            }

            return item;
        }
         
        private void CopyCurrentSelection()
        {
            IEnumerable<DesignerItem> selectedDesignerItems =
                this.SelectionService.CurrentSelection.OfType<DesignerItem>();

            List<Connection> selectedConnections =
                this.SelectionService.CurrentSelection.OfType<Connection>().ToList();

            foreach (Connection connection in this.Children.OfType<Connection>())
            {
                if (!selectedConnections.Contains(connection))
                {
                    DesignerItem sourceItem = (from item in selectedDesignerItems
                                               where item.ID == connection.Source.ParentDesignerItem.ID
                                               select item).FirstOrDefault();

                    DesignerItem sinkItem = (from item in selectedDesignerItems
                                             where item.ID == connection.Sink.ParentDesignerItem.ID
                                             select item).FirstOrDefault();

                    if (sourceItem != null &&
                        sinkItem != null &&
                        BelongToSameGroup(sourceItem, sinkItem))
                    {
                        selectedConnections.Add(connection);
                    }
                }
            }

            XElement designerItemsXML = SerializeDesignerItems(selectedDesignerItems);
            XElement connectionsXML = SerializeConnections(selectedConnections);

            XElement root = new XElement("Root");
            root.Add(designerItemsXML);
            root.Add(connectionsXML);

            root.Add(new XAttribute("OffsetX", 10));
            root.Add(new XAttribute("OffsetY", 10));

            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Xaml, root);
        }

        private void DeleteCurrentSelection()
        {
            foreach (Connection connection in SelectionService.CurrentSelection.OfType<Connection>())
            {
                this.Children.Remove(connection);
            }

            foreach (DesignerItem item in SelectionService.CurrentSelection.OfType<DesignerItem>())
            {
                Control cd = item.Template.FindName("PART_ConnectorDecorator", item) as Control;

                List<Connector> connectors = new List<Connector>();
                GetConnectors(cd, connectors);

                foreach (Connector connector in connectors)
                {
                    foreach (Connection con in connector.Connections)
                    {
                        this.Children.Remove(con);
                    }
                }
                this.Children.Remove(item);
            }

            SelectionService.ClearSelection();
            UpdateZIndex();
        }

        private void UpdateZIndex()
        {
            List<UIElement> ordered = (from UIElement item in this.Children
                                       orderby Canvas.GetZIndex(item as UIElement)
                                       select item as UIElement).ToList();

            for (int i = 0; i < ordered.Count; i++)
            {
                Canvas.SetZIndex(ordered[i], i);
            }
        }

        private static Rect GetBoundingRectangle(IEnumerable<DesignerItem> items)
        {
            double x1 = Double.MaxValue;
            double y1 = Double.MaxValue;
            double x2 = Double.MinValue;
            double y2 = Double.MinValue;

            foreach (DesignerItem item in items)
            {
                x1 = Math.Min(Canvas.GetLeft(item), x1);
                y1 = Math.Min(Canvas.GetTop(item), y1);

                x2 = Math.Max(Canvas.GetLeft(item) + item.Width, x2);
                y2 = Math.Max(Canvas.GetTop(item) + item.Height, y2);
            }

            return new Rect(new Point(x1, y1), new Point(x2, y2));
        }

        private static Rect GetBoundingRectangle(List<Control> items)
        {
            double x1 = Double.MaxValue;
            double y1 = Double.MaxValue;
            double x2 = Double.MinValue;
            double y2 = Double.MinValue;

            foreach (Control item in items)
            {
                x1 = Math.Min(Canvas.GetLeft(item), x1);
                y1 = Math.Min(Canvas.GetTop(item), y1);

                x2 = Math.Max(Canvas.GetLeft(item) + item.Width, x2);
                y2 = Math.Max(Canvas.GetTop(item) + item.Height, y2);
            }

            return new Rect(new Point(x1, y1), new Point(x2, y2));
        }

        private void GetConnectors(DependencyObject parent, List<Connector> connectors)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child is Connector)
                {
                    connectors.Add(child as Connector);
                }
                else
                    GetConnectors(child, connectors);
            }
        }

        protected Connector GetConnector(Guid itemID, String connectorName) //RUG: ConnectorName is Top, Bottom, Left, Right
        {
            DesignerItem designerItem = null; 
            bool found = false;
            bool foundInDesignerItem = false;

            //looks for the designerItem object associated to the connector
            foreach(DesignerItem dItem in this.Children.OfType<DesignerItem>())
            {
                //as first check if the connector is associated with a DesignerItem (IsInput || IsDefaultOutput)
                if (dItem.ID == itemID)
                {
                    designerItem = dItem;
                    found = true;
                    foundInDesignerItem = true;
                }
                if (found) break;

                if (dItem is DesignerItem_Fct)
                {
                    // if not checks if is associated with a inputParameter (IsInputParameter)
                    foreach (Fct_Parameter pItem in (dItem as DesignerItem_Fct).InputParameters)
                    {
                        if (pItem.ID == itemID)
                        {
                            designerItem = dItem;
                            found = true;
                        }
                    }
                    if (found) break;

                    // if not checks if is associated with a returnParameter (IsReturnParameter)
                    foreach (Fct_Parameter rpItem in (dItem as DesignerItem_Fct).ReturnParameters)
                    {
                        if (rpItem.ID == itemID)
                        {
                            designerItem = dItem;
                            found = true;
                        }
                    }
                    if (found) break;

                    // if not checks if is associated with a additionalOutput (IsAdditionalOutput)
                    foreach (DesignerItem_Fct.Parent_name_ID rpItem in (dItem as DesignerItem_Fct).AdditionalOutputs)
                    {
                        if (rpItem.ID == itemID)
                        {
                            designerItem = dItem;
                            found = true;
                        }
                    }
                    if (found) break;
                }

                if (dItem is DesignerItem_FctGroupConnector)
                {
                    // if not checks if is associated with a inputParameter (IsInputParameter)
                    foreach (Fct_Parameter pItem in (dItem as DesignerItem_FctGroupConnector).InputParameters)
                    {
                        if (pItem.ID == itemID)
                        {
                            designerItem = dItem;
                            found = true;
                        }
                    }
                    if (found) break;

                    // if not checks if is associated with a returnParameter (IsReturnParameter)
                    foreach (Fct_Parameter rpItem in (dItem as DesignerItem_FctGroupConnector).ReturnParameters)
                    {
                        if (rpItem.ID == itemID)
                        {
                            designerItem = dItem;
                            found = true;
                        }
                    }
                    if (found) break;
                }

            }
            
            Control connectorDecorator = designerItem.Template.FindName("PART_ConnectorDecorator", designerItem) as Control;
            object connectors = ClassTreeHelper.FindVisualChildren<Connector>(connectorDecorator);
            
            if (connectors == null)
            {
                MessageBox.Show("Erro by retrieving the connector",
                               "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            /* this extra IF is needed because if we look for Out or In connector, we will never find the
             * correspondence just by looping through the connectors and checking .AssociatedID==itemID;
             * in fact the ID we wante to look for is the designerItem.ID!! (not in connectors)      */
            if (connectorName == "In" || connectorName == "Out")
                foreach (Connector element in (IEnumerable<Control>)connectors)
                    if (element.Name == connectorName)
                    {
                        // in this case we only return the connector if the given ID is the ID
                        // of the designer item. This ot avoid wrong matches (i.e. passed the ID
                        // of the parameter/additionalOut and the connectorName = "Out" or "In")
                        if (!foundInDesignerItem)
                            return null;
                        return element;
                    }

            foreach (Connector element in (IEnumerable<Control>)connectors)
                if (element.AssociatedID == itemID)
                    return element;

            return null;
        }

        public static Connector GetConnector(DesignerItem designerItem, Guid itemID, String connectorName) //RUG: ConnectorName is Top, Bottom, Left, Right
        {
            Control connectorDecorator = designerItem.Template.FindName("PART_ConnectorDecorator", designerItem) as Control;
            object connectors = ClassTreeHelper.FindVisualChildren<Connector>(connectorDecorator);

            if (connectorName == "In" || connectorName == "Out")
                foreach (Connector element in (IEnumerable<Control>)connectors)
                    if (element.Name == connectorName)
                        return element;

            foreach (Connector element in (IEnumerable<Control>)connectors)
                if (element.AssociatedID == itemID)
                    return element;

            return null;
        }

        private bool BelongToSameGroup(IGroupable item1, IGroupable item2)
        {
            IGroupable root1 = SelectionService.GetGroupRoot(item1);
            IGroupable root2 = SelectionService.GetGroupRoot(item2);

            return (root1.ID == root2.ID);
        }

        protected string GetXamlTag(StackPanel toolPalette, string BlockName)
        {
            //retrieve the Tag property of the ToolBoxItem that contains the xml code from the FunctionCatalog
            object children = ClassTreeHelper.FindVisualChildren<ToolboxItem>(toolPalette);
            foreach (ToolboxItem element in (IEnumerable<Control>)children)
            {
                string _name = (element.Content as FrameworkElement).Name;
                if (_name == BlockName)
                    return XamlWriter.Save(element.Content);
            }

            MessageBox.Show(string.Format("Erro by retrieving the Group Icon graphical properties.\nElement:" +
                                          " {0} not found in the FunctionCatalog", BlockName),
                                "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);  
            return "";
        }

        #endregion
    }
}
