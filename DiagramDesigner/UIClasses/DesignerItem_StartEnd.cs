﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DiagramDesigner
{
    // Define the Start or End DesignerItem. Is also valid for subItems.
    public class DesignerItem_StartEnd : DesignerItem
    {
        // if IsStart is set to true, then automatically isEnd is set to false and vice versa
        // these param are used in the connectorDecorator to show whether the In or the Out connector

        public DesignerItem_StartEnd() : base() {}
        public DesignerItem_StartEnd(Guid id) : base(id) {}

        #region IsStart
        private bool isStart;
        public bool IsStart
        {
            get { return isStart; }
            set
            {
                isStart = value;
                isEnd = !value;
                //AssociatedInOutNameID = null;
            }
        }
        #endregion

        #region IsEnd
        private bool isEnd;
        public bool IsEnd
        {
            get { return isEnd; }
            set
            {
                isEnd = value;
                isStart = !value;
                //AssociatedInOutNameID = null;
            }
        }
        #endregion

        public bool IsAdditionalEnd
        {
            get { return AssociatedInOutNameID != null; }
        }

        #region AassociatedInOutNameID

        public DesignerItem_Fct.Parent_name_ID AssociatedInOutNameID
        {
            get { return (DesignerItem_Fct.Parent_name_ID)GetValue(AssociatedInOutNameIDProperty); }
            set {
                isStart = false;
                isEnd = false;
                SetValue(AssociatedInOutNameIDProperty, value);
            }
        }
        public static readonly DependencyProperty AssociatedInOutNameIDProperty =
            DependencyProperty.Register("AssociatedInOutNameID", typeof(DesignerItem_Fct.Parent_name_ID), typeof(DesignerItem_StartEnd));
        #endregion

    }
}
