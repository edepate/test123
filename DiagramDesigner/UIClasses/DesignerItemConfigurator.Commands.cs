﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DiagramDesigner
{
    public partial class DesignerItemConfigurator : Control
    {
        public static RoutedCommand TEST = new RoutedCommand();

        public DesignerItemConfigurator()
        {
            this.CommandBindings.Add(new CommandBinding(TEST, TEST_Executed));
        }

        private void TEST_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (designerItem == null) return;
            
            DataGrid paramList = this.Template.FindName("paramList", this) as DataGrid;
            if (paramList.SelectedItem == null) return;

            int selIdx = paramList.SelectedIndex;

            Fct_Parameter a = ((ObservableCollection<Fct_inParameter>)paramList.ItemsSource)[selIdx].getAssociatedReturnParameter();
        }
    }


}
