﻿using System.Windows;
using System.Windows.Controls;

namespace DiagramDesigner
{
    public class Fct_Parameter_Xaml : ContentControl
    {
        public string _PropName
        {
            get { return (string)GetValue(_PropNameProperty); }
            set { SetValue(_PropNameProperty, value); }
        }
        public static readonly DependencyProperty _PropNameProperty =
            DependencyProperty.Register("_PropName", typeof(string), typeof(Fct_Parameter_Xaml));

        public string _Type
        {
            get { return (string)GetValue(_TypeProperty); }
            set { SetValue(_TypeProperty, value); }
        }
            public static readonly DependencyProperty _TypeProperty =
         DependencyProperty.Register("_Type", typeof(string), typeof(Fct_Parameter_Xaml));

        public string _Value
        {
            get { return (string)GetValue(_ValueProperty); }
            set { SetValue(_ValueProperty, value); }
        }
        public static readonly DependencyProperty _ValueProperty =
            DependencyProperty.Register("_Value", typeof(string), typeof(Fct_Parameter_Xaml));

        public bool _ExternallyDefined
        {
            get { return (bool)GetValue(_ExternallyDefinedProperty); }
            set { SetValue(_ExternallyDefinedProperty, value); }
        }
        public static readonly DependencyProperty _ExternallyDefinedProperty =
            DependencyProperty.Register("_Name", typeof(bool), typeof(Fct_Parameter_Xaml));

        public bool _ReturnsValue
        {
            get { return (bool)GetValue(_ReturnsValueProperty); }
            set { SetValue(_ReturnsValueProperty, value); }
        }
        public static readonly DependencyProperty _ReturnsValueProperty =
            DependencyProperty.Register("_ReturnsValue", typeof(bool), typeof(Fct_Parameter_Xaml));

    }
}
