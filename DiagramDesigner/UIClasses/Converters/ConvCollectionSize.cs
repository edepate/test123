﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace DiagramDesigner
{
    class ConvCollectionSize : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString(); //(value as Collection<object>).Count.ToString();

            //return 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
