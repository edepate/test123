﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace DiagramDesigner
{
    public partial class DesignerItemConfigurator: Control
    {
        public DesignerItem designerItem
        {
            get { return (DesignerItem)GetValue(designerItemProperty); }
            set {
                SetValue(designerItemProperty, value);
            }
        }
        public static readonly DependencyProperty designerItemProperty =
            DependencyProperty.Register("designerItem", typeof(DesignerItem), typeof(DesignerItemConfigurator));

        //public int InputParametersCount
        //{
        //    get
        //    {
        //        if (designerItem == null)
        //            return 0;
        //        if (designerItem.InputParameters == null)
        //            return 0;
        //        return designerItem.InputParameters.Count();
        //    }
        //}
    }


}
