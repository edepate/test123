﻿using System.Windows;
using System.Windows.Controls;

namespace DiagramDesigner
{
    // Implements ItemsControl for ToolboxItems    
    public class Toolbox : ItemsControl
    {
        // Defines the ItemHeight and ItemWidth properties of
        // the WrapPanel used for this Toolbox
        public Size ItemSize
        {
            get {
               
                
                return itemSize; }
            set { itemSize = value; }
        }
        private Size itemSize = new Size(50, 50);

        // Creates or identifies the element that is used to display the given item.  
        /* RUG: GetContainerForItemOverride returns the container for items.      
          Each ItemsControl should specify its own type of item container. For example, ListBox
          returns instance of ListBoxItem in this method. When you create your own ItemsControl
          you can override this method and return custom container.
         */
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new ToolboxItem();
        }

        // Determines if the specified item is (or is eligible to be) its own container.        
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return (item is ToolboxItem);
        }
    }
}
