﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;

namespace DiagramDesigner
{
    public class DesignerCanvas_Group : DesignerCanvas
    {
        /**************************************************
         *  Group DesignerCanvas: contains Properties and Methods to
         *  handle the grouping
         **************************************************/

        /* When a group is created, a new DesignerItem (DesignerItem_FctGroup) will be created and represents the group.
        * The connections with outside will be connected to the DesignerItem_FctGroup. If the connector is over parameters,
        * each parameter connector of DesignerItem_FctGroup associated to a parameter connector of internal DesignerItem_Fct
        * will be represented in the DesignerItem_FctGroup.subCanvas as DesignerItem_GroupConnector.
        * The Queue ext2intConn_ToRedrawQueue stores these references as long as the redrawing is not complete.
        * For In/Out connector the representative item is the DesginerItem_StartEnd.
        */

        public DesignerCanvas_Group(StackPanel _toolPalette) : base(_toolPalette)
        {
            ext2intConn_ToRedrawQueue = new ObservableQueue<FctGroup_BoundaryRef2InternRefID>();
            ext2intConn_ToRedrawQueue.CollectionChanged += Ext2intConn_ToRedrawQueue_CollectionChanged;
            this.Loaded += DesignerCanvas_Loaded;
        }
        
        /* structure to stores:
         * CASE 1) InputParameter/ReturnParameter
         *     - FctGroupParameter is the reference to the DesignerItem_GroupItem.InputParameter or .ReturnParameter
         *     - itemInternRefID is the ID of the InputParameter/ReturnParameter in the associated DesignerItem_Fct (in the group)
         * CASE 2) In/Out
         *     - FctGroupParameter is null
         *     - itemInternRefID is the reference to the In/Out connector of the associated DesignerItem_Fct (in the group)
        */
        public struct FctGroup_BoundaryRef2InternRefID
        {
            //reference to the parameter 
            public object itemFctGroupRef;
            public Guid itemInternRefID;
            //public string itemInternInOrOut;

            public FctGroup_BoundaryRef2InternRefID(object _itemFctGroupRef, Guid _itemInternRefID)
            {
                itemFctGroupRef = _itemFctGroupRef;
                itemInternRefID = _itemInternRefID;
            }
        }

        public class ObservableQueue<T> : Queue<T>, INotifyCollectionChanged, INotifyPropertyChanged
        {
            public virtual event NotifyCollectionChangedEventHandler CollectionChanged;
            protected virtual event PropertyChangedEventHandler PropertyChanged;

            public ObservableQueue() { }

            public new virtual void Clear()
            {
                base.Clear();
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }

            public new virtual T Dequeue()
            {
                var item = base.Dequeue();
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                return item;
            }

            public new virtual void Enqueue(T item)
            {
                base.Enqueue(item);
                this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            }

            protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
                this.RaiseCollectionChanged(e);
            }

            protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
            {
                this.RaisePropertyChanged(e);
            }

            private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
            {
                if (this.CollectionChanged != null)
                    this.CollectionChanged(this, e);
            }

            private void RaisePropertyChanged(PropertyChangedEventArgs e)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, e);
            }

            event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
            {
                add { this.PropertyChanged += value; }
                remove { this.PropertyChanged -= value; }
            }
        }

        #region ext2intConn_ToRedrawQueue 

        public ObservableQueue<FctGroup_BoundaryRef2InternRefID> ext2intConn_ToRedrawQueue;

        // the CollectionChanged draw the DesignerItem_FctGroupConnector or DesignerItem_StartEnd
        private void Ext2intConn_ToRedrawQueue_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems == null) return; // exit if is called from Dequeue (during DesignerCanvas_Loaded)
            foreach (FctGroup_BoundaryRef2InternRefID item in e.NewItems)
            {
                DesignerItem const_ = null;
                string xamlString = "";
                object _itemRef = item.itemFctGroupRef;

                // handles In/Out --> Start/End connectors
                if ((_itemRef is DesignerItem_Fct.Parent_name_ID) || (_itemRef is string))
                {
                    
                    const_ = new DesignerItem_StartEnd();
                    const_.Width = 40;
                    const_.Height = 30;
                    SetConnectorDecoratorTemplate(const_, "default");
                    Canvas.SetZIndex(const_, 1);
                    
                    if (_itemRef is string)
                    {
                        if ((string)_itemRef == "In")
                        {
                            xamlString = GetXamlTag(this.ToolPalette, "Start");
                            if (xamlString == "") return;
                            const_.BlockName = "Start";
                            (const_ as DesignerItem_StartEnd).IsStart = true;
                            Canvas.SetTop(const_, 10);
                            Canvas.SetLeft(const_, 10);
                        }
                        else if ((string)_itemRef == "Out")
                        {
                            xamlString = GetXamlTag(this.ToolPalette, "End");
                            if (xamlString == "") return;
                            const_.BlockName = "End";
                            (const_ as DesignerItem_StartEnd).IsStart = false;
                            Canvas.SetTop(const_, this.Height - 10 - const_.Height);
                            Canvas.SetLeft(const_, 10);
                        }
                    }
                    else if (_itemRef is DesignerItem_Fct.Parent_name_ID)
                    {
                        DesignerItem_Fct.Parent_name_ID _ref = (DesignerItem_Fct.Parent_name_ID)item.itemFctGroupRef;
                        xamlString = GetXamlTag(this.ToolPalette, "End");
                        if (xamlString == "") return;
                        const_.BlockName = "End";
                        
                        // for additionalOut we need to add also some extra properties, like the connector name
                        // that will be shown
                        (const_ as DesignerItem_StartEnd).AssociatedInOutNameID =
                            new DesignerItem_Fct.Parent_name_ID(_ref.Parent, _ref.Name, _ref.ID);
                        Canvas.SetTop(const_, this.Height - 10 - const_.Height);
                        Canvas.SetLeft(const_, 10);
                    }
                    const_.Content = XamlReader.Load(XmlReader.Create(new StringReader(xamlString)));

                }

                // handles parameter connectors
                if (item.itemFctGroupRef is Fct_Parameter)
                {   // shared properties of Fct_Parameter and Fct_inParameter

                    Fct_Parameter _ref = (Fct_Parameter)item.itemFctGroupRef;
                    const_ = new DesignerItem_FctGroupConnector();

                    // creates the link between the connector and the parameter of the DesignerItem_FctGroupConnector this
                    // connector is related to
                    (const_ as DesignerItem_FctGroupConnector).AssociatedParamID = _ref.ID;
                    const_.BlockName = "GroupConnector";
                    const_.Width = 40;
                    const_.Height = 20;
                    Canvas.SetZIndex(const_, 1);
                    xamlString = GetXamlTag(this.ToolPalette, "GroupConnector");
                    if (xamlString == "") return;
                    const_.Content = XamlReader.Load(XmlReader.Create(new StringReader(xamlString)));

                    if (_ref is Fct_inParameter)
                    {   // dedicated properties for Fct_inParameter
                        Fct_Parameter tmp = new Fct_Parameter(
                            const_,
                            _ref.Name,
                            _ref.Type,
                            null);
                        Canvas.SetTop(const_, 10);
                        int left_ = (from _item in this.Children.OfType<DesignerItem_FctGroupConnector>()
                                     where _item.ReturnParameters.Count > 0
                                     select _item).Count();
                        Canvas.SetLeft(const_, 10 + (1 + left_) * 50);
                        (const_ as DesignerItem_FctGroupConnector).ReturnParameters.Add(tmp);
                    }
                    else 
                    {   // dedicated properties for Fct_Parameter 
                        Fct_inParameter tmp = new Fct_inParameter(
                            const_,
                            _ref.Name,
                            _ref.Type,
                            null, true);
                        Canvas.SetTop(const_, this.Height - 10 - const_.Height);
                        int left_ = (from _item in this.Children.OfType<DesignerItem_FctGroupConnector>()
                                     where _item.InputParameters.Count > 0
                                     select _item).Count();
                        Canvas.SetLeft(const_, 10 + (1 + left_) * 50);
                        (const_ as DesignerItem_FctGroupConnector).InputParameters.Add(tmp);
                    }
                }

                if (const_ != null)
                {
                    this.Children.Add(const_);
                    SetConnectorDecoratorTemplate(const_, "default");
                }
            }
        }
        #endregion

        /* in DesignerCanvas_Loaded the connectors between DesignerItem_FctGroupConnector - internal DesignerItem_Fct as
         * well as the connectors between DesignerItem_StartEnd - internal DesignerItem_Fct are created.
         * IMP: this function is fired whenever a new object will be added to the children and his render completed.
         *      It's the best alternative to wait before accessing the DesignerItem's connectors (that in fact are
         *      accessible only after the render). Waiting for the dispatcher does not work in this case (in some cases
         *      works) */
        private void DesignerCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            while (ext2intConn_ToRedrawQueue.Count > 0)
            {
                FctGroup_BoundaryRef2InternRefID item = ext2intConn_ToRedrawQueue.Dequeue();
                Connector source = null;
                Connector sink = null;
                Guid GroupConnector_paramID = Guid.Empty;
                object _itemRef = item.itemFctGroupRef;

                // handles Input/Return parameter connections
                if (_itemRef is Fct_Parameter)
                {   // shared settings for Fct_Parameter and Fct_inParameter

                    Fct_Parameter _ref = (Fct_Parameter)_itemRef;
                    // if the GroupConnector is not associated then do not draw the connection (it might happen that
                    // the connection is manualy deleted after the groupin by the user)
                    if (item.itemInternRefID == Guid.Empty) continue;

                    // looks for constant input connectors whose return parameter's ID matches with the parameterID
                    // of the actual ext2intConn_ToRedrawQueue.
                    foreach (DesignerItem_FctGroupConnector iter in this.Children.OfType<DesignerItem_FctGroupConnector>())
                        if (iter.AssociatedParamID == _ref.ID)
                        {
                            if (iter.ReturnParameters.Count > 0)
                            {
                                GroupConnector_paramID = iter.ReturnParameters[0].ID;
                                break;
                            }
                            if (iter.InputParameters.Count > 0)
                            {
                                GroupConnector_paramID = iter.InputParameters[0].ID;
                                break;
                            }
                        }

                    if (GroupConnector_paramID == Guid.Empty)
                    {
                        MessageBox.Show("Correspondence groupFunction's inputParameter - GroupConnector not found",
                                    "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if (_ref is Fct_inParameter)
                    {   // handles Fct_inParameter: FctGroup.InputParameter --> GroupConnector
                        source = GetConnector(GroupConnector_paramID, "returnParamConn_i");
                        // only if no connection is associated to the connector, then re-generate the connection.
                        if (!(source.Connections.Count == 0)) continue;
                        sink = GetConnector(item.itemInternRefID, "inputParamConn_i");
                    }
                    else
                    {   // handles Fct_Parameter: GroupConnector --> FctGroup.ReturnParameter
                        sink = GetConnector(GroupConnector_paramID, "inputParamConn_i");
                        // only if no connection is associated to the connector, then re-generate the connection.
                        if (!(sink.Connections.Count == 0)) continue;
                        source = GetConnector(item.itemInternRefID, "returnParamConn_i");
                    }
                }

                // handles In/Out --> Start/End connectors
                if (_itemRef is string)
                {
                    // note that only one block "In" can be present in the group. Same for "Out". Therefore
                    // on the first hit we berak;
                    if ((string)_itemRef == "In")
                        foreach (DesignerItem_StartEnd iter in this.Children.OfType<DesignerItem_StartEnd>())
                            if (iter.IsStart)
                            {
                                source = GetConnector(iter.ID, "Out");
                                // only if no connection is associated to the connector, then re-generate the connection.
                                if (!(source.Connections.Count == 0)) continue;
                                sink = GetConnector(item.itemInternRefID, "In");
                                break;
                            }
                    if ((string)_itemRef == "Out")
                        foreach (DesignerItem_StartEnd iter in this.Children.OfType<DesignerItem_StartEnd>())
                            if (iter.IsEnd)
                            {
                                sink = GetConnector(iter.ID, "In");
                                if (!(sink.Connections.Count == 0)) continue;
                                source = GetConnector(item.itemInternRefID, "Out");
                                break;
                            }
                }

                if (_itemRef is DesignerItem_Fct.Parent_name_ID)
                {
                    foreach (DesignerItem_StartEnd iter in this.Children.OfType<DesignerItem_StartEnd>())
                        if (iter.AssociatedInOutNameID != null)
                        {
                            sink = GetConnector(iter.ID, "In_fromAdditionalOut");
                            // only if no connection is associated to the connector, then re-generate the connection.
                            if (!(sink.Connections.Count == 0)) continue;
                            source = GetConnector(item.itemInternRefID, "additionalOutConn_i");
                        }
                }

                if ((source == null) || (sink == null))
                {
                    MessageBox.Show("Connector not found during drawing of connection with outside in FctGroup",
                                "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    //return;
                }

                Connection connection = new Connection(source, sink);
                this.Children.Add(connection);

            }
        }
    }
}
