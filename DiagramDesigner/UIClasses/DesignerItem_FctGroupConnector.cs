﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DiagramDesigner
{
    /**************************************************
     *  Group Internal Connector: defines the connectors that reflects the connections coming/going
     * from/to outside the group function
     **************************************************/
    public class DesignerItem_FctGroupConnector : DesignerItem
    {
        public DesignerItem_FctGroupConnector() : base()
        {
            InputParameters = new ObservableCollection<Fct_Parameter>();
            ReturnParameters = new ObservableCollection<Fct_Parameter>();
        }
        public DesignerItem_FctGroupConnector(Guid id) : base(id)
        {
            InputParameters = new ObservableCollection<Fct_Parameter>();
            ReturnParameters = new ObservableCollection<Fct_Parameter>();
        }

        #region InputParameters
        public ObservableCollection<Fct_Parameter> InputParameters
        {
            get { return (ObservableCollection<Fct_Parameter>)GetValue(inputParametersProperty); }
            set { SetValue(inputParametersProperty, value); }
        }
        public static readonly DependencyProperty inputParametersProperty =
            DependencyProperty.Register("InputParameters", typeof(ObservableCollection<Fct_Parameter>), typeof(DesignerItem_FctGroupConnector));
        #endregion

        #region ReturnParameters
        public ObservableCollection<Fct_Parameter> ReturnParameters
        {
            get { return (ObservableCollection<Fct_Parameter>)GetValue(ReturnParametersProperty); }
            set { SetValue(ReturnParametersProperty, value); }
        }
        public static readonly DependencyProperty ReturnParametersProperty =
            DependencyProperty.Register("ReturnParameters", typeof(ObservableCollection<Fct_Parameter>), typeof(DesignerItem_FctGroupConnector));
        #endregion
        
        // The .AssociatedParamID is == to the parameter.ID associated to the groupItem
        // (ReturnParameter or InputParameter)
        public Guid AssociatedParamID { get; set; }

    }
}
