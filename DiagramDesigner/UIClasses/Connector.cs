﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace DiagramDesigner
{
    public class Connector : Control, INotifyPropertyChanged
    {
        #region AssociatedID
        //RUG: needed to create 1:1 correspondence with associated Fct_Parameter
        private Guid associatedID;
        public Guid AssociatedID
        {
            get
            {
                if (associatedID == Guid.Empty)  // RUG: is not null if the connection already exists
                {
                    object dataContext = this.DataContext;
                    if (dataContext is DesignerItem)
                        associatedID = (dataContext as DesignerItem).ID;
                    if (dataContext is Fct_Parameter)
                        associatedID = (dataContext as Fct_Parameter).ID;
                    // if is an AdditionalOut, then we return the associated ID
                    if (dataContext is DesignerItem_Fct.Parent_name_ID)
                        associatedID = ((DesignerItem_Fct.Parent_name_ID)dataContext).ID;
                }

                return associatedID;
            }
        }
        #endregion

        #region connector identifiers
        // IsInputParameter and IsReturnOutputParameter defines if the connector is associated to the parameters
        // IsInput and IsDefaultOutputParameter defines if the connector is associated to the execution arrow
        public bool IsInputParameter
        {
            get { return (bool)GetValue(IsInputParameterProperty); }
            set {
                SetValue(IsInputParameterProperty, value);
                OnPropertyChanged("IsInputParameter");
            }
        }
        public static readonly DependencyProperty IsInputParameterProperty =
            DependencyProperty.Register("IsInputParameter", typeof(bool), typeof(Connector), new PropertyMetadata(false));

        public bool IsReturnParameter
        {
            get { return (bool)GetValue(IsReturnParameterProperty); }
            set {
                SetValue(IsReturnParameterProperty, value);
                OnPropertyChanged("IsReturnParameter");
            }
        }
        public static readonly DependencyProperty IsReturnParameterProperty =
            DependencyProperty.Register("IsReturnParameter", typeof(bool), typeof(Connector), new PropertyMetadata(false));

        public bool IsInput
        {
            get { return (bool)GetValue(IsInputProperty); }
            set {
                SetValue(IsInputProperty, value);
                OnPropertyChanged("IsInput");
            }
        }
        public static readonly DependencyProperty IsInputProperty =
            DependencyProperty.Register("IsInput", typeof(bool), typeof(Connector), new PropertyMetadata(false));

        public bool IsDefaultOutput
        {
            get { return (bool)GetValue(IsDefaultOutputProperty); }
            set {
                SetValue(IsDefaultOutputProperty, value);
                OnPropertyChanged("IsDefaultOutput");
            }
        }
        public static readonly DependencyProperty IsDefaultOutputProperty =
            DependencyProperty.Register("IsDefaultOutput", typeof(bool), typeof(Connector), new PropertyMetadata(false));

        public bool IsAdditionalOutput
        {
            get { return (bool)GetValue(IsAdditionalOutputProperty); }
            set
            {
                SetValue(IsAdditionalOutputProperty, value);
                OnPropertyChanged("IsAdditionalOutput");
            }
        }
        public static readonly DependencyProperty IsAdditionalOutputProperty =
            DependencyProperty.Register("IsAdditionalOutput", typeof(bool), typeof(Connector), new PropertyMetadata(false));
        #endregion

        // drag start point, relative to the DesignerCanvas
        private Point? dragStartPoint = null;

        public ConnectorOrientation Orientation { get; set; }

        // center position of this Connector relative to the DesignerCanvas
        private Point position;
        public Point Position
        {
            get { return position; }
            set
            {
                if (position != value)
                {
                    position = value;
                    OnPropertyChanged("Position"); // RUG: the class connection will register to this event both its source and
                    // sink connectors for the handler OnConnectorPositionChanged. The handler will update the geometry of
                    // the connection path. This is the reason why we notify here the change of the "Position"
                }
            }
        }

        // the DesignerItem this Connector belongs to;
        // retrieved from DataContext, which is set in the
        // DesignerItem template
        // RUG: this property is set directly from the GET method (Also for the first time)
        private DesignerItem parentDesignerItem;
        public DesignerItem ParentDesignerItem
        {
            get
            {
                if (parentDesignerItem == null)  // RUG: is not null if the connection already exists
                {
                    object dataContext = this.DataContext;
                    if (dataContext is DesignerItem)
                        parentDesignerItem = dataContext as DesignerItem;
                    //if (dataContext is DesignerItem_FctGroupConnector)
                    //    parentDesignerItem = dataContext as DesignerItem_FctGroupConnector;
                    if (dataContext is Fct_Parameter)
                        parentDesignerItem = (dataContext as Fct_Parameter).ParentDesignerItem;
                    if (dataContext is DesignerItem_Fct.Parent_name_ID)
                        parentDesignerItem = ((DesignerItem_Fct.Parent_name_ID)dataContext).Parent;
                }

                return parentDesignerItem;
            }
        }

        // keep track of connections that link to this connector
        private List<Connection> connections;
        public List<Connection> Connections
        {
            get
            {
                if (connections == null)
                    connections = new List<Connection>();
                return connections;
            }
        }

        public Connector()
        {
            // fired when layout changes 
            base.LayoutUpdated += new EventHandler(Connector_LayoutUpdated); // RUG: when the connector is created, it subscribes
            // for the "LayoutUpdated" event the handler Connector_LayoutUpdated. This handler will update the position of the
            // connector each time there is a change in the layout. Without this the connector would be stuck always in the same
            // position if there is any moving of the parent Item.
        }

        // when the layout changes we update the position property
        void Connector_LayoutUpdated(object sender, EventArgs e)
        {
            DesignerCanvas designer = GetDesignerCanvas(this);
            if (designer != null)
            {
                //get centre position of this Connector relative to the DesignerCanvas
                // LUK: DEFINES WHERE THE TEMPORARY CONNECTION STARTS - LINE

                this.Position = this.TransformToAncestor(designer).Transform(new Point(this.Width / 2, this.Height / 2));
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DesignerCanvas canvas = GetDesignerCanvas(this);
            if (canvas != null)
            {
                // position relative to DesignerCanvas
                this.dragStartPoint = new Point?(e.GetPosition(canvas));
                e.Handled = true;
            }
        }

        // LUK: This function is triggered only if you hover over connector
        // IT ALSO Triggers the connection line creation
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            // if mouse button is not pressed we have no drag operation, ...
            if (e.LeftButton != MouseButtonState.Pressed)
                this.dragStartPoint = null;

            // but if mouse button is pressed and start point value is set we do have one
            if (this.dragStartPoint.HasValue)
            {
                // create connection adorner 
                DesignerCanvas canvas = GetDesignerCanvas(this); //RUG: returns the main designer canvas, myDesign 
                //(see Window1.xaml), also if the element is inside a group
                if (canvas != null)
                {
                    /*RUG: GetAdornerLayer gets the layer the adorner can be applied to. If for example we want to add
                           an adorner to the button "Btn", we need to use the method Add from the Btn associated adorner
                           layer:
                             AdronerLayer Btn_al = AdornerLayer.GetAdornerLayer(Btn);
                             Btn_al.Add(new FourBoxes(Btn));
                           where "FourBoxes" is the name of the Adorner we want to associate to.
                           Here, OnMouseMove, we associate the ConnectorAdorner to the canvas.
                    */
                    AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(canvas);
                    if (adornerLayer != null)
                    {
                        // exits if the connection is started from incoming execution connector. It must flow from outcoming
                        // execution connector to incoming execution connector
                        if (this.IsInput) return;

                        // exits if the connection is started form an inputParameter. Must start always from a returnParameter
                        // or from an additionalReturnParameter
                        if (this.IsInputParameter) return;

                        ConnectorAdorner adorner = new ConnectorAdorner(canvas, this);
                        if (adorner != null)
                        {
                            adornerLayer.Add(adorner);
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        internal ConnectorInfo GetInfo()
        {
            ConnectorInfo info = new ConnectorInfo();
            info.DesignerItemLeft = DesignerCanvas.GetLeft(this.ParentDesignerItem);
            info.DesignerItemTop = DesignerCanvas.GetTop(this.ParentDesignerItem);
            info.DesignerItemSize = new Size(this.ParentDesignerItem.ActualWidth, this.ParentDesignerItem.ActualHeight);
            info.Orientation = this.Orientation;
            info.Position = this.Position;
            return info;
        }

        // iterate through visual tree to get parent DesignerCanvas
        private DesignerCanvas GetDesignerCanvas(DependencyObject element)
        {
            while (element != null && !(element is DesignerCanvas))
                element = VisualTreeHelper.GetParent(element);

            return element as DesignerCanvas;
        }

        //RUG: added by me
        public DesignerItem GetDesignerItem(DependencyObject element)
        {
            while (element != null && !(element is DesignerItem))
                element = VisualTreeHelper.GetParent(element);

            return element as DesignerItem;
        }

        #region INotifyPropertyChanged Members

        // we could use DependencyProperties as well to inform others of property changes
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }

    // provides compact info about a connector; used for the 
    // routing algorithm, instead of hand over a full fledged Connector
    internal struct ConnectorInfo
    {
        public double DesignerItemLeft { get; set; }
        public double DesignerItemTop { get; set; }
        public Size DesignerItemSize { get; set; }
        public Point Position { get; set; }
        public ConnectorOrientation Orientation { get; set; }
    }

    //RUG: this value reflects what has been specified in xaml for the "Orientation" property of s:Connector
    public enum ConnectorOrientation
    {
        None,
        Left,
        Top,
        Right,
        Bottom
    }
}
