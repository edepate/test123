﻿//using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Windows;
using System.Windows.Media;

/*
RUG: sometimes does not work if the object to look for is in another branch of the tree. Use instead
     object f = LogicalTreeHelper.FindLogicalNode((DependencyObject)obj, "MyDesigner")  (with the restriction
     that we have to provide the name)
     

RUG: creted this class to get all the children of a certaing type of a given object. Usage:

    1) Looping over all object that match the pattern:
        foreach(System.Windows.Shapes.Path path in ClassTreeHelper.FindVisualChildren<System.Windows.Shapes.Path>(this))
        {
            ...
        }

    2) Get only the first object (we have to include System.Linq to use .First() method:
        object c = ClassTreeHelper.FindVisualChildren<System.Windows.Shapes.Path>(this).First();
*/

namespace DiagramDesigner
{
    public static class ClassTreeHelper
    {

        //why is IEnumerable<T> at the beginning and what does it mean?
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    System.Console.WriteLine(child.GetType());
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

    }
}
