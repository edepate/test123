﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;

namespace DiagramDesigner
{
    public class ConnectorAdorner : Adorner
    {
        private PathGeometry pathGeometry;
        private DesignerCanvas designerCanvas;
        private Connector sourceConnector;
        private Pen drawingPen;

        private DesignerItem hitDesignerItem;
        private DesignerItem HitDesignerItem
        {
            get { return hitDesignerItem; }
            set
            {
                if (hitDesignerItem != value)
                {
                    if (hitDesignerItem != null)
                        hitDesignerItem.IsDragConnectionOver = false;

                    hitDesignerItem = value;

                    if (hitDesignerItem != null)
                        hitDesignerItem.IsDragConnectionOver = true;
                }
            }
        }

        private Connector hitConnector;
        private Connector HitConnector
        {
            get { return hitConnector; }
            set
            {
                if (hitConnector != value)
                {
                    hitConnector = value;
                }
            }
        }

        // LUK: THis is temporary line

        public ConnectorAdorner(DesignerCanvas designer, Connector sourceConnector) //RUG: this object is instantiated
            //from the OnMouseMove from Connector.cs
            : base(designer)
        {
            this.designerCanvas = designer;
            this.sourceConnector = sourceConnector;
            drawingPen = new Pen(Brushes.Red, 1);
            drawingPen.LineJoin = PenLineJoin.Bevel;
            this.Cursor = Cursors.Cross;
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (HitConnector != null)
            {
                Connector sourceConnector = this.sourceConnector;
                Connector sinkConnector = this.HitConnector;
                Connection newConnection = new Connection(sourceConnector, sinkConnector);

                Canvas.SetZIndex(newConnection, designerCanvas.Children.Count);

                // LUK: This adds the permanent connection
                this.designerCanvas.Children.Add(newConnection);
                
            }
            if (HitDesignerItem != null)
            {
                this.HitDesignerItem.IsDragConnectionOver = false;
            }

            if (this.IsMouseCaptured) this.ReleaseMouseCapture();

            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this.designerCanvas);
            if (adornerLayer != null)
            {
                adornerLayer.Remove(this); // RUG: once the connection has been created, we don't need the ConnectorAdorner anymore
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!this.IsMouseCaptured) this.CaptureMouse();
                HitTesting(e.GetPosition(this)); //RUG: this function checks if we hit the connector, or sets the IsDragConnectionOver
                // property to show the connectors in case the mouse moves over the designerItem
                this.pathGeometry = GetPathGeometry(e.GetPosition(this));
                this.InvalidateVisual();
            }
            else
            {
                if (this.IsMouseCaptured) this.ReleaseMouseCapture();
            }
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);
            dc.DrawGeometry(null, drawingPen, this.pathGeometry);

            // without a background the OnMouseMove event would not be fired
            // Alternative: implement a Canvas as a child of this adorner, like
            // the ConnectionAdorner does.
            dc.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));
        }

        private PathGeometry GetPathGeometry(Point position) // RUG: part of this code is repeated in connection. Here is used just
            // to draw on the fly the temporary connection during the drag operation (creation phase). The drawing of the new geometry is done in
            // the OnMouseMove handler, by the method InvalidateVisual, that forces the re-render --> calls OnRender --> calls
            // dc.DrawGeometry(null, drawingPen, this.pathGeometry);
        {
            PathGeometry geometry = new PathGeometry();

            ConnectorOrientation targetOrientation;
            if (HitConnector != null)
                targetOrientation = HitConnector.Orientation;
            else
                targetOrientation = ConnectorOrientation.None;

            List<Point> pathPoints = PathFinder.GetConnectionLine(sourceConnector.GetInfo(), position, targetOrientation);

            if (pathPoints.Count > 0)
            {
                PathFigure figure = new PathFigure();
                figure.StartPoint = pathPoints[0];
                pathPoints.Remove(pathPoints[0]);
                figure.Segments.Add(new PolyLineSegment(pathPoints, true));
                geometry.Figures.Add(figure);
            }

            return geometry;
        }

        private void HitTesting(Point hitPoint) // RUG: this function is also duplicated in the ConnectionAdorner class, but it that
        {    // case is used during the modification of a connection. Here is used in the generation phase
            bool hitConnectorFlag = false;

            DependencyObject hitObject = designerCanvas.InputHitTest(hitPoint) as DependencyObject;

            while  (hitObject != null &&
                   hitObject != sourceConnector.ParentDesignerItem &&
                   hitObject.GetType() != typeof(DesignerCanvas)) // RUG: the while cycle, starting from the hitObject, goes back
                    // to the partent tree until it finds the associated designerItem of the hit object. This is used for showing the
                    // connectors once the mouse moves on the designerItem (by setting the property hitDesignerItem.IsDragConnectionOver 
                    // = true through the "set" method of hitDesignerItem). It also does the same if hitObject is the Connector, but in
                    // this case the IsDragConnectionOver will be set to false
            {
                if (hitObject is Connector)
                {
                    // conditions so that a connection can be estabilished
                    if (sourceConnector.IsInputParameter)
                        if (!(hitObject as Connector).IsReturnParameter)
                            return;
                    if (sourceConnector.IsReturnParameter)
                        if (!(hitObject as Connector).IsInputParameter)
                            return;
                    if (sourceConnector.IsDefaultOutput)
                        if (!(hitObject as Connector).IsInput)
                            return;

                    HitConnector = hitObject as Connector;
                    hitConnectorFlag = true;
                }

                if (hitObject is DesignerItem) // RUG: this IF becomes true wether if we hit the designerItem, or if we hit the
                    // connector, where in this last case the last instruction hitObject = VisualTreeHelper.GetParent(hitObject) 
                    // has to run several times before hitObject becomes DesignerItem
                {
                    HitDesignerItem = hitObject as DesignerItem;
                    if (!hitConnectorFlag)
                        HitConnector = null;
                    return;
                }
                hitObject = VisualTreeHelper.GetParent(hitObject); // RUG: this routine will select cyclically while executing the
                // WHILE statement the parent object, until the DesignerItem is reached. When the DesignerItem is reached the previous
                // IF trigger the "return", that ends the function.
            }

            HitConnector = null;
            HitDesignerItem = null;
        }
    }
}
