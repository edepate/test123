﻿using System.IO;
using System.Xml.Serialization;

namespace DiagramDesigner
{
    public static class fctserializer
    {
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static T Deserialize<T>(this string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }



        //public string SerializeToString(T value)
        //{
        //    var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
        //    var serializer = new XmlSerializer(value.GetType());
        //    var settings = new XmlWriterSettings();
        //    settings.Indent = true;
        //    settings.OmitXmlDeclaration = true;

        //    using (var stream = new StringWriter())
        //    using (var writer = XmlWriter.Create(stream, settings))
        //    {
        //        serializer.Serialize(writer, value, emptyNamepsaces);
        //        return stream.ToString();
        //    }
        //}
    }

}
